@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
		        <form class="form-horizontal" action="{{ route('vendortypes.store') }}" method="post">
	                {{ csrf_field() }}
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-vendortypes"></span>
							Create New Vendor Type
						</h2>
					</div>

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label><p>Vendor Type</p></label>
				            <input
					            class="form-control input-lg"
					            type="text"
					            id="vendortype-name"
		                        name="name"
		                        placeholder="Vendor Type Name"
		                        value="{{ old('name') }}">
						</div>
			        </div>

			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label><p>Booth Fee</p></label>
				            <br>
				            @foreach ($markets as $market)
	                            <label class="col-sm-3 col-form-label col-form-label-lg">{{$market->name}}</label>
	                            <label class="col-sm-9">
	                                 <input
	                                    class="form-control input-lg"
	                                    type="text"
	                                    id="booth_fee_{{$market->id}}"
	                                    name="booth_fee[{{$market->id}}]"
	                                    value="{{ old('booth_fee.' . $market->id) }}"
	                                    placeholder="how much">
	                            </label>
	                        @endforeach
						</div>
			        </div>

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label><p>Color</p></label>
				            <input
					            class="form-control input-lg"
					            type="text"
		                        id="vendortype-color"
		                        name="color"
		                        value="{{ old('color') }}"
		                        placeholder='#ff0000'>
						</div>
			        </div>

                    </div>
                    <div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Save
								</button>
								<a class="btn btn-warning" href="{{ URL::route('vendortypes.index') }}">Cancel</a>
								<br><br>
							</label>
						</div>
					</div>
				</form>

	            <a class="btn btn-default" href="{{ URL::route('vendortypes.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection