@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="content">
                <div class="form-horizontal" >
                    {{ csrf_field() }}
                    <div class="header">
                        <h2 class="title">
                        <span class="icon_div ti-vendortypes"></span>
                            View Vendor Type
                        </h2>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label class='setup_label'> Name</p></label>
                            <br>
                            <label class='numbers'>{{ $vendortype->name }}</label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label class='setup_label'> Booth Fee</p></label>
                            <br>
                            <ul class="numbers">
	                        @if ($vendortype->markets)
	                            @foreach ($vendortype->markets as $vt_market)
	                                <li>{{ $vt_market->name }} - ${{ $vt_market->pivot->booth_fee }}</li>
	                            @endforeach
							@else
								<li>None</li>
							@endif
                            </ul>
                            <label class='numbers'>{{ $vendortype->booth_fee }}</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">

                            <label class='setup_label'> Color</label>
                            <br>
                            <label class='numbers'>{{ $vendortype->color }}</label>
                            <span class="form-control" style='width:30px;display:inline !important;background-color:{{$vendortype->color }}'>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="form-group">
                            <div class="col-sm-9">
                                <form method="POST" action="{{ URL::route('vendortypes.destroy', $vendortype->id) }}"> <!--- action points to route :EL --->
                                    {{ csrf_field() }} <!--- adds token to ensure valid web site sent request :EL --->
                                    <input type="hidden" name="_method" value="DELETE"> <!--- fakes delete method  :EL --->
                                    <a class="btn btn-fill btn-info" href="{{ URL::route('vendortypes.edit', $vendortype->id) }}">Edit</a> <!--- link points to route :EL --->
                                    <input type="submit" value="Delete" class="btn btn-fill btn-warning">

                                    <label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
                                    <label class="col-sm-9">

                                    <br><br>
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="btn btn-default" href="{{ URL::route('vendortypes.index') }}">View All</a>
            </div>
        </div>
    </div>
</div>
@endsection



