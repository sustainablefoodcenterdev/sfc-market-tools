@extends('layouts.app')

@section('content')
<div class="header">
    <h2 class="title">
        <i class="icon_div ti-vendortypes"></i>
        Vendor Types
        <a class="btn btn-primary" href="{{ route('vendortypes.create') }}">Add New</a>
    </h2>
</div>

<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
        	<th>Color</th>
            <th>Type</th>
        	<th>Booth Fee</th>
        	<th>Actions</th>
    	</tr>
    </thead>
    <tbody>

		@foreach ($vendortypes as $vendortype)
        <tr>
            <td><a href='{{ URL::route('vendortypes.edit', $vendortype->id) }}'><button class="btn btn-lg btn-info" >Edit</button></a></td>
			<td><span style='background-color:{{ $vendortype->color }}'>&nbsp;&nbsp;&nbsp;</span></td>
			<td>{{ $vendortype->name }}</td>
        	<td>
                <ul>
                @foreach ($vendortype->markets as $vt_market)
                    <li>{{ $vt_market->name }} - ${{ $vt_market->pivot->booth_fee }}</li>
                @endforeach
                </ul>
            </td>
        	<td>
		        <form method="POST" action="{{ URL::route('vendortypes.destroy', $vendortype->id) }}">
		        	<a href='{{ URL::route('vendortypes.show', $vendortype->id) }}'><button class="btn btn-med btn-info"  type="button">View</button></a>
				    {{ csrf_field() }}
				    <input type="hidden" name="_method" value="DELETE">
				    <input type="submit" value="Delete" class="btn btn-med btn-info">
				</form>
	        </td>
        </tr>
		@endforeach
    </tbody>
</table>

@endsection