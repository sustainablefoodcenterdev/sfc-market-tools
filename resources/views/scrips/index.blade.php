@extends('layouts.app')

@section('content')
<div class="header">
    <h2 class="title">
        <i class="icon_div ti-scrips"></i>
        Scrip
        <a class="btn btn-primary" href="{{ route('scrips.create') }}">Add New</a>
		<br><small>Deleted scrips are shown at bottom <a href='#deleted'><i class="icon_div ti-link"></i></a></small>
    </h2>
</div>

<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
            <th>Color</th>
        	<th>Name</th>
        	<th>Denomination</th>
        	<th>Distribute?</th>
        	<th>Actions</th>
    	</tr>
    </thead>
    <tbody>

	@php
		$found = false;
	@endphp
	@foreach ($scrips as $scrip)

		@if (! empty($scrip->deleted_at) )
			@if (! $found )
				<tr class='success' style='text-align:center'>
					<td colspan='7'><a name='deleted'>DELETED SCRIPS</td>
				</tr>
				@php
					$found = true;
				@endphp
			@endif
		@endif
        <tr>
            <td>
				@if (empty($scrip->deleted_at) )
					<a href='{{ URL::route('scrips.edit', $scrip->id) }}'><button class="btn btn-lg btn-info" >Edit</button></a>
				@else
					&nbsp;
				@endif
			</td>
			<td><span style='background-color:{{ $scrip->color }}'>&nbsp;&nbsp;&nbsp;</span></td>
			<td>{{ $scrip->name }}</td>
        	<td>${{ $scrip->denomination }}</td>
        	<td>@if ($scrip->distributed_at_market)
	        		Yes
	        	@else
	        		No
				@endif
        	</td>
        	<td>
				@if (empty($scrip->deleted_at) )
					<form method="POST" action="{{ URL::route('scrips.destroy', $scrip->id) }}">
						<a href='{{ URL::route('scrips.show', $scrip->id) }}'><button class="btn btn-med btn-info" type='button'>View</button></a>
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<input type="submit" value="Hide" class="btn btn-med btn-info">
					</form>
				@else
					<form method="GET" action="{{ URL::route('scrips.restore', $scrip->id) }}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">

						<input type="submit" value="Unhide" class="btn btn-med btn-info">
					</form>
				@endif

	        </td>
        </tr>
		@endforeach
    </tbody>
</table>

@endsection