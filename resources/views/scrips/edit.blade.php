@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="content">
		        <form class="form-horizontal" action="{{ URL::route('scrips.update', $scrip->id) }}" method="post">
	                {{ csrf_field() }}
					<input type="hidden" name="_method" value="PATCH">
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-scrips"></span>
							Edit Scrip Type: {{ $scrip->name }}
						</h2>
					</div>

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
				            <label><p>Scrip</p></label>
		                    	<input
		                        class="form-control input-lg"
		                        type="text"
		                        id="scrip-name"
		                        name="name"
		                        placeholder="Scrip Type"
		                        value="{{ old('name', $scrip->name) }}">
							</label>

						</div>
			        </div>
					<br clear="all"/>
			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
							<label><p>Denomination</p></label>
							<input
		                        class="form-control input-lg"
		                        type="text"
		                        id="scrip-denomination"
		                        name="denomination"
		                        placeholder="Scrip Type"
		                        value="{{ old('denomination', $scrip->denomination) }}">
						</div>
			        </div>
			        <br clear="all"/>
			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
							<label><p>Color</p></label><br>
							 <input
		                        class="form-control input-lg"
		                        type="text"
		                        id="scrip-color"
		                        name="color"
		                        placeholder="Scrip Type"
		                        value="{{ old('color', $scrip->color) }}"
		                        style='display:inline !important;width:150px'>
		                        <span class="form-control input-lg" style='width:30px;display:inline !important;background-color:{{ old('color', $scrip->color) }}'>
						</div>
			        </div>


					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label class="input_control"><p>Distributed at Market</p></label>
				            <label class="form-control input-lg">
				            <input
					            type="checkbox"
		                        id="distributed-at-market"
		                        name="distributed_at_market"
		                        value="1"
		                        @if (old('distributed_at_market', $scrip->distributed_at_market)) checked @endif
		                        > Yes
				            </label>
						</div>
			        </div>

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
							<label><p>Dashboard Display Order</p></label><br>
				            <input
					            class="form-control input-lg"
					            type="text"
		                        id="dashboard-scrip-order"
		                        name="dashboard_sort_order"
		                        value="{{ old('dashboard_sort_order', $scrip->dashboard_sort_order) }}"
		                        placeholder='number'>
						</div>
			        </div>

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
							<label><p>Report Display Order</p></label><br>
				            <input
					            class="form-control input-lg"
					            type="text"
		                        id="report-scrip-order"
		                        name="report_sort_order"
		                        value="{{ old('report_sort_order', $scrip->report_sort_order) }}"
		                        placeholder='number'>
						</div>
			        </div>

					<div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Update
								</button>
								<a class="btn btn-warning" href="{{ URL::route('scrips.index') }}">Cancel</a>
							</label>
						</div>
						<br><br><br><br>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
