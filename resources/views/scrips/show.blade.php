@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
	        <div class="content">
		        <div class="form-horizontal">
	                {{ csrf_field() }}
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-scrips"></span>
							View Scrip
						</h2>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Name</label>
								<br>
								<label class='numbers'>{{ $scrip->name }}</label>
							</div>
				        </div>

						<div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Denomination</label>
								<br>
								<label class='numbers'>${{ $scrip->denomination }}</label>
							</div>
				        </div>

						<div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Color</label>
								<br>
								<label class='numbers'>{{ $scrip->color }}</label>
								<span class="form-control" style='width:30px;display:inline !important;background-color:{{$scrip->color }}'>
		                        	&nbsp;&nbsp;&nbsp;
		                        </span>
							</div>
				        </div>
					</div>

					<div class="col-sm-6">
				        <div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'>Distributed at Market?</label>
								<br>
								<label class='numbers'>@if ($scrip->distributed_at_market) Yes @else No @endif</label>
							</div>
				        </div> <div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'>Dashboard Sort Order</label>
								<br>
								<label class='numbers'>{{ $scrip->dashboard_sort_order }}</label>
							</div>
				        </div>
				         <div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'>Report Sort Order</label>
								<br>
								<label class='numbers'>{{ $scrip->report_sort_order }}</label>
							</div>
				        </div>
					</div>
					<br clear="all">
					<br clear="all">
                    <div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<form method="POST" action="{{ URL::route('scrips.destroy', $scrip->id) }}"> <!-- action points to route :EL -->
								    <label class='numbers'>{{ csrf_field() }}</label> <!-- adds token to ensure valid web site sent request :EL -->
								    <input type="hidden" name="_method" value="DELETE"> <!-- fakes delete method  :EL -->
								    <a class="btn btn-fill btn-info" href="{{ URL::route('scrips.edit', $scrip->id) }}">Edit</a> <!-- link points to route :EL -->
								    <input type="submit" value="Delete" class="btn btn-fill btn-warning">
								</form>
							</div>
						</div>
					</div>
	            <a class="btn btn-default" href="{{ URL::route('scrips.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>

@endsection
