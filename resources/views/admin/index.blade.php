@extends('layouts.app')

@section('sidebar_button')
!<div class="dropdown">
      <button href="#" class="btn btn-lg btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          Admin Tools
          <b class="caret"></b>
      </button>
      <ul class="dropdown-menu">
        <li><p><a href="{{ route('market_days.index') }}">Setup New Market Day</a></p></li>
        <li><p><a href="{{ route('markets.index') }}">At the Market</a></p></li>
        <li><p><a href="#">Back at the Office</a></p></li>
        <li><p><a href="{{ route('admin') }}">Admin Tools</a></p></li>
      </ul>
</div>
@endsection


@section('sidebar_links')
	{{-- @include('sidebar_links') --}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
				{{--
                <div class="panel-heading">Administration</div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ route('markets.index') }}">Markets</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('market_days.index') }}">Market Days</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('scrips.index') }}">Scrip</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('vendors.index') }}">Vendors</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('vendortypes.index') }}">Vendor Types</a>
                    </li>
                </ul>
            </div>
                --}}
        </div>
    </div>
</div>
@endsection
