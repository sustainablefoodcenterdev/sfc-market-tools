<ul class="list-group vendor-list">
    @foreach ($selected_vendors as $vendor_id)
        @php $vendor = $vendors->where('value', $vendor_id)->first(); @endphp
        <li
            class="list-group-item"
            data-id="{{ $vendor['value'] }}"
            data-name="{{ $vendor['text'] }}"
            style="cursor: pointer;"
            >
            <span class="icon_div ti-move"></span>&nbsp;&nbsp;
            {{ $vendor['text'] }}

			{{-- market_crew does not get the delete button --}}
			@hasrole(['market_crew'])
			@else
	            <span class="icon_div ti-trash text-danger pull-right remove-item"></span>
			@endhasrole

            <input value="{{ $vendor['value'] }}" type="hidden" name="vendors[]">
        </li>
    @endforeach
</ul>

<div class="row">
    <div class="col-sm-8">
        <select class="form-control" id="vendors"></select>
    </div>
    <div class="col-sm-4">
        <a class="add-item btn btn-default"><span class="icon_div ti-plus"></span></a>
    </div>
</div>