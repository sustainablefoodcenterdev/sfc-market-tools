@extends('layouts.app')

@section('content')
<div class="header">
    <h2 class="title">
        <i class="icon_div ti-market_days"></i>
        Market Days
		@hasrole(['superadmin','market_coordinator'])
        	<a class="btn btn-primary btn-lg" href="{{ route('market_days.create') }}">Add New</a>
        @endhasrole
    </h2>
</div>


<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
{{--            <th>&nbsp;</th> --}}
            <th>Name</th>
            <th>
	            @hasrole(['superadmin','double_dollar_coordinator','market_coordiantor'])
	            	Comments
	            @endhasrole
	        </th>
        	<th>Day</th>
        	<th>Actions</th>
    	</tr>
    </thead>
    <tbody>

		@foreach ($market_days as $market_day)
        <tr>
            <td><a href='{{ URL::route('market_days.edit', $market_day->id) }}'><button class="btn btn-lg btn-info" >Edit</button></a></td>
{{--            <td>
				@hasrole(['superadmin','market_coordinator'])
		            <a href='{{ URL::route('market_days.batch_upload', $market_day->id) }}'><button class="btn btn-lg btn-info" >Batch Insert Vendors</button></a>
		        @endhasrole
	        </td>
--}}
			<td>
				<img src="/img/markets/{{ $market_day->market->image }}" width='80' height='60'>
				{{ $market_day->market->name}}
			</td>
			<td>
				@hasrole(['superadmin','double_dollar_coordinator'])
					<small><b>SCRIP:</b></small>&nbsp;{{ substr($market_day->scrip_comments,1,20)}}
				@endhasrole
				@hasrole(['superadmin','market_coordinator'])
					<br>
					<small><b>MARKET:</b></small>&nbsp;{{ substr($market_day->market_comments,1,20)}}
				@endhasrole
			</td>
        	<td>{{ date('D M d, Y', strtotime($market_day->date)) }}</td>
        	<td>
		        <form method="POST" action="{{ URL::route('market_days.destroy', $market_day->id) }}">
		        	<a href='{{ URL::route('market_days.show', $market_day->id) }}'><button class="btn btn-med btn-info"  type="button">View</button></a>
				    {{ csrf_field() }}
				    <input type="hidden" name="_method" value="DELETE">
				    <input id='delete_btn' type="button" value="Delete" class="btn btn-med btn-info">
				</form>
	        </td>

        </tr>
		@endforeach
    </tbody>
</table>

@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    <script>
        jQuery(document).ready(function($) {
	        // confirm delete
	        $('input[id=delete_btn]').click( function() {
		        if (confirm('Are you sure you want to delete this Market Day?\n\nIt cannot be undone') ) {
			        $(this).closest('form').submit()
		        }
	        })
	    })
    </script>
@endsection

