
<script>
    $( ".vendor-list" ).sortable();
    // only not active ones here since its adding
    var options =    {!! json_encode($vendors->where('deleted_at',NULL)->values()->toArray() ) !!}
    var unselected_items = {!! json_encode( $unselected_vendors ) !!};
    var selected_items = {!! json_encode( $selected_vendors ) !!};

    // set the select search
    $('#vendors').selectize({
        placeholder: "Add Vendor",
        options: options,
        maxItems: 1,
        items: unselected_items
    });

    $('.vendor-list').on('click', '.remove-item', function(event) {
        event.preventDefault();
        var $parent = $(this).parents('.list-group-item').first();
        $('#vendors')[0].selectize.addOption({'value': $parent.data('id'), 'text': $parent.data('name')});
        $('#vendors')[0].selectize.refreshOptions(false);
        $parent.remove();
    });

    $('.add-item').on('click', function(event) {
        event.preventDefault();
        var selected_item = $('#vendors')[0].selectize.options[ $('#vendors')[0].selectize.items[0] ];
        if ( typeof selected_item !== "undefined" ) {
            $('#vendors')[0].selectize.removeOption(selected_item.value);
            $('#vendors')[0].selectize.refreshOptions(false);
            $('.vendor-list').append(
                '<li class="list-group-item" data-id="' + selected_item.value + '" '
                + 'data-name="' + selected_item.text + '" '
                + 'style="cursor: pointer;"><span class="icon_div ti-move"></span>&nbsp;&nbsp;'
                + selected_item.text
                + '<span class="icon_div ti-trash text-danger pull-right remove-item"></span>'
                + '<input type="hidden" value="' + selected_item.value + '" name="vendors[]"></li>'
            );
        }
    });

    for (var i = 0; i < selected_items.length; i++) {
        if ( $('.list-group-item[data-id="' + selected_items[i] + '"]').length ) {
            $('#vendors')[0].selectize.removeOption(selected_items[i]);
        }
    }

    $('#vendors')[0].selectize.refreshOptions(false);

	// character counter for comments
    $('textarea[id=comments]').characterCounter();



    // ** populate any OLD vendors if any
	@if (old('vendors'))
		//
		var $select = $("#vendors").selectize();
		var selectize = $select[0].selectize;

    	// loop through any old vendors
		@foreach(old('vendors') as $old_vendor)
	    	// select vendor and click + button
			selectize.setValue({{ $old_vendor[0] }});
			$('.add-item').click();
		@endforeach

	@endif


</script>