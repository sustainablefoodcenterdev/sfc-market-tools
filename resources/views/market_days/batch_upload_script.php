<script>
	// toggle warning class on dropdown
    $('select[id=vendor_list]').change( function(event) {
        event.preventDefault();
        // add/remove warning based on value being blank
		if ($(this).val()) {
			$(this).removeClass('btn-danger');
		}
		else {
			// not blank, remove warning
			$(this).addClass('btn-danger');
		}
		// remove error f needed
		if ($(".btn-danger:visible").length == 0) {
	        // show alert
	        $('#submit_text').text('');
	    }
    });
	// valiate before submitting
    $('#submit_button').click( function() {
	    if ($(".btn-danger:visible").length > 0) {
	        // show alert
	        $('#submit_text').text('There is an error on the page.');
	        return false;
	    }
    })

    // activate the delete button
    $('.remove-vendor').on('click', function(event) {
        event.preventDefault();
        // define the row to be removed
        var this_delete_row=$(this).parent('div').parent('.form-group');
		// remove the select
		this_delete_row.remove();

		// disable submit button if none left
		if (! $('#uploaded-vendor-names').length) {
			$('#submit_button').prop('disabled',true);
		}
    });

</script>