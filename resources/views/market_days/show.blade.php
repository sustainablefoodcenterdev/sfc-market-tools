@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
	        <div class="content">
		        <div class="form-horizontal" >
	                {{ csrf_field() }}
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-market_days"></span>
							View Market Day
						</h2>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Date</label>
								<br>
								<label class='numbers'>{{ date('D M d, Y', strtotime($market_day->date)) }}</label>
							</div>
				        </div>
				        <div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Market</label>
								<br>
								<label class='numbers'>{{ $market_day->market->name }}</label>
							</div>
				        </div>

				        @hasrole(['superadmin','market_coordinator'])
				        <div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Market Comments</label>
								<br>
								<label class='numbers'>{{ $market_day->market_comments }}</label>
							</div>
				        </div>
				         @endhasrole
					</div>
					<div class="col-sm-6">
						 @hasrole(['superadmin','market_coordinator','market_crew'])
				        <div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Vendors
								@hasrole(['superadmin','market_coordinator'])
									    <a href='{{ URL::route('market_days.batch_upload', $market_day->id) }}'><button class="btn btn-sm btn-info" >Batch Insert Vendors</button></a>
								@endhasrole
								</label>
								<br>
								<label class='numbers'>
								 @if ( count($market_day->vendors) )
		                            <ol>
		                            @foreach ($market_day->vendors()->orderBy('location_order')->get() as $vendor)
		                                <li>{{ $vendor->name }}</li>
		                            @endforeach
		                            </ol>
		                        @else
		                        	--none--
		                        @endif
		                        </label>

							</div>
				        </div>
				        @endhasrole

						@hasrole(['superadmin','double_dollar_coordinator'])
						<div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Scrip Brought</label>
								<br>
								<label class='numbers'>
								@if ( count($market_day->brought_scrips) )

									<ul class="list-group">

                                        @foreach ($market_day->brought_scrips as $brought_scrip)
                                            <li class="list-group-item clearfix">
                                                <div class="col-xs-6"><span style='display:inline !important;background-color:{{ old('color', $brought_scrip->color) }}'>&nbsp;&nbsp;&nbsp;</span>&nbsp;{{ $brought_scrip->name }}</div>
                                                <div class="col-xs-6">{{ $brought_scrip->pivot->pieces }} pieces</div>
                                            </li>
                                        @endforeach
                                	</ul>
                                    @else
                                    	--none--
                                    @endif

								</label>
							</div>
				        </div>
				        <div class="form-group row">
					        <div class="col-sm-1">&nbsp;</div>
							<div class="col-sm-9">
								<label class='setup_label'> Scrip Comment</label>
								<br>
								<label class='numbers'>{{ $market_day->scrip_comments }}</label>
							</div>
				        </div>
						@endhasrole
					</div>
                    <div class="card-footer">
						<div class="col-sm-9 col-sm-offset-3">
							<form method="POST" action="{{ URL::route('market_days.destroy', $market_day->id) }}"> <!-- action points to route :EL -->
							    {{ csrf_field() }} <!-- adds token to ensure valid web site sent request :EL -->
							    <input type="hidden" name="_method" value="DELETE"> <!-- fakes delete method  :EL -->
							    <a class="btn btn-fill btn-info" href="{{ URL::route('market_days.edit', $market_day->id) }}">Edit</a> <!-- link points to route :EL -->
							    <input id='delete_btn' type="button" value="Delete" class="btn btn-fill btn-warning">
							</form>
						</div>
					</div>
				</div>
	            <a class="btn btn-default" href="{{ URL::route('market_days.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    <script>
        jQuery(document).ready(function($) {
	        // confirm delete
	        $('input[id=delete_btn]').click( function() {
		        if (confirm('Are you sure you want to delete this Market Day?\n\nIt cannot be undone') ) {
			        $(this).closest('form').submit()
		        }
	        })
	    })
    </script>
@endsection
