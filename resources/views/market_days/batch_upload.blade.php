@extends('layouts.app')

@section('content')

<div class="container content">
    <div class="row">
        <div class="col-sm-8">

            <form class="form-horizontal" action="{{ URL::route('market_days.do_batch_upload', $market_day->id) }}" method="POST">
                {{ csrf_field() }}

                <div class="header">
                    <h2 class="title">
                    <span class="icon_div ti-market_days"></span>
                        Upload Vendors for Market Day {{ $market_day->name }}
                    </h2>
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="content">

                    @if (empty($submitted_vendors) && !old('vendors'))
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label col-form-label-lg">Vendors</label>
                            <div class="col-sm-9">
                                <textarea name="vendor_upload" cols="40" rows="10" style="resize: both;" class="form-control"></textarea>
                            </div>
                        </div>
                    @else
                        <h2>Confirm Vendor List</h2>
                        <hr>

                        <div class="row">
                            <div class="col-sm-6">
                                <p>Uploaded Name</p>
                            </div>
                            <div class="col-sm-6">
                                <p>Entire Vendor List</p>
                            </div>
                        </div>

                        @if (!empty($submitted_vendors))
                            @foreach($submitted_vendors as $submitted_vendor)

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input type="text" name="uploaded_vendor_names[]" id="uploaded-vendor-names" class="form-control" value="{{ $submitted_vendor }}">
                                    </div>
                                    <div class="col-sm-5">
                                        @include('market_days.vendor_dropdown')
                                    </div>
                                    <div class="col-sm-1">
										<a href="#" class="pull-right text-danger remove-vendor"><i class="icon_div ti-trash"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        @elseif (old('vendors'))
                            @php $uploaded_vendor_names = old('uploaded_vendor_names'); @endphp
                            @php $selected_vendor_ids = old('vendors'); @endphp
                            @foreach($uploaded_vendor_names as $key => $uploaded_vendor_name)
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input type="text" name="uploaded_vendor_names[]" class="form-control" value="{{ $uploaded_vendor_name }}">
                                    </div>
                                    <div class="col-sm-5">
                                        @include('market_days.vendor_dropdown', ['selected_id' => $selected_vendor_ids[$key]])
                                    </div>
									<div class="col-sm-1">
										<a href="#" class="pull-right text-danger remove-vendor"><i class="icon_div ti-trash"></i></a>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                    @endif

                </div>


                <div class="card-footer">
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-1">
	                        &nbsp;
                        </div>
                        <div class="col-md-2">
                            <button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
                                Upload
                            </button>
                        </div>
                        <div class="col-md-4">
                            <span id="submit_text" class="alert-warning"></span>
                        </div>
                        <div class="col-md-4">
							<a class="btn btn-warning" href="{{ URL::route('market_days.index') }}">Cancel</a>
							&nbsp;&nbsp;
							<a class="btn btn-warning" href="#" onclick="window.history.back()">Back</a>
                        </div>
	                </div>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	@parent
	@include('market_days.batch_upload_script')
@endsection