@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="content">
		        <form id='batch_upload_form' class="form-horizontal" action="{{ URL::route('market_days.update', $market_day->id) }}" method="POST">
	                {{ csrf_field() }}
					<input type="hidden" name="_method" value="PATCH">

					<div class="header">
						<h2 class="title">
						<span class="icon_div ti-market_days"></span>
							Edit Market Day {{ $market_day->name }}
						</h2>
					</div>

		            @if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

					<!---  :EL --->
					@hasrole(['superadmin','market_coordinator'])
					<div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label><p>Date </p></label>
				            <input
					            class="form-control input-lg"
					            type="date"
		                        id="date"
		                        name="date"
		                        placeholder="YYYY-MM-DD"
		                        value="{{ old('date', date('Y-m-d', strtotime($market_day->date))) }}">
						</div>
					</div>
			        <br clear="all"/>

					<div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
							 <label><p>Market</p></label>
							 <select
		                        class="form-control input-lg"
		                        type="text"
		                        id="market"
		                        name="market_id"
		                        >
		                        @foreach ($markets as $market)
		                            @if ($market->id === $market_day->market->id)
		                                <option selected value="{{ $market->id }}">{{ $market->name }}</option>
		                                @continue
		                            @endif
		                            <option value="{{ $market->id }}">{{ $market->name }}</option>
		                        @endforeach
		                    </select>
						</div>
					</div>
			        <br clear="all"/>

					<div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-5">
				            <label><p>Market Comments</p></label>
		                      <textarea
		                        class="form-control"
		                        id="comments"
		                        name="market_comments"
		                        data-max-length='1000'
		                        rows='6'
		                        columns='80'
		                        placeholder="Comments about the market">
		                        {{ old('market_comments',$market_day->market_comments)}}
		                        </textarea>
						</div>
					</div>

					@hasrole(['superadmin','market_coordinator','market_crew'])
					 <div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-6">
				            <label><p>Vendors</p></label>
				            @include('market_days.vendor_list')
						</div>
					</div>
			        <br clear="all"/>
					@else
						{{-- double dollar needs hiddens --}}
						<input type="hidden" name="market_comments" value="{{ $market_day->market_comments }}">
					@endhasrole

					@else
	                    {{-- double dollar needs hiddens --}}
						<input type="hidden" name="date" value="{{ date('Y-m-d', strtotime($market_day->date)) }}">
						<input type="hidden" name="market_id" value="{{ $market_day->market->id }}">
			        @endhasrole

					@hasrole(['superadmin','double_dollar_coordinator'])
					<br clear="all"/>
					<div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-8">
				            <label><p>Scrip Brought</p></label>

							@foreach ($scrips as $scrip)
							<div class="form-group">
								<label class="col-sm-5">
									<span style='display:inline !important;background-color:{{ old('color', $scrip->color) }}'>&nbsp;&nbsp;&nbsp;</span>
									{{ $scrip->name }}
								</label>
								<div class="col-sm-3">
				                    <input
				                        class="form-control"
				                        type="number"
				                        id="scrip_amount"
				                        name="scrip_amount[{{ $scrip->id }}]"
				                        @if ($market_day->brought_scrips()->where('scrip_id', $scrip->id)->count())
					                        value="{{ old('scrip_amount.' . $scrip->id, $market_day->brought_scrips()->where('scrip_id', $scrip->id)->first()->pivot->pieces) }}"
					                    @else
						                    value="{{ old('scrip_amount.' . $scrip->id, 0) }}"
					                    @endif
				                        placeholder="pieces"
				                        >
								</div>
							</div>
							@endforeach
						</div>
					</div>
			        <br clear="all"/>
			        <div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-6">
				            <label><p>Scrip Comments</p></label>
				            <textarea
					            class="form-control input-lg"
					            id="comments"
		                        name="scrip_comments"
								data-max-length='1000'
		                        rows='6'
		                        columns='80'
		                        placeholder="Comments about scrips">{{ old('scrip_comments', $market_day->scrip_comments) }}</textarea>
						</div>
					</div>
			        <br clear="all"/>
			        @else
			        	{{-- market needs hidden --}}
                    	<input type="hidden" name="scrip_amount[]" value=''>
                    	<input type="hidden" name="scrip_comments" value='{{ $market_day->scrip_comments }}'>
			        @endhasrole

					<div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Update
								</button>
								<a class="btn btn-warning" href="{{ URL::route('market_days.index') }}">Cancel</a>
							</label>
						</div>
						<br><br><br><br>
					</div>
				</form>
            </div>
            <a class="btn btn-default" href="{{ URL::route('market_days.index') }}">View All</a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	@parent
	@include('market_days.vendor_list_script')
@endsection