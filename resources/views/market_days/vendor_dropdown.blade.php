{{-- assume not in list --}}
@php
	$vendor_found = false;
@endphp

{{-- loop and see if vendor is in list --}}
@foreach($vendors as $vendor)
	@if (isset($selected_id) && (int)$selected_id === $vendor->id)
		@php
			$vendor_found = true;
			$vendor_id = $vendor->id;
			$submitted_vendor = $vendor->name;
		@endphp
	@elseif (isset($submitted_vendor) && $vendor->name == $submitted_vendor)
		@php
			$vendor_found = true;
			$vendor_id = $vendor->id;
		@endphp
	@endif
@endforeach

{{-- if items is in list, display it along with hidden input; otherwise show dropdown --}}
@if ($vendor_found)
	<input type="hidden" name="vendors[]" value="{{ $vendor_id }}">
	<span class="form-control">{{ $submitted_vendor }}</span>
@else
<select class="form-control vendor-dropdown btn-danger btn-fill" name="vendors[]" id="vendor_list">
	<option value="">--match to existing vendor--</option>
	@foreach($vendors as $vendor)
		<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
	@endforeach
</select>

@endif
