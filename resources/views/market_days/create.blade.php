@extends('layouts.app')

@section('content')
<div class="container content">
    <div class="row">
        <div class="col-sm-8">
	        <form class="form-horizontal" action="{{ URL::route('market_days.store') }}" method="POST">
                {{ csrf_field() }}
				 <div class="header">
					<h2 class="title">
					<span class="icon_div ti-market_days"></span>
						Create New Market Day
					</h2>
				</div>

	            @if (count($errors) > 0)
	                <div class="alert alert-danger">
	                    <ul>
	                        @foreach ($errors->all() as $error)
	                            <li>{{ $error }}</li>
	                        @endforeach
	                    </ul>
	                </div>
	            @endif

				<div class="form-group">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-5">
		            <label><p>Market Date</p></label>
		             <input
                        class="form-control"
                        type="date"
                        id="market-date"
                        name="date"
                        placeholder="yyyy-mm-dd"
                        value="{{ old('date') }}"
						size="10">
					</div>
		        </div>


                <div class="form-group">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-5">
		            <label><p>Market</p></label>
		             <select
                        class="form-control"
                        type="text"
                        id="market"
                        name="market_id"
                        >
	                    <option value="">Select a market</option>
                        @foreach ($markets as $market)
	                        {{-- if day == old data, select it
							QUESTION:  if old('day') = '' (blank) why does it match 0 in IF statement --}}
                            <option value="{{ $market->id }}" @if ($market->id ==  old('market_id') && old('market_id') != '') selected @endif>{{ $market->name }}</option>
                        @endforeach
                    </select>
					</div>
		        </div>

                <div class="form-group">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-5">
		            <label><p>Market Comments</p></label>
                      <textarea
                        class="form-control"
                        id="comments"
                        name="market_comments"
                        data-max-length='1000'
                        rows='6'
                        columns='80'
                        placeholder="Comments about the market">{{ old('market_comments')}}</textarea>
					</div>
		        </div>

				<div class="form-group">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-8">
		             @include('market_days.vendor_list')
					</div>
		        </div>

		        @hasrole(['superadmin','double_dollar_coordinator'])
					<br clear="all"/>
					<div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-8">
				            <label><p>Scrip Brought</p></label>

							@foreach ($scrips as $scrip)
							<div class="form-group">
								<label class="col-sm-5">
									<span style='display:inline !important;background-color:{{ old('color', $scrip->color) }}'>&nbsp;&nbsp;&nbsp;</span>
									{{ $scrip->name }}
								</label>
								<div class="col-sm-3">
				                    <input
				                        class="form-control"
				                        type="number"
				                        id="scrip_amount"
				                        name="scrip_amount[{{ $scrip->id }}]"
										value="{{ old('scrip_amount.' . $scrip->id, 0) }}"
				                        placeholder="pieces"
				                        >
								</div>
							</div>
							@endforeach
						</div>
					</div>
			        <br clear="all"/>
			        <div class="form-group">

						<div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-6">
				            <label><p>Scrip Comments</p></label>
				            <textarea
					            class="form-control input-lg"
					            id="comments"
		                        name="scrip_comments"
								data-max-length='1000'
		                        rows='6'
		                        columns='80'
		                        placeholder="Comments about scrips">{{ old('scrip_comments') }}</textarea>
						</div>
					</div>
			        <br clear="all"/>
			    @endhasrole

				<br clear="all"><br>
	          	<div class="card-footer">
					<div class="form-group">
						<div class="col-sm-9">
							<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
							<label class="col-sm-9">
							<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
								Create
							</button>
							<a class="btn btn-warning" href="{{ URL::route('market_days.index') }}">Cancel</a>
						</label>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	@include('market_days.vendor_list_script')
@endsection