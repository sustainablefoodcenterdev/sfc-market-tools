<div class="row" >
    <div class="col-md-4 vendor-search">
    	<div class="input-group">
    		<span class="input-group-addon"  style='border:0px;margin:-10px;'><i class="icon_div ti-search"></i></span>
    		<select class="vendor_search" name='vendor_search_id' class="form-control input-lg">
			</select>
    	</div>
    </div>
</div>

@section('scripts')
{{-- Outputs script tags from app.blade.php first --}}
	@parent
	<script>
        // data collection for <option> statements
        @if ( isset( $market_day ) )
        var data = {!! json_encode( $market_day->getVendorsForMultiSelect() ) !!};
		// if empty, alter <select>

		if (data.length == 0) {

			$selectized = $('.vendor_search').selectize({
	            placeholder: "No Vendor Attended",
	            options: []
	        })
	        $selectized[0].selectize.disable();

		} // if
		else {
			// not empty, populate DD
	        var base_route = "{{ route('tools.' . $tool, ['market_day' => $market_day->id, 'vendor' => 'vendor_placeholder']) }}";

	        $('.vendor_search').selectize({
	            placeholder: "Select a vendor",
	            options: data
	        });

	        $(".vendor_search").on('change', function(event){
	            var route = base_route.replace('vendor_placeholder', $(event.target).find(':checked').val());
	            window.location.href = route;
	        });
		} // else
        @endif

	</script>
@endsection
