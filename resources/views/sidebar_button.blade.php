{{-- if logged in show links --}}
@if (Auth::check())


<div class="sidebar-wrapper">
	{{--	:user --}}
	<div class="user">
        <div class="info">

            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <span>
                	<h3>
					@if ($url_level == 'admin' )
						Setup
					@elseif ($url_level == 'tools')
						At the Market
					@elseif ($url_level == 'reports')
						Reports
					@else
						Start
					@endif
					<b class="caret"></b>
                	</h3>
				</span>
            </a>
			<div class="clearfix"></div>

            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    {{-- Coordinator level and above --}}
					@hasrole(['superadmin','double_dollar_coordinator','market_coordinator','market_crew'])
						@if ($url_level != 'admin' )
							<li><h4><a href="{{ route('admin') }}">Setup</a></h4></li>
						@endif
					@endhasrole

					{{-- everyone has access to market tools --}}
					@if ($url_level != 'tools' )
						<li><h4><a href="{{ route('tools.home') }}">At the Market</a></h4></li>
					@endif

					{{-- Coordinator level and above --}}
					@hasrole(['superadmin','double_dollar_coordinator','market_coordinator'])
						@if ($url_level != 'reports' )
							<li><h4><a href="{{ route('reports.index') }}">Reports</a></h4></li>
						@endif
					@endhasrole

                </ul>
            </div>
        </div>
    </div>

	{{-- start list for links --}}

		<ul class="nav">

			{{-- display correct selected option --}}

			{{-- ADMIN --}}
			@if ($url_level == 'admin' )

				@hasrole(['superadmin','market_coordinator'])
					<li class="{{ (Request::is('admin/markets') || Request::is('admin/*/markets') || Request::is('admin/markets/*') ? 'active' : '') }} sidebar_link sidebar_link ">
						<a href="{{ route('markets.index') }}">
								<div class='icon_div'>
									<i class="ti-markets"></i>
								</div>
								<div class='label_div'>
									<p>Markets</p>
								</div>
						</a>
						<br>
				    </li>
			    @endhasrole
				@hasrole(['superadmin','double_dollar_coordinator','market_coordinator','market_crew'])
				    <li class="	{{ (Request::is('admin/market_days') || Request::is('admin/*/market_days') || Request::is('admin/market_days/*') ? 'active' : '') }} sidebar_link sidebar_link ">
				        <a href="{{ route('market_days.index') }}">
								<div class='icon_div'>
									<i class="ti-market_days"></i>
								</div>
								<div class='label_div'>
									<p>Market Days</p>
								</div>
					     </a>
							<br>
				    </li>
				@endhasrole

				@hasrole(['superadmin','market_coordinator'])
				    <li class="	{{ (Request::is('admin/scrips') || Request::is('admin/*/scrips') || Request::is('admin/scrips/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('scrips.index') }}">
								<div class='icon_div'>
									<i class="ti-scrips"></i>
								</div>
								<div class='label_div'>
									<p>Scrip</p>
								</div>
				        </a>
							<br>
				    </li>
				    <li class="	{{ (Request::is('admin/vendors') || Request::is('admin/*/vendors') || Request::is('admin/vendors/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('vendors.index') }}">
								<div class='icon_div'>
									<i class="ti-vendors"></i>
								</div>
								<div class='label_div'>
									<p>Vendors</p>
								</div>
					     </a>
							<br>
				    </li>
				    <li class="	{{ (Request::is('admin/vendortypes') || Request::is('admin/*/vendortypes') || Request::is('admin/vendortypes/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('vendortypes.index') }}">
								<div class='icon_div'>
									<i class="ti-vendortypes"></i>
								</div>
								<div class=n'label_div'>
									<p>Vendor Types</p>
								</div>
					    </a>
						<br>
				    </li>
				@endhasrole
			@elseif ($url_level == 'tools' )
				@if ( isset( $market_day ) )
					@php
						$vendor_id = isset( $vendor ) ? $vendor->id : '';
					@endphp

					{{--  Everyone gets Dashboard  --}}
					<li class="	{{ (Request::is('*/dashboard/*'))  || (Request::is('*/dashboard'))? 'active' : '' }} sidebar_link">
						<a href="{{ route('tools.dashboard', ['market_day' => $market_day->id]) }}">
							<div class='icon_div'>
								<i class="ti-dashboard"></i>
							</div>
							<div class='label_div'>
								<p>Dashboard</p>
							</div>
						</a>
						<br>
				    </li>

					{{-- Super Admin and Market Staff have access to vendor interactions --}}
					@hasrole(['superadmin','double_dollar_coordinator','double_dollar_associate'])
						<li class="	{{ (Request::is('*/distribute_scrip/*'))  || (Request::is('*/distribute_scrip'))? 'active' : '' }} sidebar_link ">
							<a href="{{ route('tools.distribute_scrip.index', ['market_day' => $market_day->id]) }}">
								<div class='icon_div'>
									<i class="ti-distribute_scrips"></i>
								</div>
								<div class='label_div'>
									<p>Distribute Scrip</p>
								</div>
							</a>
							<br>
					    </li>
					@endhasrole

					{{-- Super Admin and Market Staff have access to vendor interactions --}}
					@hasrole(['superadmin','market_crew','market_coordinator'])
						<li class=" {{ (Request::is('*/booth_fees/*')) || (Request::is('*/booth_fees')) ? 'active' : '' }} sidebar_link sidebar_link ">
							<a href="{{ route('tools.show_booth_fees', ['market_day' => $market_day->id]) }}">
								<div class='icon_div'>
									<i class="ti-booth_fees"></i>
								</div>
								<div class='label_div'>
									<p>Booth Fee</p>
								</div>
							</a>
							<br>
					    </li>


						<li class="{{ (Request::is('*/collect_scrips/*')) || (Request::is('*/collect_scrips')) ? 'active' : '' }} sidebar_link">
							<a href="{{ route('tools.show_collect_scrips', ['market_day' => $market_day->id]) }}">
								<div class='icon_div'>
									<i class="ti-scrips"></i></i>
								</div>
								<div class='label_div'>
									<p>Collect Scrip</p>
								</div>
							</a>
							<br>
					    </li>

						<li class=" {{ (Request::is('*/estimated_sales/*')) || (Request::is('*/estimated_sales')) ? 'active' : '' }} sidebar_link">
							<a href="{{ route('tools.show_estimated_sales', ['market_day' => $market_day->id]) }}">
								<div class='icon_div'>
									<i class="ti-estimated_sales"></i>
								</div>
								<div class='label_div'>
									<p>Estimated Sales</p>
								</div>
							</a>
							<br>
					    </li>
				    @endhasrole
				    <hr>
					<li class=" {{ (Request::is('*/reset/*')) ? 'active' : '' }} sidebar_link">
						<a href="{{ route('tools.reset') }}">
							<div class='icon_div'>
								<i class="ti-reset"></i>
							</div>
							<div class='label_div'>
								<p>Change Markets</p>
							</div>
						</a>
						<br>
				    </li>
				@endif
			{{-- *** REPORTS ** --}}
			@elseif ($url_level == 'reports' )
				@hasrole(['superadmin','double_dollar_coordinator'])

				    <li class="	{{ (Request::is('reports/distribute_scrip') || Request::is('reports/*/distribute_scrip') || Request::is('reports/distribute_scrip/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('reports.distribute_scrip_form') }}">
							<div class='icon_div'>
								<i class="ti-scrips"></i>
							</div>
							<div class='label_div'>
								<p>Scrip Distributed</p>
							</div>
				        </a>
						<br>
				    </li>
				@endhasrole
				@hasrole(['superadmin','market_coordinator'])
					<li class="	{{ (Request::is('reports/booth_fees') || Request::is('reports/*/booth_fees') || Request::is('reports/booth_fees/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('reports.booth_fees_form') }}">
								<div class='icon_div'>
									<i class="ti-booth_fees"></i>
								</div>
								<div class='label_div'>
									<p>Booth Fees</p>
								</div>
				        </a>
						<br>
				    </li>

				    <li class="	{{ (Request::is('reports/collect_scrip') || Request::is('reports/*/collect_scrip') || Request::is('reports/collect_scrip/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('reports.collect_scrip_form') }}">
								<div class='icon_div'>
									<i class="ti-scrips"></i>
								</div>
								<div class='label_div'>
									<p>Scrip Collected</p>
								</div>
				        </a>
						<br>
				    </li>


					<li class="	{{ (Request::is('reports/estimated_sales') || Request::is('reports/*/estimated_sales') || Request::is('reports/estimated_sales/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('reports.estimated_sales_form') }}">
								<div class='icon_div'>
									<i class="ti-estimated_sales"></i>
								</div>
								<div class='label_div'>
									<p>Estimated Sales</p>
								</div>
				        </a>
						<br>
				    </li>

				    <li class="	{{ (Request::is('reports/tableau_export') || Request::is('reports/*/tableau_export') || Request::is('reports/tableau_export/*') ? 'active' : '') }} sidebar_link ">
				        <a href="{{ route('reports.tableau_export_form') }}">
								<div class='icon_div'>
									<i class="ti-tableau_export"></i>
								</div>
								<div class='label_div'>
									<p>Tableau</p>
								</div>
				        </a>
						<br>
				    </li>


				@endhasrole
			@endif

		</ul>
</div>
@endif
