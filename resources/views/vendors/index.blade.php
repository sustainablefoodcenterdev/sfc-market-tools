@extends('layouts.app')

@section('content')

<div class="header">
    <h2 class="title">
        <i class="icon_div ti-vendors"></i>
        Vendors
        <a class="btn btn-primary" href="{{ route('vendors.create') }}">Add New</a>
        <br><small>Deleted vendors are shown at bottom <a href='#deleted'><i class="icon_div ti-link"></i></a></small>
    </h2>
</div>

<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
            <th>Vendor</th>
            <th>Type</th>
        	<th>Email</th>
        	<th>Phone</th>
        	<th>Receipt</th>
        	<th>Actions</th>
    	</tr>
    </thead>
    <tbody>
	@php
		$found = false;
	@endphp
    @foreach ($vendors as $vendor)

    	@if (! $found and ! empty($vendor->deleted_at) )
	        <tr class='success' style='text-align:center'>
		        <td colspan='7'><a name='deleted'>DELETED VENDORS</td>
	        </tr>
	        @php
				$found = true;
			@endphp
		@endif
        <tr>
            <td>
	            @if (! $found)
	            	<a href='{{ URL::route('vendors.edit', $vendor->id) }}'><button class="btn btn-lg btn-info" >Edit</button></a></td>
	            @endif
			<td>{{ $vendor->name }}</td>
        	<td>
	        	@foreach ($vendor_types as $vendor_type)
					@if ($vendor_type->id == $vendor->vendor_type_id)
						{{ $vendor_type->name }}<br/>
					@endif
				@endforeach
        	</td>
        	<td><a href='mailto:{{ $vendor->email }}'>{{$vendor->email}}</a></td>
        	<td>{{ $vendor->phone }}</td>
        	<td>
	        	<!---  icons for receipts :EL --->
	        	@if ($vendor->email_receipt === 1)
					<span class="icon_div ti-email"></span>
					&nbsp;&nbsp;
	        	@endif

	        	@if ($vendor->print_receipt === 1)
					<span class="icon_div ti-printer"></span>
	        	@endif
	        </td>
        	<td>
				@if (empty($vendor->deleted_at) )
					<form method="POST" action="{{ URL::route('vendors.destroy', $vendor->id) }}">
						<a href='{{ URL::route('vendors.show', $vendor->id) }}'><button class="btn btn-med btn-info" type='button'>View</button></a>
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<input type="submit" value="Hide" class="btn btn-med btn-info">
					</form>
				@else
					<form method="GET" action="{{ URL::route('vendors.restore', $vendor->id) }}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">

						<input type="submit" value="Unhide" class="btn btn-med btn-info">
					</form>
				@endif
	        </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection