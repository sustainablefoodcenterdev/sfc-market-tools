@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="content">
		        <form class="form-horizontal" action="{{ URL::route('vendors.update', $vendor->id) }}" method="post">
	                {{ csrf_field() }}
	                <input type="hidden" name="_method" value="PATCH">
					<div class="header">
						<h2 class="title">
						<span class="icon_div ti-vendors"></span>
							Edit Vendor {{ $vendor->name }}
						</h2>
					</div>

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

					<div class="row">

						<div class="col-sm-6">

							<div class="form-group">

								<div class="col-sm-12">
						            <label><p>Name</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-name"
				                        name="name"
				                        placeholder="Vendor Name"
				                        value="{{ old('name', $vendor->name) }}">
								</div>
					        </div>
					        <br clear="all"/>

							<div class="form-group">

								<div class="col-sm-12">
						            <label><p>Type</p></label>
						            <select
							            class="form-control input-lg"
							            id="vendor-type"
							            name="vendor_type_id">
										<option value="">Choose the type</option>
										@foreach ($vendor_types as $vendor_type)
											<option value="{{ $vendor_type->id }}" @if ($vendor_type->id == $vendor->vendor_type_id)  selected @endif>{{ $vendor_type->name }}</option>
										@endforeach
									    </select>
								</div>
					        </div>
					        <br clear="all"/>

					        <div class="form-group">

								<div class="col-sm-12">
						            <label><p>Email</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-email"
				                        name="email"
				                        value="{{ old('email', $vendor->email) }}">
								</div>
					        </div>
					        <br clear="all"/>

					        <div class="form-group">

								<div class="col-sm-12">
						            <label><p>Phone</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-phone"
				                        name="phone"
				                        value="{{ old('phone', $vendor->phone) }}">
								</div>
					        </div>
					        <br clear="all"/>

					        <!--- <div class="form-group">

								<div class="col-sm-12">
						            <label><p>Receipts</p></label>
						            <br>
						            <span class="input-lg form-control">
						            <label >
							            <input
					                        type="checkbox"
					                        id="email_receipt"
					                        name="email_receipt"
					                        value="1"
					                        @if ($vendor->email_receipt==1) checked @endif
					                        >
										Email
						            </label>
										&nbsp;&nbsp;|&nbsp;&nbsp;
									<label >
					                    <input
					                        type="checkbox"
					                        id="print_receipt"
					                        name="print_receipt"
					                        value="1"
					                        @if ($vendor->print_receipt==1) checked @endif
					                        >
					                    Print
						            </label>
								</div>
					        </div>
					        <br clear="all"/>
 :EL --->						</div>

						<div class="col-sm-6">


					        <div class="form-group">

								<div class="col-sm-12">
						            <label><p>Accepted Scrip</p></label>
						            @php $old_scrip_ids = old('accept_scrip_id_list', []); @endphp
						            @foreach ($scrips as $scrip)
										<label class="form-control form-control-inline">
											<span  style='width:30px;display:inline !important;background-color:{{$scrip->color }}'>
					                        	&nbsp;&nbsp;&nbsp;
					                        </span>
											&nbsp;
											<input
												name="accept_scrip_id_list[]"
												type="checkbox"
												value="{{ $scrip->id }}"
												@if (old('_method'))
													@if (in_array((string)$scrip->id, $old_scrip_ids, true))
														checked
													@endif
												@elseif ($vendor->scrips->where('pivot.accepted',true)->contains($scrip))
												checked
												@endif
												>
											{{ $scrip->name }}
										</label>
									@endforeach
								</div>
					        </div>
					        <br clear="all"/>

						</div>
					</div>

                    <div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Update
								</button>
								<a class="btn btn-warning" href="{{ URL::route('vendors.index') }}">Cancel</a>
							</label>
						</div>
						<br><br><br><br>
					</div>
				</form>
	            <a class="btn btn-default" href="{{ URL::route('vendors.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection
