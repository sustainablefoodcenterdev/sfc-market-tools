@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
	        <div class="content">
		        <div class="form-horizontal">
	                <label class='numbers'>{{ csrf_field() }}</label>
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-vendors"></span>
							View Vendor
						</h2>
					</div>

					<div class="form-group row">

					<!--- VENDOR INFO COLUMN :EL --->
					<div class="col-sm-7">
						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Name</p></label>
								<br>
								<label class='numbers'>{{ $vendor->name }}</label>
							</div>
				        </div>

						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Type</p></label>
								<br>
								<label class='numbers'>{{ $vendor->type->name }}</label>
							</div>
				        </div>

						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Email</p></label>
								<br>
								<label class='numbers'>{{ $vendor->email }}</label>
							</div>
				        </div>
						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Phone</p></label>
								<br>
								<label class='numbers'>
									{{ $vendor->phone }}
								</label>
							</div>
				        </div>


<!--- 						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Receipts</p></label>
								<br>
								<label class='numbers'>
									@if ($vendor->email_receipt == 1)  -Email @endif
									@if ($vendor->print_receipt == 1)  -Print @endif
								</label>
							</div>
				        </div>
 :EL --->					</div>

					<!--- ACCEPTED SCRIPS COLUMN :EL --->
					<div class="col-sm-5">
						<div class="form-group row">
							<div class="col-sm-12">
								<label class='setup_label'> Accepted Scrip</label>
								<br>
	                                @foreach ($vendor->scrips->where('pivot.accepted',true) as $scrip)
	                                    <label class='numbers'>
											<span class="form-control" style='width:30px;display:inline !important;background-color:{{$scrip->color }}'>
					                        	&nbsp;&nbsp;&nbsp;
					                        </span>

	                                    	{{ $scrip->name }}
	                                    </label>
	                                    <br>
	                                @endforeach

							</div>
				        </div>
					</div>

			        <div class="card-footer row">
						<div class="col-sm-9 col-sm-offset-3">
							<form method="POST" action="{{ URL::route('vendors.destroy', $vendor->id) }}">
							    <label class='numbers'>{{ csrf_field() }}</label>
							    <input type="hidden" name="_method" value="DELETE">
							    <a class="btn btn-fill btn-info" href="{{ route('vendors.edit', $vendor->id) }}">Edit</a>
							    <input type="submit" value="Delete" class="btn btn-fill btn-warning">
							</form>
						</div>
					</div>
		        </div>
	            <a class="btn btn-default" href="{{ URL::route('vendors.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection




