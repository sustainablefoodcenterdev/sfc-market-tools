@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
	        <div class="card">
		        <form class="form-horizontal" action="{{ route('vendors.store') }}" method="post">
	                {{ csrf_field() }}
					 <div class="header">
						<h2 class="title">
						<span class="icon_div ti-vendors"></span>
							Create Vendor
						</h2>
					</div>

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

					<div class="form-group row">
				        <div class="col-sm-6">


							<div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Name</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-name"
										name="name"
				                        placeholder="Vendor Name"
				                        value="{{ old('name') }}">
								</div>
					        </div>

							<div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Type</p></label>
						            <select
							            class="form-control form-control-lg"
							            type="text"
							            id="vendor-type"
							            name="vendor_type_id">
										<option value="">Choose the type</option>
										@foreach ($vendor_types as $vendor_type)
										{{-- if vendor_type == old data, select it --}}
										<option value="{{ $vendor_type->id }}" @if ($vendor_type->id ==  old('vendor_type_id') && old('vendor_type_id') != '') selected @endif>{{ $vendor_type->name }}</option>
										@endforeach
							        </select>
								</div>
					        </div>


							<div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Email</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-email"
				                        name="email"
				                        value="{{ old('email') }}"
				                        placeholder="john@example.com">
								</div>
					        </div>

					        <div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Phone</p></label>
						            <input
							            class="form-control input-lg"
							            type="text"
							            id="vendor-phone"
				                        name="phone"
				                        value="{{ old('phone') }}"
				                        placeholder="512-123-4567">
								</div>
					        </div>

					        <!--- <div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Receipts</p></label>

									<label class="form-control">
									<input
				                        type="checkbox"
				                        id="email_receipt"
				                        name="email_receipt"
				                        value="1"
				                        @if (old('email_receipt') == 1) checked @endif
				                        >
				                        Email
									</label>


									<label class="form-control">
				                    <input
				                        type="checkbox"
				                        id="print_receipt"
				                        name="print_receipt"
				                        value="1"
				                        @if (old('print_receipt') == 1) checked @endif
				                        >
				                        Print
									</label>
								</div>
					        </div> :EL --->

				        </div>

						<div class="col-sm-6">
					        <div class="form-group row">
						        <div class="col-sm-1">&nbsp;</div>
								<div class="col-sm-11">
						            <label><p>Accepted Scrips</p></label>
									@php $old_scrip_ids = old('accept_scrip_id_list', []); @endphp
						            @foreach ($scrips as $scrip)
										<label class="form-control form-control-inline">
											<span  style='width:30px;display:inline !important;background-color:{{$scrip->color }}'>
					                        	&nbsp;&nbsp;&nbsp;
					                        </span>
											&nbsp;
											<input
												name="accept_scrip_id_list[]"
												type="checkbox"
												@if (in_array((string)$scrip->id, $old_scrip_ids, true))
												checked
												@endif
												value="{{ $scrip->id }}">
											 {{ $scrip->name }}
										</label>
									@endforeach
								</div>
					        </div>
				        </div>
					</div>

			        <div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Save
								</button>
								<a class="btn btn-warning" href="{{ URL::route('vendors.index') }}">Cancel</a>
							</label>
						</div>
						<br><br><br><br>
					</div>

				</form>
	            <a class="btn btn-default" href="{{ URL::route('vendors.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection