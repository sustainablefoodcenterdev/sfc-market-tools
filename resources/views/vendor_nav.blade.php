<div class="row" style="background-color: #ddd;margin-top:-20px;padding-top:20px">
    <div class="col-md-8">
        @if ( isset($prev_vendor) )
        	<a class="btn btn-default vendor_nav_btn first_btn" href="{{ route('tools.' . $tool, ['market_day' => $market_day->id, 'vendor' => $prev_vendor->id]) }}">
	            <i class="icon_div ti-arrow-left"></i>
	            &nbsp;
	            {{ str_limit($prev_vendor->name, 15) }}
            </a>
        @else
        	<button class="btn btn-default vendor_nav_btn first_btn" disabled><i class="icon_div ti-arrow-left"></i>&nbsp; </button>
        @endif
        @if ( isset($next_vendor) )
            <a class="btn btn-default vendor_nav_btn" href="{{ route('tools.' . $tool, ['market_day' => $market_day->id, 'vendor' => $next_vendor->id]) }}">
               {{ str_limit($next_vendor->name, 15) }}
               &nbsp;
               <i class="icon_div ti-arrow-right"></i>
            </a>

        @else
        	<button class="btn btn-default vendor_nav_btn first_btn" disabled>&nbsp; <i class="icon_div ti-arrow-right"></i></button>
        @endif
    </div>

    <div class="col-md-4 vendor-search">
    	<div class="input-group">
    		<span class="input-group-addon"  style='background-color:#ddd;border:0px;'><i class="icon_div ti-search"></i></span>
    		<select class="vendor_search" name='vendor_search_id' class="form-control input-lg">
			</select>
    	</div>
    </div>
</div>

@section('scripts')
{{-- Outputs script tags from app.blade.php first --}}
	@parent
	<script>
        // data collection for <option> statements
        @if ( isset( $market_day ) )
        var data = {!! json_encode( $market_day->getVendorsForMultiSelect() ) !!};
        var base_route = "{{ route('tools.' . $tool, ['market_day' => $market_day->id, 'vendor' => 'vendor_placeholder']) }}";

        $('.vendor_search').selectize({
            placeholder: "Select a vendor",
            options: data
        });

        $(".vendor_search").on('change', function(event){
            var route = base_route.replace('vendor_placeholder', $(event.target).find(':checked').val());
            window.location.href = route;
        });
        @endif

	</script>
@endsection
