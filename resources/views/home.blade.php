@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
				{{-- if there's a market today, show link --}}
				@if ($today_market_days)
					<h1>{{ date('D M d, Y')}}</h1>
					@foreach ($today_market_days as $today_market_day)
						<h4><a href='{{ URL::route('tools.dashboard', $today_market_day->id) }}'><button class="btn btn-lg btn-info btn-fill" >Manage today's {{ $today_market_day->market->name}} market</button></a> </h4>
					@endforeach
				@else
					You are logged in!
				@endif

				{{-- DOUBLE DOLLAR --}}
				@hasrole(['double_dollar_coordinator','double_dollar_associate'])
					<img src="/img/logos/DoubleDollars_SFC_Logo400X340.jpg" align="left" hspace="10" width="200" height="200">
					@hasrole(['double_dollar_coordinator'])

						<form id="market_id_form" method="GET" class="form-horizontal" action="">

							<div class="row">
								<div class="col-md-8">
								<h1>Transaction Logs</h1>
								<select id="market_day_id" name="market_day_id" class="btn btn-lg btn-info btn-fill">
									<option>DOWNLOAD</option>
									@foreach ($past_market_days as $market_day)
										<option value="{{route('reports.distribute_scrip', $market_day->id)}})">{{$market_day->market->name }} - {{ date('D M d, Y', strtotime($market_day->date)) }}</option>
									@endforeach
								</select>
							</div>
						</form>
					@endhasrole


				@endhasrole

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
	@include('home_script')

@endsection
