@extends('layouts.app')

@section('content')

<div class="content clearfix">
		<div class="row">

			<div class="col-sm-12">
		        <h3 class="title">Scrip Distributed Report</h3>
			</div>
		</div>
        <hr>
	</div>
	<div class="row">
		<div class="col-sm-1">
			&nbsp;
		</div>
		<div class="col-sm-10">
			<form
				id="market_id_form"
	            method="GET"
	            class="form-horizontal"
	            action="">
	            {{ csrf_field() }}
	            <input type="hidden" name="_method" value="PATCH">

				<select name='market_day' id='market_day_select' required class="btn btn-lg select-info">
					<option value="">Select a Market Day</option>
					@foreach($market_days as $market_day)
					<option value="{{$market_day->id}}" data-route="{{ route('reports.distribute_scrip', $market_day->id) }}">{{$market_day->market->name}} on {{ $market_day->date->format('F d, Y')}}</option>
					@endforeach
				</select>
				<button class="btn btn-primary btn-fill btn-lg" type="submit">Download Excel Document</button>
			</form>

@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    @include('reports.market_day_select_scrip')
@endsection