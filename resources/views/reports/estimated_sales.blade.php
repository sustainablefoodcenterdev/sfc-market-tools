@extends('layouts.app')


@section('content')


	<div class="content clearfix">
		<div class="row">

			<div class="col-sm-5">
		        <h2 class="title"><i class="icon_div ti-estimated_sales"></i>Estimated Sales</h2>
			</div>
			<div class="col-sm-6">
				&nbsp;
			</div>
		</div>
        <hr>

	<div class="row">

		<div class="col-sm-6">
			{{-- make table responsive --}}
			<div class="table-responsive" style+"width">

				<table class="table table-hover table-striped">
				    <thead>
				        <tr class='success'>
				            <th>&nbsp;</th>
				            <th>Vendor</th>
				            <th>Sales</th>
				    	</tr>
				    </thead>
				    <tbody>

					@foreach ($estimated_sales as $vendor)
			        <tr>
						<td>


							<a class='btn btn-info' href='route("tools.show_estimated_sales_for_vendor")}}/{{$vendor["id"]}}'>
							Enter
							</a>
						</td>
						<td>{{ $vendor['name'] }}</td>
						<td>${{ $vendor['sales'] }}</td>
			        </tr>
					@endforeach
				    </tbody>
				</table>
			</div>

		</div>
	</div>



@endsection