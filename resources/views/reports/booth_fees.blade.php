@extends('layouts.app')


@section('content')


	<div class="content clearfix">
		<div class="row">

			<div class="col-sm-6">
		        <h2 class="title"><i class="icon_div ti-booth_fees"></i>Booth Fees Collected</h2>
			</div>
			<div class="col-sm-6">
				&nbsp;
			</div>
		</div>
        <hr>

	<div class="row">

		<div class="col-sm-6">
	{{-- make table responsive --}}
	<div class="table-responsive" style+"width">

		<table class="table table-hover table-striped">
		    <thead>
		        <tr class='success'>
		            <th>&nbsp;</th>
		            <th>Vendor</th>
		            <th>Fees</th>
		            <th>Booths</th>
		    	</tr>
		    </thead>
		    <tbody>

			@foreach ($booth_fees as $vendor)
	        <tr>
				<td>
					<a class='btn btn-info' href='route("tools.show_booth_fees_for_vendor")}}/{{$vendor["id"]}}'>					Enter
					</a>
				</td>
				<td>{{ $vendor['name'] }}</td>
				<td>${{ $vendor['fee'] }}</td>
				<td>{{ $vendor['booths'] }}</td>
	        </tr>
			@endforeach
		    </tbody>
		</table>
	</div>

		</div>
	</div>



@endsection