@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="content">
	            <form class="form-horizontal" action="{{ route('markets.update', $market->id) }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PATCH">
					<div class="header">
						<h2 class="title">
						<span class="icon_div ti-markets"></span>
							Edit Market: {{ $market->name }}
						</h2>
					</div>

					@if (count($errors) > 0)
				        <div class="alert alert-danger">
				            <ul>
				                @foreach ($errors->all() as $error)
				                    <li>{{ $error }}</li>
				                @endforeach
				            </ul>
				        </div>
				    @endif

			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
				            <label><p>Market</p></label>
				            <input
					            class="form-control input-lg"
					            type="text"
					            id="market-name"
					            name="name"
					            placeholder="Market Name"
					            value="{{ old('name', $market->name) }}"
					            size="10">
						</div>
			        </div>
			        <br clear="all"/>
			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
				            <label><p>Market Day</p></label>
				            <select
					            class="form-control input-lg"
					            type="text"
					            id="market-day"
					            name="day">
								<option value="">Choose the Day</option>
								@foreach ([0,1,2,3,4,5,6] as $day)
									<option value="{{ $day }}" @if ($day == $market->day) selected @endif >{{ jddayofweek($day-1, 1) }}</option>
								@endforeach
					        </select>
						</div>
			        </div>
			        <br clear="all"/>
			        <div class="form-group">
				        <div class="col-sm-1">&nbsp;</div>
						<div class="col-sm-4">
				            <label><p>Image</p></label>

			                <img src="/img/markets/{{ $market->image }}" width="180" height="120">
				            <input
				                class="form-control-file input-lg"
				                type="file"
				                id="market-image"
				                name="image"
				                value="">
						</div>
			        </div>
					<div class="card-footer">
						<div class="form-group">
							<div class="col-sm-9">
								<label class="col-sm-3 col-form-label col-form-label-lg">&nbsp;</label>
								<label class="col-sm-9">
								<button id="submit_button" type="submit" class="btn btn-fill btn-info btn_submit">
									Update
								</button>
								<a class="btn btn-warning" href="{{ URL::route('markets.index') }}">Cancel</a>
							</label>
						</div>
						<br><br><br><br>
					</div>
				</form>

				<a class="btn btn-default" href="{{ URL::route('markets.index') }}">View All</a>
			</div>
		</div>
	</div>
</div>
@endsection