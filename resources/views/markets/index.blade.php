@extends('layouts.app')

@section('content')


<div class="header">
    <h2 class="title">
        <i class="icon_div ti-markets"></i>
        Markets
        <a class="btn btn-primary" href="{{ route('markets.create') }}">Add New</a>
    </h2>
</div>

<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
            <th>Name</th>
        	<th>Image</th>
        	<th>Day of Week</th>
        	<th>Actions</th>
    	</tr>
    </thead>
    <tbody>

		@foreach ($markets as $market)
        <tr>
            <td><a href='{{ URL::route('markets.edit', $market->id) }}'><button class="btn btn-lg btn-info" >Edit</button></a></td>
			<td>{{ $market->name }}</td>
        	<td><img src='/img/markets/{{$market->image}}' width='80' height='60'></td>
        	<td>{{ jddayofweek($market->day-1, 2) }}</td>
        	<td>
		        <form method="POST" action="{{ URL::route('markets.destroy', $market->id) }}">
		        	<a href='{{ URL::route('markets.show', $market->id) }}'><button class="btn btn-med btn-info"  type="button">View</button></a>
				    {{ csrf_field() }}
				    <input type="hidden" name="_method" value="DELETE">
				    <input type="submit" value="Delete" class="btn btn-med btn-info">
				</form>
	        </td>

        </tr>
		@endforeach
    </tbody>
</table>
@endsection