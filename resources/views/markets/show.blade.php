@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
	        <div class="content">

				<div class="header">
					<h2 class="title">
					<span class="icon_div ti-markets"></span>
						View Market
					</h2>
				</div>

				<div class="form-group row">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-4">
						<label><p> {{ $market->name }} on {{ jddayofweek($market->day-1, 1) }}</p></label>
					</div>
		        </div>
				<div class="form-group row">
			        <div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-4">
						<img src="/img/markets/{{ $market->image }}" width="180" height="120">
					</div>
		        </div>

	            <div class="card-footer row">
					<div class="col-sm-9 col-sm-offset-3">
						<form method="POST" action="{{ route('markets.destroy', $market->id) }}">
						    {{ csrf_field() }}
						    <input type="hidden" name="_method" value="DELETE">
						    <a class="btn btn-fill btn-info" href="{{ route('markets.edit', $market->id) }}">Edit</a>
						    <input type="submit" value="Delete" class="btn btn-fill btn-warning">
						</form>
					</div>
				</div>

	            <a class="btn btn-default" href="{{ route('markets.index') }}">View All</a>
			</div>
        </div>
    </div>
</div>
@endsection



