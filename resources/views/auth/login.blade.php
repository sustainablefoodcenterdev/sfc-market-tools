@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="header">
				<h2 class="title">Login</h2>
			</div>
	        <div class="panel-body">
	            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
	                {{ csrf_field() }}

	                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
	                    <label for="username" class="col-md-4 control-label"><p>Username</p></label>

	                    <div class="col-md-6">
	                        <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

	                        @if ($errors->has('username'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('username') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                    <label for="password" class="col-md-4 control-label"><p>Password</p></label>

	                    <div class="col-md-6">
	                        <input id="password" type="password" class="form-control" name="password" required>

	                        @if ($errors->has('password'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('password') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>

	                <div class="form-group">
	                    <div class="col-md-6 col-md-offset-4">
	                        <div class="checkboxDELETE"> {{-- changed from checkbox to checkboxDELETE since former was causing the checkbox to be invisible --}}
	                            <label>
	                                <input type="checkbox" name="remember"> Remember Me
	                            </label>
	                        </div>
	                    </div>
	                </div>

	                <div class="form-group">
	                    <div class="col-md-8 col-md-offset-4">
	                        <button type="submit" class="btn btn-primary btn-fill btn_submit">
	                            Login
	                        </button>

	                        <!--- <a class="btn btn-link" href="{{ url('/password/reset') }}">
	                            Forgot Your Password?
	                        </a> :EL --->
	                    </div>
	                </div>
	            </form>
            </div>
        </div>
    </div>
</div>
@endsection
