@component('mail::message')
![Farmers market Logo][logo]
[logo]: {{asset('/img/logos/sfc_farmers_market_logo.png')}} "Farmers market Logo"
<p class='address'>
Sustainable Food Center |
2921 E. 17th Street, Building C |
Austin, TX 78702 |
512-236-0074
</p>
@component('mail::panel')
### Location: {{ $market_day->market->name }}
### Date: {{ $market_day->date->format('D, M jS, Y') }}<br>
### Vendor: {{ $vendor->name }}
### Type: {{ $vendor_type_name }}
@endcomponent


# Collected Scrip Receipt
This is your receipt for Scrip collected at today's market.

@component('mail::table')
| Scrip Name	| Pieces Collected | Total Amount |
| ------------- | ----------------:|----------------:|
@foreach($collected_scrips as $scrip_name => $collected)
| {{ $scrip_name }} | {{ $collected['pieces'] }} | ${{ $collected['amount'] }}|
@php
$total_pieces+= $collected['pieces'];
$total_amount+= $collected['amount'];
@endphp
@endforeach
| **Totals** | **{{ $total_pieces }}** | **${{ $total_amount }}**|
@endcomponent

@if ($vendor->pivot->collected_scrip_note)
### Note
@component('mail::panel')
{{ $vendor->pivot->collected_scrip_note }}
@endcomponent
@endif


Thanks,<br>
{{ config('app.name') }}
@endcomponent
