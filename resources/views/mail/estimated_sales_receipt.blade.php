@component('mail::message')
![Farmers market Logo][logo]
[logo]: {{asset('/img/logos/sfc_farmers_market_logo.png')}} "Farmers market Logo"
<p class='address'>
Sustainable Food Center |
2921 E. 17th Street, Building C |
Austin, TX 78702 |
512-236-0074
</p>
@component('mail::panel')
### Location: {{ $market_day->market->name }}
### Date: {{ $market_day->date->format('D, M jS, Y') }}<br>
### Vendor: {{ $vendor->name }}
### Type: {{ $vendor_type_name }}
@endcomponent


# Collected Scrip Receipt
This is your receipt for Estimated Sales at today's market.
## Estimated Sales    	  ${{ $vendor->pivot->estimated_sales  }}

@if ($vendor->pivot->estimated_sales_note)
### Note
@component('mail::panel')
{{ $vendor->pivot->estimated_sales_note }}
@endcomponent
@endif



Thanks,<br>
{{ config('app.name') }}
@endcomponent
