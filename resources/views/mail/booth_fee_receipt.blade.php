@component('mail::message')

![Farmers market Logo][logo]
[logo]: {{asset('/img/logos/sfc_farmers_market_logo.png')}} "Farmers market Logo"

<p class='address'>
Sustainable Food Center |
2921 E. 17th Street, Building C |
Austin, TX 78702 |
512-236-0074
</p>
@component('mail::panel')
### Location: {{ $market_day->market->name }}
### Date: {{ $market_day->date->format('D, M jS, Y') }}<br>
### Vendor: {{ $vendor->name }}
### Type: {{ $vendor_type_name }}
@endcomponent
# Booth Receipt
@component('mail::table')
|               		 |          |
| ---------------------- | --------:|
| Fee / booth      		 | ${{ $default_booth_fee }} |
| Number of  Booths      		 | {{ $vendor->pivot->booths_paid_for }} |
| Booth Fees Collected   | ${{ $vendor->pivot->booth_fee }} |
@endcomponent

@if ($vendor->pivot->booth_fee_note)
### Note
@component('mail::panel')
{{ $vendor->pivot->booth_fee_note }}
@endcomponent
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent
