@extends('layouts.app')


@section('content')


	<div class="content clearfix">
		<div class="row">

			<div class="col-sm-5">
		        <h2 class="title"><i class="icon_div ti-estimated_sales"></i>Estimated Sales</h2>
			</div>
			<div class="col-sm-6">
			</div>
		</div>
        <hr>
	</div>
{{--	<a class="btn btn-primary" href="{{ route('tools.show_estimated_sales_for_vendor', ['market_day' => $market_day->id, 'vendor' => $vendor->id]) }}">First Vendor</a>
	<br><br>
	--}}
		    @include('vendor_search', ['tool' => 'show_estimated_sales_for_vendor'])
	<div class="row">

		<div class="col-sm-10">
			{{-- make table responsive --}}
			<div class="table-responsive" style+"width">

				<table class="table table-hover table-striped">
				    <thead>
				        <tr class='success'>
				            <th>&nbsp;</th>
				            <th>Vendor</th>
				            <th>Sales</th>
				            <th>Note</th>
				    	</tr>
				    </thead>
				    <tbody>

					@foreach ($estimated_sales as $estimated_sale)
			        <tr>
						<td>
							@if ($estimated_sale['attended_market'])
							<a class="btn btn-info btn-lg" href="{{ route('tools.show_estimated_sales_for_vendor', ['market_day' => $market_day->id, 'vendor' => $estimated_sale['id']]) }}">
							Enter
							</a>
							@else
								<small class='text-warning'>Not at market</small>
							@endif
						</td>
						<td>{{ $estimated_sale['name'] }}</td>
						<td>
							@if ($estimated_sale['attended_market'])
								${{ $estimated_sale['sales'] }}
							@else
								$--
							@endif


						</td>
						<td>{{ $estimated_sale['note'] }}</td>
			        </tr>
					@endforeach
				    </tbody>
				</table>
			</div>

		</div>
	</div>



@endsection