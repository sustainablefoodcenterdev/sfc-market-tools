@extends('layouts.app')

@section('content')
    <div class="content clearfix">
		<h2 class="title">
			<i class="icon_div ti-booth_fees"></i>Booth Fee for <b> {{ $vendor->name }}</b>

	          <button class="btn btn-default vendor_nav_btn tight btn_spacer" type='button' id="absent_button" data-button_type='NCNS'>
		          NCNS
            </button>
	        <button class="btn btn-default vendor_nav_btn tight btn_spacer"  type='button' id="absent_button" data-button_type='Called'>
		          Called
            </button>

		</h2>

        <hr>
		<br><br>


        <form id="booth_fee_form" class="form-horizontal" method="POST" action="{{ route('tools.show_booth_fees_for_vendor', ['market_day' => $market_day->id, 'vendor' => $vendor->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PATCH">
            <input type="hidden" name="attended_market" id="attended-market" value="1">

			@if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <label for="booth-fees" class="col-sm-2 control-label">Booth Fees</label>
                <div class="col-sm-4">
    				<div class="input-group no-margin">
    					<span class="input-group-addon"><i class="icon_div ti-money"></i></span>
    					<input
                            class="form-control input-lg"
                            type="number"
                            id="booth-fees"
                            name="booth_fee"
                            placeholder="Booth Fees"
                            value="{{ old('booth_fee', $vendor->pivot->booth_fee) }}"
    						style="width:200px">
    				</div>
                </div>
                <div class="col-sm-4">
                @if($default_booth_fee)
                    <a class="btn btn-default btn-lg add-default-fee" data-booth-fee="{{ $default_booth_fee }}" href="#">
                        Default Fee = ${{ $default_booth_fee }}
                    </a>
				@else
                    <span class="text-warning" data-booth-fee="{{ $default_booth_fee }}" href="#">
                        No Default Fee set for this market
                    </span>
                @endif
                </div>
            </div>
            <div class="form-group">
                <label for="booths-paid-for" class="col-sm-2 control-label"># of Booths</label>
                <div class="col-sm-10">
    				<input
                        class="form-control input-lg"
                        type="number"
                         min="0"
                        id="booths-paid-for"
                        name="booths_paid_for"
                        placeholder="# of Booths"
                        value="{{ old('booths_paid_for', $vendor->pivot->booths_paid_for) }}"
    					style="width:239px">
                </div>
            </div>

            <div class="form-group">
				<label for="note" class="col-sm-2 col-sm-offset-0">
				<a href="#" class="btn btn-info btn-lg" id="note_button">Add Note</a></label>
<div class="col-sm-4" id="notes" class="btn btn-info">


                </div>
            </div>

            <br>

            <div class="form-group">
                <div class="col-sm-12">
        			<input type="submit" value="Save & Email" class="btn btn-primary btn-lg btn-fill btn_spacer" @if ($vendor->print_receipt) target="_blank" @endif>
                </div>
			</div>

        </form>
		<br><br>



    </div>
@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    <script>
        $(document).ready(function($) {

	        // init the use of default fee button if there'e an existing value that is a multiple of the defulat fee
			var this_paid_fee_value = parseInt($('#booth-fees').val());
			var this_default_fee_value = parseInt($('.add-default-fee').data('booth-fee'));
			if (this_paid_fee_value % this_default_fee_value == 0) {
				var default_fee_button_used=true;
			}
			else {
				var default_fee_button_used=false;
			}

			$('#note_button').click( function(event) {
				event.preventDefault();
				// remove button
				$(this).remove();
				// show the notes
				$('#notes').html('<textarea class="form-control input-lg" id="booth-fees-note" name="booth_fee_note" placeholder="Notes...">{{ old('booth_fee_note', $vendor->pivot->booth_fee_note) }}</textarea>');
			})

            $('.add-default-fee').on('click', function(event) {
                event.preventDefault();
                // if there's a number already, use it.
				if ( $.isNumeric($('#booths-paid-for').val()) && $('#booths-paid-for').val() != 0 ) {
					this_booth_count = $('#booths-paid-for').val();
				}
				else {
					this_booth_count = 1;
				}

				$('#booths-paid-for').val(this_booth_count);

                $('#booth-fees').val( $(this).data('booth-fee')*this_booth_count );
				// raise flag
				default_fee_button_used=true;
				// if CALLED or NCNS is in notes, remove it
				if ( $('#booth-fees-note').length )  {
					var this_note_text = $('#booth-fees-note').val();
					if (this_note_text.toLowerCase() == 'ncns' || this_note_text.toLowerCase() == 'called') {
						$('#booth-fees-note').val('');
					}
				}

            });
			@if ($vendor->pivot->booth_fee_note != '')
				$('#note_button').click();
			@endif

			// NCNS & Called button
			$('button[id=absent_button]').click(function (){
				// set value to blank
				$('#booth-fees').val('0');
				$('#booths-paid-for').val('0');
				$('#attended-market').val('0');

				// set notes to data-button_type
				$('#note_button').click();
				$('#booth-fees-note').val($(this).data('button_type'));

				// submit form
//				$('#booth_fee_form').submit();
			})

			// if change the # booths and default_fee_button_used is true, update booth_fee
		    $('#booths-paid-for').on('keyup change click', function(event) {
				if (default_fee_button_used) {
					// multiply booth paid for times default fee and add to booth-fees.val
					var this_booth_count=parseInt(this.value);
					if ($.isNumeric(this_booth_count) && $.isNumeric( this_default_fee_value) ) {
						$('#booth-fees').val(this_booth_count*this_default_fee_value);
					}
				}

			})

		});
    </script>
@endsection