@extends('layouts.app')

@section('content')

<style>
.scrip_cell {width:60px;float:left;text-align:right;}
.scrip_cell.left {text-align:left;}

</style>
    <div class="header">
        <h2 class="title">
            <i class="icon_div ti-money"></i>Distribute Scrip
            <a class="btn btn-primary" href="{{ route('tools.distribute_scrip.create', ['market_day' => $market_day]) }}">Add New</a>
        </h2>
    </div>

    <table class="table table-hover table-striped">
        <thead>
            <tr class='success'>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Zip</th>
                <th>Scrip</th>
                <th>Notes</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
			{{-- for each customer... --}}
            @foreach ($market_day->scrip_sales as $scrip_sale)
            <tr>
                <td><a href="{{ route('tools.distribute_scrip.edit', ['market_day' => $market_day->id, 'scrip_sale' => $scrip_sale->id]) }}" class="btn btn-lg btn-info" >Edit</a></td>
                <td>{{ $scrip_sale->name }}</td>
                <td>{{ $scrip_sale->zip_code }}</td>
                <td>
	                {{-- for each scrip customer bought... --}}
	                @php
	                	// inint vars for this scrip
	                	$last_four_digits='';
	                	$scrip_total=[];
	                @endphp
					@foreach ($distributed_scrip_types as $scrip_type => $items)
						@foreach ($items['ids'] as $this_id)
							@if ($scrip_sale->id && $scrip_sale->scrips()->wherePivot('scrip_id', $this_id)->first())
								@php
									// create new index if not already exists
									if (! array_key_exists($scrip_type,$scrip_total)) {
										$scrip_total[$scrip_type]=[];
										$scrip_total[$scrip_type]['total']=0;
									}
									// assign digits to array (set more than once: inefficient)
									$last_four_digits=$scrip_sale->scrips()->wherePivot('scrip_id', $this_id)->first()->pivot->card_number;

									// amount * denomination
									$scrip_total[$scrip_type]['total'] += $scrip_sale->scrips()->wherePivot('scrip_id', $this_id)->first()->pivot->amount * $items['denominations'][$loop->index];
								@endphp
								<b>
								</b>
						    @endif
						@endforeach
						@if ( array_key_exists($scrip_type,$scrip_total) )
						<div class='scrip_cell'>{{ $scrip_type }}</div><div class='scrip_cell'> {{ $last_four_digits }}</div><div class='scrip_cell left'> = ${{$scrip_total[$scrip_type]['total']}}</div><br>
						@endif
					@endforeach

                </td>
                <td>{{ $scrip_sale->distribute_scrip_note }}</td>
                <td>
					 <form method="POST" action="{{ URL::route('tools.distribute_scrip.destroy', [$market_day->id, $scrip_sale->id]) }}">
					    {{ csrf_field() }}
					    <input type="hidden" name="_method" value="DELETE">
					    <input type="submit" value="Delete" class="btn btn-med btn-info">
					</form>
                </td>


            </tr>
            @endforeach
        </tbody>
    </table>

@endsection
