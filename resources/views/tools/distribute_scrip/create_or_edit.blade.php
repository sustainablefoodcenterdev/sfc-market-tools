@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <div class="card">
            @if (isset($scrip_sale->id))
                <form method="POST" action="{{ route('tools.distribute_scrip.update', ['market_day' => $market_day->id, 'scrip_sale' => $scrip_sale]) }}">
                    <input type="hidden" name="_method" value="PATCH">
            @else
                <form method="POST" action="{{ route('tools.distribute_scrip.store', ['market_day' => $market_day->id]) }}">
            @endif
                {{ csrf_field() }}

                <div class="header">
                    <h4 class="title">
                        Distribute Scrip
                    </h4>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">

                                <div class="form-group">
                                    <label>Name</label>
                                    <input
                                            class="form-control"
                                            type="text"
                                            id="name"
                                            name="name"
                                            placeholder="Name"
                                            value="{{ old('name', $scrip_sale->name) }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">

                                    <label>Zip Code</label>
                                    <input
                                            class="form-control"
                                            type="text"
                                            id="zip_code"
                                            name="zip_code"
                                            placeholder="Zip"
                                            value="{{ old('zip_code', $scrip_sale->zip_code) }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>First Time?</label>

                                    <br clear="all">
                                    <Label class="form-control">
                                        <input
                                            class=""
                                            type="checkbox"
                                            id="first_market_visit"
                                            name="first_market_visit"
                                            @if (old('first_market_visit', false) || $scrip_sale->first_market_visit) checked="checked" @endif
                                            value="1">
                                            Yes
                                    </Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
					<fieldset>
                    <div class="row">
                        <div class="col-md-5">

                            <div class="form-group">
                                @foreach ($distributed_scrip_types as $scrip_type => $item)

                                    <div class="col-md-6">{{ $scrip_type }}
                                        <div class="input-group">
                                            <input
                                                class="form-control"
                                                data-row_type="{{ $scrip_type }}"
                                                type="number"
                                                name="scrip_type_digits[{{ $scrip_type }}]"
                                                id="scrip_type_digits"
                                                @if ($scrip_sale->id && $scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][2])->first())
                                                value="{{ $scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][2])->first()->pivot->card_number }}"
                                                @else
                                                value="{{ old('scrip_type_digits.' . $scrip_type, '') }}"
                                                @endif
                                                placeholder="last 4 digits"
                                                data-row_count='{{ $loop->index }}'>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <br>
                                        <div class="input-group">
                                            @if ($scrip_type != 'FMNP') <span class="input-group-addon">$</span>@endif
                                            <input
                                                class="form-control"
                                                name="dollar_amount[{{ $scrip_type }}]"
                                                type="number"
                                                data-row_type="{{ $scrip_type }}"
                                                data-denomination="{{$item['denominations'][1]}}"
                                                id="scrip_amount"
                                                value="{{ old('scrip_type_digits.' . $scrip_type, '') }}"
                                                placeholder="@if ($scrip_type == 'FMNP') # of booklets @else dollars @endif"
                                                min="0"
                                                inputmode="numeric"
                                                pattern="[0-9]*"
                                                data-row_count='{{ $loop->index }}'>
                                        </div>
                                    </div>
								@endforeach

                            </div>
                        </div>
						<div class="col-md-6 scrip_contrast">
							<div class="form-group">
								@foreach ($distributed_scrip_types as $scrip_type => $item)

	                                <div class="col-md-4">
										<div class="input-group">

		                                    {{-- show EBT for non - FMNP --}}
		                                    @if ( $scrip_type != 'FMNP')
		                                    	$1
		                                        EBT
		                                    @else
		                                    	FMNP value
		                                    @endif

		                                    <input class="form-control"
                                                data-row_type="{{ $scrip_type }}"
                                                type="number"
                                                name="scrip_amount_ones[{{$item['ids'][0]}}]"
                                                id="scrip_amount_ones"
                                                @if ($scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][0])->first())
	                                                value="{{ $scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][0])->first()->pivot->amount }}"
                                                @else
													value="{{ old('scrip_amount_ones.' . $item['ids'][0], '') }}"
                                                @endif
                                                inputmode="numeric"
                                                pattern="[0-9]*"
                                                @if ($item['denominations'][1] == '')readonly @endif
                                                data-denomination="{{$item['denominations'][0]}}"
                                                data-row_count='{{ $loop->index }}'>
		                                    <input type="hidden" name="scrip_id_ones[]" value="{{$item['ids'][0]}}">
										</div>
	                                </div>
	                                <div class="col-md-4">
										<div class="input-group">
		                                    @if ($item['denominations'][1] != '')
		                                        $5 EBT
		                                        <input
                                                    class="form-control"
                                                    data-row_type="{{ $scrip_type }}"
                                                    type="number"
                                                    name="scrip_amount_fives[{{$item['ids'][1]}}]"
                                                    id="scrip_amount_fives"
                                                    @if ($scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][1])->first())
                                                    	value="{{ $scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][1])->first()->pivot->amount }}"
                                                    @else
														value="{{ old('scrip_amount_fives.' . $item['ids'][1], '') }}"
                                                    @endif
                                                    inputmode="numeric"
                                                    pattern="[0-9]*"
                                                    data-denomination="{{$item['denominations'][1]}}"
                                                    data-row_count='{{ $loop->index }}'>
		                                        <input type="hidden" name="scrip_id_fives[]" value="{{$item['ids'][1]}}">
		                                    @endif
										</div>
	                                </div>
	                                <div class="col-md-4">
	                                    DD
	                                    <input
                                            readonly
                                            class="form-control"
                                            data-row_type="{{ $scrip_type }}"
                                            type="number"
                                            name="scrip_amount_dd[{{$item['ids'][2]}}]"
                                            id="scrip_amount_dd"
                                            @if ($scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][2])->first())
                                            value="{{ $scrip_sale->scrips()->wherePivot('scrip_id', $item['ids'][2])->first()->pivot->amount }}"
                                            @else
												value="{{ old('scrip_amount_dd.' . $item['ids'][2], '') }}"
                                            @endif
                                            inputmode="numeric"
                                            pattern="[0-9]*"
                                            data-row_count='{{ $loop->index }}'>
	                                    <input type="hidden" name="scrip_id_dd[]" value="{{$item['ids'][2]}}">

	                                </div>
	                                <br>
	                                <br clear="all">
									@endforeach
							</div>
                        </div>

					</fieldset>
                </div>

                <div class="row">

					<div class="col-md-12">
						<div class="col-md-5">
                            @unless (old('distribute_scrip_note')) <a href='#' id="notes"  class="btn btn-info btn-lg">Add Note</a> @endunless
                            <textarea @unless (old('distribute_scrip_note')) style="display: none;" @endunless class="form-control input-lg" id="distribute-scrip-note" name="distribute_scrip_note" placeholder="Notes...">{{ old('distribute_scrip_note', '') }}</textarea>


                        </div>
                        <div class="col-md-6">
                        </div>
					</div>
                </div>
<br clear="all">
<br clear="all">
                <div class="row">
                        <div class="col-md-1">
	                        &nbsp;
                        </div>
                        <div class="col-md-2">
                            <button type="submit" id='process_button' class="btn btn-fill btn-info  btn-lg">Process</button>
                        </div>
                        <div class="col-md-3">
                            <span id="submit_text" class="alert-warning"></span>
                        </div>
                </div>
                <br>
            </form>
        </div> <!-- end card -->
    </div> <!--  end col-md-12  -->
@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent

    <script language="JavaScript1.2" type="text/javascript">
    // <!--
    $(document).ready(function() {
        // show total: denomination * quantity
        $('input[id$=scrip_amount]').keyup( function()  {

            // define script type id
            var this_row=$(this).data('row_type');

            // define value
            var this_value=$(this).val();
            // max of 30 for DD
            if (this_value >= 30 && this_row != 'FMNP') {
                var this_dd_value = 30;
            }
            else {
                var this_dd_value = this_value;
            }

            // define denomination
            var this_denomination=$(this).data('denomination');
            /*** show number of pieces of scrip to give in current and $1 denomination *///
            //  if value is number, find number of peices for the denominations and remove any errors on row

            if ( $.isNumeric(this_value) ) {
                // if denomination is not empty, then there's more than one.
                if (this_denomination != '') {
                    $('input[data-row_type='+this_row+'][id=scrip_amount_fives]').val(Math.floor(this_value/this_denomination)).removeClass('alert-warning');
                    $('input[data-row_type='+this_row+'][id=scrip_amount_ones]').val(this_value % this_denomination).removeClass('alert-warning');
                    $('input[data-row_type='+this_row+'][id=scrip_amount_dd]').val(this_dd_value).removeClass('alert-warning');
                }
                else{
                    // denomination is empty, so only ones
//                  var this_html='<p><b>'+this_value+' ones</b>.</p>'
                    if (this_row == 'FMNP') {
                        var this_denomination = 30;
                    }
                    else {
                        var this_denomination = 1;
                    }

                    $('input[data-row_type='+this_row+'][id=scrip_amount_ones]').val(this_value*this_denomination).removeClass('alert-warning');
                    $('input[data-row_type='+this_row+'][id=scrip_amount_dd]').val(this_dd_value*this_denomination).removeClass('alert-warning');

                }

           }
        })

        // notes section
        $('#notes').click( function(event) {
            event.preventDefault();
            $(this).siblings('textarea').slideDown();
            $(this).hide();
        });

        // update dollar cell if scrips fields are changed
        $('input[id=scrip_amount_ones], input[id=scrip_amount_fives]').change( function() {

            // define script type id
            var this_row=$(this).data('row_type');
			var this_dollar_amount = parseInt($('input[data-row_type='+this_row+'][id$=scrip_amount]').val());

//				$('#process_button').attr('disabled',false);

			if (isNaN(this_dollar_amount) || this_dollar_amount <= 0) {
				this_dollar_amount=0;
			}
			var this_denomination=parseInt($(this).data('denomination'));
			var this_scrip_value = parseInt($(this).val());
			if 	(this_denomination != 1) {
				this_scrip_value=this_scrip_value*this_denomination;
			}

			// if  amount more than #scrip_amount value, error
			if (this_scrip_value > this_dollar_amount) {
				$(this).addClass(' alert-warning');
//					$('#process_button').attr('disabled','disabled');
			}

			else {
			// else amount is less than #scrip_amount value, so determine other denomination's value

				// determine the other denomination's value
				if 	(this_denomination != 1) {
					// add the remaning dollar amount not covered in fives to ones
					$('input[data-row_type='+this_row+'][id=scrip_amount_ones]').val(this_dollar_amount - this_scrip_value);
					// clear errors
					$('input[data-row_type='+this_row+'][id$=scrip_amount_ones]').removeClass(' alert-warning');
					$('input[data-row_type='+this_row+'][id$=scrip_amount_fives]').removeClass(' alert-warning');
					$('#submit_text').text('');

				} // denom != 1
				else {
					// see if remaining fives is an integer
					var remaining_fives = ( (this_dollar_amount - this_scrip_value)/5);
					if (Number.isInteger(remaining_fives) ) {
						$('input[data-row_type='+this_row+'][id=scrip_amount_fives]').val(remaining_fives);
						// clear errors
						$('input[data-row_type='+this_row+'][id$=scrip_amount_ones]').removeClass(' alert-warning');
						$('input[data-row_type='+this_row+'][id$=scrip_amount_fives]').removeClass(' alert-warning');
						$('#submit_text').text('');
					}
					else {
						//not an integer, so error
						$(this).addClass(' alert-warning');
//							$('#process_button').attr('disabled','disabled');
					}
				} // else demonination ==  1

			} // else not too large

        });

		$('#process_button').click(function() {
			if ($(".alert-warning:visible").length > 0) {
		        // show alert
		        $('#submit_text').text('There is an error on the page.');
		        return false;
		    }
		});

		// ** populate dollars on page load **
		// set the FMNP  ones value to be the same as DD value
		$('input[id=scrip_amount_ones][data-row_type=FMNP]').val($('input[id=scrip_amount_dd][data-row_type=FMNP]').val());


		// loop over all #scrip_amount_ones
		$('input[id=scrip_amount_ones]').each( function(i) {
			// if it has data,
			if (this.value != '') {
				// get the current row
				this_row=$(this).data('row_count');
				// get ones data
				var this_amount=parseInt(this.value);
				// check if $5 or DD have data and add
				if ($('input[id=scrip_amount_fives][data-row_count='+this_row+']').val() > 0) {
					this_amount+=parseInt(5*$('input[id=scrip_amount_fives][data-row_count='+this_row+']').val());
				}
				// else if FMPB, divide by 30
				else if ( $(this).data('row_type') == 'FMNP') {
					this_amount=this_amount/30;
				}

				// set value
				$('input[id=scrip_amount][data-row_count='+this_row+']').val(this_amount);

			} // if it has data,


		}) // loop over each #scrip_amount_ones


    });
    // -->
    </script>
@endsection