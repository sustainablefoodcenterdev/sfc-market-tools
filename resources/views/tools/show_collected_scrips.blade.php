@extends('layouts.app')

@section('content')
<div class="content clearfix">
	<div class="row">

		<div class="col-sm-5">
	        <h2 class="title"><i class="icon_div ti-scrips"></i>Collect Scrip</h2>
		</div>
		<div class="col-sm-6">
			&nbsp;
		</div>
	</div>
    <hr>
</div>




@include('vendor_search', ['tool' => 'show_collect_scrips_for_vendor'])

<div class="row">

	<div class="col-sm-10">

{{--		<a class="btn btn-primary" href="{{ route('tools.show_collect_scrips_for_vendor', ['market_day' => $market_day->id, 'vendor' => $vendor->id]) }}">First Vendor</a>
		<br><br>
--}}
		{{-- make table responsive --}}
		<div class="table-responsive" style+"width">

			<table class="table table-hover table-striped">
			    <thead>
			        <tr class='success'>
			            <th>&nbsp;</th>
			            <th>Vendor</th>
			            <th>Scrip</th>
			            <th>Notes</th>
			    	</tr>
			    </thead>
			    <tbody>
{{-- 				@foreach ($market_day->vendors_scrip_collected_info as $vendor_name => $scrips_collected) --}}
					@foreach ($market_day->vendors()->orderBy("name")->get() as $vendor)

					@php ($scrips_collected=$market_day->vendors_scrip_collected_info[$vendor->name])
			        <tr>
				        <td>
					        @if ($vendor->pivot->attended_market)
								<a class='btn btn-info btn-lg' href="{{ route('tools.show_collect_scrips_for_vendor', ['market_day' => $market_day->id, 'vendor' => $scrips_collected['id']]) }}">
								Enter
								</a>
							@else
								<small class='text-warning'>Not at market</small>
							@endif
				        </td>
						<td>
							{{$vendor->name}}
						</td>
						<td>
							<ul>
							@if (! $vendor->pivot->attended_market)
								--
							@else
								@foreach ($scrips_collected['scrip_collected'] as $scrip_name => $scrip_count)
									<li>{{ $scrip_name }} | {{ $scrip_count['pieces'] }} pieces</li>
								@endforeach
							@endif
							</ul>
						</td>
						<td>
							{{$vendor->pivot->collected_scrip_note}}
						</td>

			        </tr>
					@endforeach
			    </tbody>
			</table>
		</div>

	</div>
</div>


@endsection