@extends('layouts.app')


@section('content')

    <div class="content clearfix">
		<div class="row">

			<div class="col-sm-5">
		        <h2 class="title">Current Market Status</h2>
			</div>
			<div class="col-sm-6">
				&nbsp;
			</div>
		</div>
        <hr>

		@hasrole(['superadmin','double_dollar_coordinator','double_dollar_associate'])
			@if ($market_day->scrip_comments != '')
				<div class="row">
					<div class="col-sm-12">
			            <div class="col-sm-11 bg-info"  style="padding:15px;">
							<small><u>DDIP Comments:</u></small><br>
			                {{ $market_day->scrip_comments }}
			            </div>
					</div>
		        </div>
				<hr>
			@endif
		@endhasrole

		@hasrole(['superadmin','market_coordinator','market_crew'])
			@if ($market_day->market_comments != '')
				<div class="row">
					<div class="col-sm-12">
			            <div class="col-sm-11 bg-info"  style="padding:15px;">
				            <small><u>Market Comments:</u></small><br>
			                {{ $market_day->market_comments }}
			            </div>
					</div>
		        </div>
				<hr>
			@endif

			<div class="row">
	            <div class="col-sm-9 col-sm-offset-1">
				<div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr  class='success'>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Completed</th>
							<th>Vendors</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
							<td>&nbsp;</td>
							<td>
								<a href="{{ route('tools.show_booth_fees', ['market_day' => $market_day->id]) }}">
									<i class="icon_div ti-booth_fees"></i>Booth Fees
								</a>
							</td>
							<td>
								<span class="@if( $vendor_attended_total - $market_day->collected_booth_fee_count  > 0) text-warning @endif">
			                        {{ $market_day->collected_booth_fee_count }}
			                    </span>
								@hasrole(['superadmin','market_coordinator'])
				                    @ ${{ $market_day->total_booth_fees }}
				                @endhasrole
							</td>
							<td>{{ $vendor_expected_up_total }} expected</td>
                        </tr>
                        <tr >
							<td>&nbsp;</td>
							<td>
								<a href="{{ route('tools.show_collect_scrips', ['market_day' => $market_day->id]) }}">
									<i class="icon_div ti-scrips"></i>Scrip Collected
								</a>
							</td>
							<td>
								<span class="@if($market_day->vendors_that_can_collect_scrip_count - $market_day->vendors_that_collected_scrip_count > 0) text-warning @endif">
			                        {{ $market_day->vendors_that_collected_scrip_count }}
			                    </span>
								@hasrole(['superadmin','market_coordinator'])
				                    @ ${{ $market_day->collected_scrip_total }}
				                @endhasrole
							</td>
							<td>{{ $vendor_attended_total }} present</td>
                        </tr>
							<td>&nbsp;</td>
							<td>
								<a href="{{ route('tools.show_estimated_sales', ['market_day' => $market_day->id]) }}">
									<i class="icon_div ti-estimated_sales"></i>Estimated Sales
								</a>
							</td>
							<td>
								<span class="@if($vendor_attended_total - $market_day->estimated_sales_count > 0) text-warning @endif">
			                        {{ $market_day->estimated_sales_count }}
			                    </span>
								@hasrole(['superadmin','market_coordinator'])
				                    @ ${{ $market_day->estimated_sales_total }}
								@endhasrole
							</td>
							<td>{{ $vendor_attended_total }} present</td>
                    </tbody>
                </table>
				</div>
	            </div>
			</div>

		@endhasrole

		{{-- admin see both, so give a line break --}}
		@hasrole('superadmin')
			<br clear="all">
		@endhasrole


		@hasrole(['superadmin','double_dollar_coordinator','double_dollar_associate'])

	        <div class="row">
	            <div class="col-sm-9 col-sm-offset-1">
		            <div class="table-responsive">
		                <table class="table table-hover table-striped">
		                    <thead>
		                        <tr  class='success'>
		                            <th>Scrip Name</th>
		                            <th>Brought</th>
		                            <th>Distributed</th>
		                            <th>Remaining</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        @foreach ($scrips as $scrip)
		                            <tr>
		                                <td>{{ $scrip->name }}</td>
		                                @if ($market_day->brought_scrips()->where('scrip_id', $scrip->id)->count())
		                                    <td>{{ $brought_pieces = $market_day->brought_scrips()->where('scrip_id', $scrip->id)->first()->pivot->pieces }}</td>
		                                @else
		                                    <td>{{ $brought_pieces = 0 }}</td>
		                                @endif
		                                <td>{{ $distributed = $market_day->getScripSoldCountById($scrip->id) }}</td>
		                                <td>
			                                {{-- show alert if below 0 --}}
			                                <span class="@if ($brought_pieces - $distributed < 0)text-warning @endif">
			                                {{ $brought_pieces - $distributed }}
			                                </span>
			                            </td>

		                            </tr>
		                        @endforeach
		                    </tbody>
		                </table>
		            </div>
	            </div>
	        </div>
        @endhasrole

    </div>


@endsection
