@extends('layouts.app')

@section('content')

    <div class="content clearfix">
        <h2 class="title"><i class="icon_div ti-scrips"></i>Collect Scrip for <strong>{{ $vendor->name }}</strong>

			@if ($vendor->pivot->attended_market)
				<br clear="all">
				<button class="btn btn-default vendor_nav_btn tight btn_spacer" type='button' id="no_scrip_collected_button" data-button_type='Did not collect scrip'>
			          Did not Collect Scrip
	            </button>
		        <button class="btn btn-default vendor_nav_btn tight btn_spacer"  type='button' id="none_to_collect_button" data-button_type='No scrip to collected'>
			          None to Collect
	            </button>
			@endif


 		</h2>
        <hr>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
		{{-- Did vendor atttend market --}}
		@if (! $vendor->pivot->attended_market)
			<h3 class='text-warning'>This vendor did not attend the market</h3>
        {{-- if there are any scrip this vendor accepts, show them --}}
        @elseif (! $vendor->scrips->where('pivot.accepted',true)->isEmpty() )

	        <form
	            method="POST"
	            class="form-horizontal"
	            action="{{ route('tools.show_collect_scrips_for_vendor', ['market_day' => $market_day->id, 'vendor' => $vendor->id]) }}">
	            {{ csrf_field() }}
	            <input type="hidden" name="_method" value="PATCH">

				{{-- show a row. will be filled in using JS --}}
				<span id='scrip_span'>
                    @if( old('add-scrip') and count( old('add-scrip') ) )
                        @php $old_add_scrips = old('add-scrip'); @endphp
                        @php $old_scrip_quantities = old('add-scrip-quantity'); @endphp
                        @foreach( $old_add_scrips as $old_add_scrip_id )
                        	<!--- if no quanity, don't display :EL --->
                        	@if ($old_scrip_quantities[$loop->index] != 0)
	                            @include('tools.scrips_dropdown', ['old_add_scrip_id' => $old_add_scrip_id, 'collected_scrip' => null, 'old_scrip_quantity' => $old_scrip_quantities[$loop->index], 'first' => $loop->first])
	                        @endif
                        @endforeach
	                @elseif( count( $vendor->pivot->scrips ) )
	                    @foreach( $vendor->pivot->scrips as $collected_scrip )
                            @include('tools.scrips_dropdown', ['collected_scrip' => $collected_scrip, 'first' => $loop->first])
	                    @endforeach
	                @else
                        @include('tools.scrips_dropdown', ['collected_scrip' => null, 'first' => false])
	                @endif
				</span>

				<div class="row">
					<br>
					<div class="col-sm-5"><button id="add_scrip_button" class="btn btn-default btn-lg" type="button">Add Scrip &nbsp;<i class="icon_div ti-plus"></i></button></div><div class="col-sm-4  input-lg"><b>Grand Total: $<span id="grand_total_span">0</span></b></div>
				</div>
				<br clear="all">
				<br clear="all">
				<div class="row">
		            <div class="col-sm-8">
			            <p><a href='' id="notes" class="btn btn-info btn-lg">Add Note</a></p>
		            </div>
		            <div class="col-sm-4">&nbsp;</div>

				</div>

				<div class="row">
					<div class="col-sm-3"></div>
				</div>

				<div class="row">
					<div class="col-sm-3"></div>
					<input type="submit" value="Save & Email" class="btn btn-primary btn-lg btn-fill btn_spacer" @if ($vendor->print_receipt) target="_blank" @endif>
				</div>

	        </form>


	    @else
	    	This vendor does not accept any scrip.
		@endif


    </div>

    <div style="display: none;" id="row_template">
        @include('tools.scrips_dropdown', ['collected_scrip' => null, 'first' => false])
    </div>
@endsection

@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    <script>
        jQuery(document).ready(function($) {

			// bind all selects
			function bind_select() {
				// remove any click events
				$(".add-scrip").off('change');
				// change dropdown [en|dis]ables options in other dropdown
				$(".add-scrip").change( function (){

					// define the current select
					var this_select = this;
					// define the selected option
					var selected_id = $(this).find(":selected").val();
/* removed since it was diabling the last dd if there were multiples
					// disable any scrips that are already used
					// loop over all visible selects
					$('.add-scrip:visible').each(function(index,element){
						// if this is not the current dropdown, then disable its selected option in the last one.
						if ( this_select != this ) {
							// disable in last dropdown
//console.log('selected_id='+selected_id+' make disabled');
							$(this).find('option[value='+selected_id+']').attr('disabled','disabled');
						}
					})
					// ^^ disable any scrips that are already used ^^ //

					// enable any scrips that are already used //
					// loop over all visible selects
					$('.add-scrip:visible').each(function(index,element){
						// if this is not the current dropdown, then disable its selected option in the last one.
						if ( this_select != this ) {
							// disable in last dropdown
							$(this).find('option[value='+selected_id+']').attr('disabled','disabled');
						}
					})
					// ^^ enable any scrips that are already used ^^ //
*/
					// set deomination span
					var this_denomination=$(this).find('option[value='+selected_id+']').data('denomination');
					// set the denomination span
					$(this).parent('div').next('div').next('div').children('span#denomination_span').text(this_denomination);

					// set color of scrips div
					$(this).parent('div').parent('div').attr('style','background-color:'+$(this).find('option[value='+selected_id+']').data('color'));

				})
			} // f(x) bind select
			bind_select();

			// add scrip button
			$('button[id=add_scrip_button]').click( function (){
				$('#scrip_span').append( $('#row_template > div').clone() );
				// change visibility
				$('#scrip_span > #row_template').css('display','block');

				// bind elements in the new row_template
				bind_select();
				bind_remove();

			});

			/* disable any scrips that are already used */
			// loop over all visible selects and make
			var this_len = $('.add-scrip:visible').length;
			$('.add-scrip:visible').each(function(index, element){
				// if this is not the last dropdown, then disable its selected option in the last one.
				if ( index != this_len - 1 ) {
					// define the selected option
					var selected_id = $(this).find(":selected").val();
					// disable in last dropdown
					$('.add-scrip:visible option[value='+selected_id+']').last().attr('disabled','disabled');
				}
			})
			/* ^^ disable any scrips that are already used ^^ */



			// remove scrip button
			function bind_remove() {
				$('.remove-scrip').on('click', function(event) {
	                event.preventDefault();
	                // define the row to be removed
	                var this_delete_row=$(this).parent('span').parent('div').parent('.form-group');
					// define the selected option for item being deleted
					var selected_id=$(this).next('select').find(":selected").val();
					// remove the select
					this_delete_row.remove();


					/* enable any scrips that were used by the one being deleted */
					// loop over all visible selects
					$('.add-scrip:visible').each(function(){
//console.log(this);
						// enable in last dropdown
						$('option[value='+selected_id+']', this).attr('disabled',false);
					})
					/* ^^^ enable any scrips that were used by the one being deleted */
	            });


				/** Disable GO/Return button as submit **/
				$('body input[type="text"],body input[type="number"],body input[type="date"]').on('keydown', function(event) {
				    if(event.keyCode == 13) {
				        document.activeElement.blur();
				        return false;
				    }
				});
				/*^^ Disable GO button ^^*/

			} // remove scrip button
			bind_remove();


            // show total: denomination * quantity
            $('body').on('keyup change', '.add-scrip, .add-scrip-quantity', function (event) {
				event.preventDefault();

                $('.add-scrip-quantity').each(function(index, el) {
                    // define value
                    var this_value = $(this).val();
                    // define denomination
                    var this_denomination = $(this).parent().prev('div').find('select option:selected').data('denomination');

                    var this_total = this_value * this_denomination;
                    //  if value is number, multiply by denomination
                    if ( ! $.isNumeric(this_total) ) {
                        var this_total = 0;
                    }
                   // show total in span
				   $(this).parent('div').next('div').find('span#scrip_total').text(this_total);

                   // show grand total: loop over items and get each total
                   var this_grand_total = 0;
                   $('span#scrip_total').each( function(){
                       this_grand_total += parseInt($(this).text(), 10);
                   })
                   $('#grand_total_span').text(this_grand_total);
                });
            });

            $('body input.add-scrip-quantity').first().trigger('change');

			// update all values on page load
			$('.add-scrip').each( function() {
				$(this).change();
			})

			$('#notes').click( function(event) {
				event.preventDefault();
				$(this).parent('p').html('Notes:<br><textarea class="form-control input-lg" id="collected-scrip-note" name="collected_scrip_note" placeholder="Notes...">{{ old("collected_scrip_note", $vendor->pivot->collected_scrip_note) }}</textarea>');
			})
			if ('{{ old("collected_scrip_note", $vendor->pivot->collected_scrip_note) }}' != '') {
				$('#notes').click();
			}

			// No estimate, zero dollars buttons
			$('button[id=no_scrip_collected_button], button[id=none_to_collect_button]').click(function (){
				// set value to blank
				$('input[name^=add-scrip-quantity]').each( function() {
					$(this).val(0);
				})
				$('#collected-scrip').val('0');

				// set notes to data-button_type
				$('#notes').click();
				$('#collected-scrip-note').val($(this).data('button_type'));
			})


        });
    </script>
@endsection