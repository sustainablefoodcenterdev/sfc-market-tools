<style>
.top-padding {padding-top:10px !important}
</style>
<div class="form-group top-padding">
    <div class="col-sm-3">
        <select name="add-scrip[]" class="add-scrip form-control input-lg" style="width:180px;">
        @foreach ($scrips as $scrip)
            <option
                value="{{ $scrip->id }}"
                data-denomination="{{ $scrip->denomination }}"
                data-color="{{ $scrip->color }}"
                @if( $collected_scrip && $collected_scrip->id === $scrip->id )
                selected
                @endif
                @if( isset($old_add_scrip_id) && $old_add_scrip_id == $scrip->id )
                selected
                @endif
                @unless( $vendor->scrips->where('pivot.accepted',true)->contains($scrip) )
                disabled
                @endunless
                >
                {{ $scrip->name }}
            </option>
        @endforeach
        </select>
    </div>
    <div class="col-sm-2">
        <input
            type="number"
            min='0'
            name="add-scrip-quantity[]"
            class="add-scrip-quantity form-control "
            placeholder="Pieces"
            @if( isset($old_scrip_quantity) && ! $collected_scrip )
                value="{{ $old_scrip_quantity }}"
            @elseif ( $collected_scrip )
                value="{{ $collected_scrip->pivot->pieces_collected }}"
            @else
                value=""
            @endif
            style="width:100px;"
            >
    </div>
    <div class="col-sm-4  input-lg">
        @ $
        <span id='denomination_span' class=''></span>
        each =
	    <span style="text-align: left">
        $<span id="scrip_total">0</span>

        <a href="#" class="pull-right text-danger remove-scrip"><i class="icon_div ti-trash"></i></a>

	    </span>
    </div>
</div>