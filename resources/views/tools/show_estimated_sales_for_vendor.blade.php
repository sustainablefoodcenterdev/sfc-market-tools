@extends('layouts.app')

@section('content')
    <div class="content clearfix">
        <h2 class="title">
	        <i class="icon_div ti-estimated_sales"></i>Estimated Sales for <strong>{{ $vendor->name }}</strong>

			<button class="btn btn-default vendor_nav_btn tight btn_spacer" type='button' id="no_estimate_button" data-button_type='Did not Get Estimate'>
		          Did not Get Estimate
            </button>
	        <button class="btn btn-default vendor_nav_btn tight btn_spacer"  type='button' id="zero_dollars_button" data-button_type='Made $0'>
		          Made $0
            </button>

        </h2>

        <hr>
		@if (! $vendor->pivot->attended_market)
			<h3 class='text-warning'>This vendor did not attend the market</h3>
		@else
		<br><br>
        <form class="form-inline" method="POST" action="{{ route('tools.show_estimated_sales_for_vendor', ['market_day' => $market_day->id, 'vendor' => $vendor->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PATCH">

			@if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

			<div class="row">
	            <div class="form-group  col-sm-4">

					<div class="input-group no-margin">
						<span class="input-group-addon"><i class="icon_div ti-money"></i></span>
						<input
				            class="form-control input-lg"
				            type="number"
				            min="0"
				            id="estimated-sales"
				            name="estimated_sales"
				            placeholder="Estimated Sales"
				            value="{{ old('estimated_sales', $vendor->pivot->estimated_sales) }}"
				            style="width:200px">
					</div>


	            </div>

			</div>

			<div class="row">
	            <div class="form-group col-sm-4">
					<br/>
					<p ><a href='' id="notes" class="btn btn-info btn-lg">Add Note</a></p>
	            </div>
			</div>

				<div class="row">
					<div class="col-sm-3"></div>
					<input type="submit" value="Save" class="btn btn-primary btn-lg btn-fill btn_spacer" @if ($vendor->print_receipt) target="_blank" @endif>
				</div>


        </form>
		<br><br>
		@endif

    </div>
</div>
@endsection


@section('scripts')
    {{-- Outputs script tags from app.blade.php first --}}
    @parent
    <script>
        $(document).ready(function($) {
			$('#notes').click( function(event) {
				event.preventDefault();
				$(this).parent('p').html('<textarea class="form-control input-lg" id="estimated-sales-note" name="estimated_sales_note" placeholder="Notes...">{{ old("estimated_sales_note", $vendor->pivot->estimated_sales_note) }}</textarea>');
			})
			if ('{{ old("estimated_sales_note", $vendor->pivot->estimated_sales_note) }}' != '') {
				$('#notes').click();
			}

			// No estimate, zero dollars buttons
			$('button[id=no_estimate_button], button[id=zero_dollars_button]').click(function (){
				// set value to blank
				$('#estimated-sales').val('0');

				// set notes to data-button_type
				$('#notes').click();
				$('#estimated-sales-note').val($(this).data('button_type'));
			})

		});
    </script>
@endsection
