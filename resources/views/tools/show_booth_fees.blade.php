@extends('layouts.app')

@section('content')
	<div class="content clearfix">
		<div class="row">

			<div class="col-sm-6">
		        <h2 class="title"><i class="icon_div ti-booth_fees"></i>Booth Fees Collected</h2>
			</div>
		</div>
        <hr>
	</div>


	<div class="row ">
		<div class="col col-sm-10">

			<div class="col-sm-1">
				<a class="btn btn-primary" href="{{ URL::route('market_days.edit', $market_day->id) }}">Add Vendor</a>
			</div>
			<br clear="all"><br>
			@include('vendor_search', ['tool' => 'show_booth_fees_for_vendor'])
			{{-- make table responsive --}}
			<div class="table-responsive">

				<table class="table table-hover table-striped">
				    <thead>
				        <tr class='success'>
				            <th>&nbsp;</th>
				            <th>Vendor</th>
				            <th>Collected</th>
				            <th>Booths</th>
				            <th>Note</th>
				    	</tr>
				    </thead>
				    <tbody>

					@foreach ($booth_fees as $booth_fee)
			        <tr>
						<td>
							<a class='btn btn-info btn-lg' href="{{ route('tools.show_booth_fees_for_vendor', ['market_day' => $market_day->id, 'vendor' => $booth_fee['id']]) }}">
								Enter
							</a>
						</td>
						<td>{{ $booth_fee['name'] }}</td>
						<td>
							{{-- IF note is NCNS or Called, show no fee --}}
							@if ($booth_fee['note'] == 'NCNS' || $booth_fee['note']=='Called')
								$--
							@else
								${{ $booth_fee['fee'] }}
							@endif
						</td>
						<td>
							{{-- IF note is NCNS or Called, show no fee --}}
							@if ($booth_fee['note'] == 'NCNS' || $booth_fee['note']=='Called')
								--
							@else
								{{ $booth_fee['booths'] }}
							@endif
						</td>
						<td>{{ $booth_fee['note'] }}</td>
			        </tr>
					@endforeach
				    </tbody>
				</table>
			</div>
		</div>

	</div>

@endsection