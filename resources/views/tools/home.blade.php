@extends('layouts.app')

@section('nav-left')

@endsection

@section('content')
<div class="header">
    <h2 class="title">
        <i class="icon_div ti-market_days"></i>
        Market Days
    </h2>
	<h2>Select the market day to manage</h2>

</div>


<table class="table table-hover table-striped">
    <thead>
        <tr class='success'>
            <th>&nbsp;</th>
            <th>Name</th>
        	<th>Day</th>
    	</tr>
    </thead>
    <tbody>

		@foreach ($market_days as $market_day)
        <tr>
            <td><a href='{{ URL::route('tools.dashboard', $market_day->id) }}'><button class="btn btn-lg btn-info" >Manage</button></a></td>
			<td>
				<img src='/img/markets/{{$market_day->market->image}}' width='80' height='60'>
				{{ $market_day->market->name}}
			</td>
        	<td>{{ date('D M d, Y', strtotime($market_day->date)) }}</td>
        </tr>
		@endforeach
		@php unset($market_day); @endphp

		<!--- previous market days :EL --->
		<tr class='alert-warning'>
			<td  class='alert-warning'></td>
			<td class='alert-warning'>
			-- Previous market days --
			</td>
			<td class='alert-warning'></td>

		</tr>

		@foreach ($previous_market_days as $previous_market_day)
        <tr>
            <td><a href='{{ URL::route('tools.dashboard', $previous_market_day->id) }}'><button class="btn btn-lg btn-info" >Manage</button></a></td>
			<td>
				<img src='/img/markets/{{$previous_market_day->market->image}}' width='80' height='60'>
				{{ $previous_market_day->market->name}}
			</td>
        	<td>{{ date('D M d, Y', strtotime($previous_market_day->date)) }}</td>
        </tr>
		@endforeach
		@php unset($previous_market_day); @endphp
    </tbody>
</table>

@endsection
