<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

</head>
<body>
    <div class="container">
        <h1>Collected Scrip Receipt</h1>

        <p>This is your receipt for Scrip collected at today's market.</p>

        <h2>Market: {{ $market_day->market->name }} - {{ $market_day->date->format('D, M jS, Y') }}</h2>

        <h2>Vendor: {{ $vendor->name }}</h2>

        <table>
            <thead>
                <tr>
                    <td>Scrip Name</td>
                    <td>Pieces Collected</td>
                </tr>
            </thead>
            <tbody>
                @foreach($collected_scrips as $scrip_name => $collected)
                <tr>
                    <td>{{ $scrip_name }}</td>
                    <td>{{ $collected }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <p>Thanks,<br>
        {{ config('app.name') }}
        </p>
    </div>
</div>
</body>
</html>