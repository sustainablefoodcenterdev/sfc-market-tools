<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

</head>
<body>
    <div class="container">
        <h1>Booth Fees Receipt</h1>

        <p>This is your booth fee receipt for attendance at today's market.</p>

        <h2>Market: {{ $market_day->market->name }} - {{ $market_day->date->format('D, M jS, Y') }}</h2>

        <h2>Vendor: {{ $vendor->name }}</h2>

        <table>
            <tbody>
                <tr>
                    <td>Booth Fees</td>
                    <td>${{ $vendor->pivot->booth_fee }}</td>
                </tr>
                <tr>
                    <td># Booths</td>
                    <td>{{ $vendor->pivot->booths_paid_for }}</td>
                </tr>
            </tbody>
        </table>

        @if ($vendor->pivot->booth_fee_note)
            <h3>Note</h3>
            <div class="well">
                <p>{{ $vendor->pivot->booth_fee_note }}</p>
            </div>
        @endif

        <p>Thanks,<br>
        {{ config('app.name') }}
        </p>
    </div>
</div>
</body>
</html>