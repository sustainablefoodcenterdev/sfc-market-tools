<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <!-- Bootstrap core CSS     -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/bootstrap-switch.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  Select Search  CSS    -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap3.min.css" rel="stylesheet"/>

    <!--  Fonts and icons -->
    {{-- <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"> --}}
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>


    <link href="/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="primary">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->
       @include('sidebar_button')
    </div>

    <div class="main-panel">
 		<nav class="navbar navbar-default">
            <div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
				</div>

                <div class="navbar-header" >
                    <button type="button" class="navbar-toggle ">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <div style="float:left;">
                    <span class="navbar-brand">
						@if (  isset($market_day->market->name) )
							<p><big><big>{{ $market_day->market->name }} - {{ $market_day->date->format('D, M jS, Y') }}</big></big></p>
						@endif
					</span>
                    </div>

                    <div style="float:left;">
		                <div class="collapse navbar-collapse">
		                    <ul class="nav navbar-nav navbar-right">
		                        <!-- Authentication Links -->
							    @if (Auth::guest())
	{{--						        <li><a href="{{ url('/login') }}">Login</a></li>
							        <li><a href="{{ url('/register') }}">Register</a></li>
	--}}
							    @else
							        <li class="dropdown text-right">
							            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							                {{ Auth::user()->name }} <span class="caret"></span>
							            </a>
							            <ul class="dropdown-menu" role="menu">
							                <li>
							                    <a href="{{ url('/logout') }}" class='text-right'
							                        onclick="event.preventDefault();
							                                 document.getElementById('logout-form').submit();">
							                        Logout
							                    </a>
							                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
							                        {{ csrf_field() }}
							                    </form>
							                </li>
							            </ul>
							        </li>
							    @endif
		                    </ul>
		                </div>
                    </div>


                </div>
            </div>
        </nav>


       <div class="content">
            <div class="container-fluid">
                <div class="row">
					<div class="card">
						@yield('content')
					</div>
	            </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> WebChronic LLC
                </div>
            </div>
        </footer>

    </div>
</div>
</body>

    @section('scripts')
    <!--   Core JS Files   -->
    <script src="/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/js/perfect-scrollbar.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="/js/bootstrap-checkbox-radio.js"></script>
	<script src="/js/bootstrap-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>  -->

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

    <script src="/js/jquery-character-counter.js"></script>

	<script language="JavaScript1.2" type="text/javascript">
		// <!--
		$(document).ready(function() {
			// select text inside of text or textarea on focus
			$('input[type="text"],input[type="number"],input[type="date"], textarea').focus(function() {
			    this.setSelectionRange(0,999); //no jquery wrapper
			}).mouseup(function(e){
			    e.preventDefault();
			});

			// INPUTs with text or numbers don't let GO button submit form.
			$('body input[type="text"],body input[type="number"],body input[type="date"]').keydown(function(){
			    if(event.keyCode == 13) {
			        document.activeElement.blur();
			        return false;
			    }
			});

		 });

	// -->
	</script>
@endsection

    @yield('scripts')
    {{-- Need scrips from individual blade templates to load *after* everything else, so we wrapped them in a section --}}


</html>
