<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{

	use SoftDeletes;

    protected $fillable = ['name', 'email', 'phone', 'vendor_type_id', 'email_receipt', 'print_receipt'];

    public function market_days() {
        return $this->belongsToMany('App\MarketDay', 'vendor_to_market_days', 'vendor_id', 'market_day_id')
            ->withPivot('id', 'booth_fee', 'estimated_sales', 'location_order', 'attended_market')
            ->using('App\MarketDayVendor')
            ->withTimestamps();
    }

    public function type() {
        return $this->belongsTo('App\VendorType', 'vendor_type_id')
			->withTrashed();
    }

    public function scrips() {
        return $this->belongsToMany('App\Scrip', 'vendor_accepted_scrips', 'vendor_id', 'scrip_id')
            ->withPivot('id', 'accepted')
            ->withTimestamps()
            ->withTrashed();
    }

}
