<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scrip extends Model
{

	use SoftDeletes;

    //
    protected $fillable = [
        'name', 'denomination', 'color', 'sort_order'
    ];

    public function getReportScrips() {

		return $this->withTrashed()->get()->sortBy("report_sort_order");

    }

    public function getDistrubutedScrips() {

	    return $this->withTrashed()->where('distributed_at_market','=',1)->get()->sortBy("sort_order");

	}
}