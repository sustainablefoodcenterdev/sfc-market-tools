<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MarketDayVendor extends Pivot
{

    public function scrips() {
        return $this->belongsToMany('App\Scrip', 'scrip_to_vendors', 'vendor_to_market_day_id', 'scrip_id')
            ->withPivot('id', 'pieces_collected')
            ->withTimestamps();
    }
}
