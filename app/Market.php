<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Market extends Model
{
    use Softdeletes;

    public function market_days() {
        return $this->hasMany('App\MarketDay')->withTimestamps();
    }

}
