<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketDay extends Model
{
    use SoftDeletes;
    
    protected $table = "market_days";

    protected $dates = ['created_at', 'updated_at', 'date'];

    public function market() {
        return $this->belongsTo('App\Market');
    }

    public function vendors() {
        return $this->belongsToMany('App\Vendor', 'vendor_to_market_days', 'market_day_id', 'vendor_id')
            ->withPivot('id', 'booth_fee', 'booth_fee_note', 'booths_paid_for', 'estimated_sales', 'estimated_sales_note', 'collected_scrip_note', 'location_order', 'attended_market', 'collected_estimated_sales')
            ->withTrashed()
            ->using('App\MarketDayVendor')
            ->withTimestamps();
    }

    public function scrip_sales() {
        return $this->hasMany('App\ScripSale');
    }

    public function brought_scrips() {
        return $this->belongsToMany('App\Scrip', 'market_day_brought_scrips', 'market_day_id', 'scrip_id')
            ->withPivot('id', 'pieces')
            ->withTimestamps()
            ->withTrashed();
    }

    public function getScripSoldCountById($scrip_id) {
        $count = 0;

        // Sale:
        $this->scrip_sales->each(function($scrip_sale) use (&$count, $scrip_id){
            $count += $scrip_sale->scrips()->where('scrip_id', $scrip_id)->sum('amount');
        });

        return $count;
    }

    public function getCollectedBoothFeeCountAttribute()
    {
        return $this->vendors()->wherePivot('collected_booth_fee', true)->count();
    }

    public function getTotalBoothFeesAttribute()
    {
        return $this->vendors()->wherePivot('collected_booth_fee', true)->sum('booth_fee');
    }

    public function getVendorsThatCanCollectScripCountAttribute()
    {
        return $this->vendors()->wherePivot('attended_market',true)->whereHas('scrips', function ($query) {
            $query->where('accepted', true);
        })->count();
    }

    public function getVendorsThatCollectedScripCountAttribute()
    {
        return $this->vendors()->wherePivot('collected_scrip', true)->count();
    }

    public function getCollectedScripTotalAttribute()
    {
        $total = 0;

        $this->vendors->each(function ($vendor, $key) use (&$total) {
            $vendor->market_days()
                ->where('market_days.id', $this->id)
                ->first()
                ->pivot
                ->scrips
                ->each(function($scrip) use (&$total) {
                    $total += ( $scrip->pivot->pieces_collected * (int)$scrip->denomination );
                });
        });

        return $total;
    }

    public function getCollectedScripCountAttribute()
    {
        $count = 0;

        $this->vendors->each(function ($vendor, $key) use (&$count) {
            $count += $vendor->market_days()
                ->where('market_days.id', $this->id)
                ->first()
                ->pivot
                ->scrips()
                ->sum('pieces_collected');
        });

        return $count;
    }

    public function getScripSoldCountAttribute()
    {
        $count = 0;

        // Sale:
        $this->scrip_sales->each(function($scrip_sale) use (&$count){
            // Line Item:
            $scrip_sale->scrips->each(function($scrip) use (&$count){
                // Quantity:
                $count += $scrip->pivot->amount;
            });
        });

        return $count;
    }

    public function getScripSoldTotalAttribute()
    {
        $total = 0;

        // Sale:
        $this->scrip_sales->each(function($scrip_sale) use (&$total){
            // Line Item:
            $scrip_sale->scrips->each(function($scrip) use (&$total){
                // $$ total
                $total += ($scrip->pivot->amount * (int)$scrip->denomination);
            });
        });

        return $total;
    }

    public function getEstimatedSalesCountAttribute()
    {
		return $this->vendors()->wherePivot('collected_estimated_sales', true)->count();

    }

    public function getEstimatedSalesTotalAttribute()
    {
        return $this->vendors()->wherePivot('collected_estimated_sales', true)->sum('estimated_sales');
    }

    /**
     * Return all vendors for this market day.
     *
     * @return string
     */
    public function getVendorsForMultiSelect()
    {
        $vendors = [];

        foreach ($this->vendors()->wherePivot('attended_market',true)->orderBy('location_order')->get() as $vendor) {
            $vendors[] = [
                'value' => $vendor->id,
                'text' => $vendor->name
            ];
        }

        return $vendors;
    }

    //Eric's attempt to get the scrips colelcted per vendor
    public function getVendorsScripCollectedInfoAttribute()
    {

		// array to hold scrip types and pieces_collected
        $collected_scrip = [];


		// loop over each vendor and 'return' $collected_scrip after loop
        $this->vendors->each(function ($vendor, $key) use (&$collected_scrip) {

			$collected_scrip[$vendor->name] = [];
            $collected_scrip[$vendor->name]['id'] = $vendor->id;
            $collected_scrip[$vendor->name]['vendor_name'] = $vendor->name;
			$collected_scrip[$vendor->name]['collected_scrip_note'] = $vendor->pivot->collected_scrip_note;
            $collected_scrip[$vendor->name]['scrip_collected'] = [];

            $vendor->market_days()
                ->where('market_days.id', $this->id)
                ->first()
                // We're accessing the Market Day off the vendor so we have access to 'pivot'
                // the pivot is represented by MarketDayVendor
                ->pivot
                ->scrips // relationship of MarketDayVendor
                ->each(function ($scrip) use (&$collected_scrip, $vendor) {
                    $collected_scrip[$vendor->name]['scrip_collected'][$scrip->name] = ['pieces'=>$scrip->pivot->pieces_collected, 'denomination'=>$scrip->denomination];
					$collected_scrip[$vendor->name]['scrip_id'] = $scrip->pivot->scrip_id;

                });
		});

        return $collected_scrip;
    }


}
