<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketVendorType extends Model
{
    //
    protected $fillable = [
        'market_id', 'vendortype_id', 'booth_fee'
    ];
}