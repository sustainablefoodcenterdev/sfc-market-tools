<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // the main level for the page: Admin, Tools, etc
        $url_level= Request::segment(1);
        View::share('url_level',$url_level);

        Validator::extend('no_duplicates', function ($attribute, $value, $parameters, $validator) {
            if (is_array($value)) {
                if (count($value) !== count( array_unique($value))) {
                    return false;
                }
            }

            return true;
        });

        Validator::extend('no_nulls', function ($attribute, $value, $parameters, $validator) {
            if (is_array($value)) {
                if (count($value) !== count(array_filter($value))) {
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
