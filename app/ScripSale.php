<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScripSale extends Model
{
    public function market_day() {
        return $this->belongsTo('App\MarketDay');
    }

    public function scrips() {
        return $this->belongsToMany('App\Scrip', 'scrip_sales_scrips', 'scrip_sale_id', 'scrip_id')
            ->withPivot('scrip_id', 'amount', 'card_number')
            ->withTimestamps()
            ->withTrashed();
    }
}
