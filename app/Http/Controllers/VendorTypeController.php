<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorType;
use App\Market;

class VendorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vendortypes=VendorType::orderBy("name")->get();
        return view('vendortypes.index', ['vendortypes'=> $vendortypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$markets = Market::all();

        return view('vendortypes.create', ['markets'=>$markets]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|unique:vendor_types',
            'color' => 'required',
        ]); // On error, automatically exits method, does not continue to code below


        $vendortype = new VendorType;
        $vendortype->name = $request->input('name');
        $vendortype->color = $request->input('color');
        $vendortype->save();

        $booth_fees = array_filter( $request->input('booth_fee', []), 'is_numeric' );
        foreach ($booth_fees as $key => $value) {
            $booth_fees[$key] = ['booth_fee' => $value];
        }
        $vendortype->markets()->sync( $booth_fees );

        return redirect()->action('VendorTypeController@show', ['vendortype' => $vendortype]);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\VendorType  $vendortype
     * @return \Illuminate\Http\Response
     */
    public function show(VendorType $vendortype)
    {
        //
        return view('vendortypes.show', ['vendortype'=> $vendortype]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\VendorType  $vendortype
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorType $vendortype)
    {
        //

		$markets = Market::all();
        return view('vendortypes.edit', ['vendortype' => $vendortype, 'markets' => $markets]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\VendorType  $vendortype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorType $vendortype)
    {

        $this->validate($request, [
            'name' => 'required|unique:vendor_types,name,' . $vendortype->id,
            'color' => 'required',
        ]);

        $vendortype->name = $request->input('name');
        $vendortype->color = $request->input('color');
        $vendortype->save();

        $booth_fees = array_filter( $request->input('booth_fee', []), 'is_numeric' );
        foreach ($booth_fees as $key => $value) {
            // $key === market ID
            $booth_fees[$key] = ['booth_fee' => $value];
        }
        $vendortype->markets()->sync( $booth_fees );

        return redirect()->action('VendorTypeController@show', ['vendortype' => $vendortype]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorType $vendortype)
    {
        //
        $vendortype->delete();
        return redirect()->action('VendorTypeController@index');

    }
}
