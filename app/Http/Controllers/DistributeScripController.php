<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\MarketDay;
use App\ScripSale;
use DB;

class DistributeScripController extends Controller
{
    protected $distributed_scrip_types = [
        'SNAP' => [
            'denominations' => [1, 5, 1],
            'ids' => [5,6,7]
        ],
        'WIC' => [
            'denominations' => [1, null, 1],
            'ids' => [8, null, 9]
        ],
        'FMNP' => [
            'denominations' => [1, null, 1],
            'ids' => [null, null, 2]
        ],
    ];

    /**
     * Show a listing of Scrip Sales at a Market Day
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function index(MarketDay $market_day)
    {
        $distributed_scrip_types = $this->distributed_scrip_types;

        return view('tools.distribute_scrip.index', compact('market_day', 'distributed_scrip_types'));
    }

    /**
     * Show the tool for distributing Scrip at a Market Day
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, MarketDay $market_day)
    {
        $distributed_scrip_types = $this->distributed_scrip_types;
        $scrip_sale = new ScripSale;

        return view('tools.distribute_scrip.create_or_edit', compact('market_day', 'distributed_scrip_types', 'scrip_sale' ));
    }

    /**
     * Store a new Scrip Sale
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function storeOrUpdate(Request $request, MarketDay $market_day, ScripSale $scrip_sale = null)
    {
        $rules = [
            'name' => 'max:20|required',
            'zip_code' => 'digits:5|required',
            'distribute_scrip_note' => 'max:100',
            'scrip_type_digits.SNAP' => 'bail|required_with:dollar_amount.SNAP',
            'dollar_amount.SNAP' => 'bail|required_with:scrip_type_digits.SNAP',
            'scrip_type_digits.WIC' => 'bail|required_with:dollar_amount.WIC',
            'dollar_amount.WIC' => 'bail|required_with:scrip_type_digits.WIC',
            'scrip_type_digits.FMNP' => 'bail|required_with:dollar_amount.FMNP',
            'dollar_amount.FMNP' => 'bail|required_with:scrip_type_digits.FMNP',
        ];

        // We don't want the 'integer' and 'digits' rules to apply if these specific inputs are sent through as null
        foreach ($rules as $rulename => $ruleval) {
            if (strpos($rulename, 'dollar_amount') !== false && $request->input($rulename) !== null) {
                $rules[$rulename] .= '|integer';
            }
            if (strpos($rulename, 'scrip_type_digits') !== false && $request->input($rulename) !== null) {
                $rules[$rulename] .= '|digits:4';
            }
        }

        $messages = [
            'scrip_type_digits.SNAP.required_with' => 'You must include the last 4 digits of the SNAP card.',
            'dollar_amount.SNAP.required_with' => 'You must include a dollar amount with the last 4 digits of the SNAP card.',
            'scrip_type_digits.WIC.required_with' => 'You must include the last 4 digits of the WIC card.',
            'dollar_amount.WIC.required_with' => 'You must include a dollar amount with the last 4 digits of the WIC card.',
            'scrip_type_digits.FMNP.required_with' => 'You must include the last 4 digits of the FMNP card.',
            'dollar_amount.FMNP.required_with' => 'You must include a booklet amount with the last 4 digits of the FMNP card.',
            'scrip_type_digits.SNAP.digits' => 'The SNAP card number must be 4 digits.',
            'scrip_type_digits.WIC.digits' => 'The WIC card number must be 4 digits.',
            'scrip_type_digits.FMNP.digits' => 'The FMNP card number must be 4 digits.',
            'dollar_amount.SNAP.integer' => 'The SNAP dollar amount must be an integer.',
            'dollar_amount.WIC.integer' => 'The WIC dollar amount must be an integer.',
            'dollar_amount.FMNP.integer' => 'The number of FMNP booklets must be an integer.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages)->validate();

        // Hard coded Scrip IDs to type
        $ids_to_type = [5 => 'SNAP', 6 => 'SNAP', 7 => 'SNAP', 2 => 'FMNP', 8 => 'WIC', 9 => 'WIC'];

        if ($scrip_sale === null) {
            $scrip_sale = new ScripSale;
        }
        $scrip_sale->name = $request->input('name');
        $scrip_sale->zip_code = $request->input('zip_code');
        $scrip_sale->distribute_scrip_note = $request->input('distribute_scrip_note');
        $scrip_sale->first_market_visit = $request->input('first_market_visit', 0);
        $market_day->scrip_sales()->save($scrip_sale);

        $scrips = [];
        $card_numbers = $request->input('scrip_type_digits', []);

        $scrip_amount_ones = $request->input('scrip_amount_ones', []);
        $scrip_id_ones = $request->input('scrip_id_ones', []);
        $scrip_id_ones = array_map('intval', $scrip_id_ones);
        foreach ($scrip_amount_ones as $id => $amount) {
            if ( in_array($id, $scrip_id_ones, true) && !is_null($amount) ) {
                $scrips[$id] = ['amount' => $amount, 'card_number' => $card_numbers[$ids_to_type[$id]] ];
            }
        }

        $scrip_amount_fives = $request->input('scrip_amount_fives', []);
        $scrip_id_fives = $request->input('scrip_id_fives', []);
        $scrip_id_fives = array_map('intval', $scrip_id_fives);
        foreach ($scrip_amount_fives as $id => $amount) {
            if ( in_array($id, $scrip_id_fives, true) && !is_null($amount) ) {
                $scrips[$id] = ['amount' => $amount, 'card_number' => $card_numbers[$ids_to_type[$id]] ];
            }
        }

        $scrip_amount_dd = $request->input('scrip_amount_dd', []);
        $scrip_id_dd = $request->input('scrip_id_dd', []);
        $scrip_id_dd = array_map('intval', $scrip_id_dd);
        foreach ($scrip_amount_dd as $id => $amount) {
            if ( in_array($id, $scrip_id_dd, true) && !is_null($amount) ) {
                $scrips[$id] = ['amount' => $amount, 'card_number' => $card_numbers[$ids_to_type[$id]] ];
            }
        }
        $scrip_sale->scrips()->sync($scrips);

        return redirect()->action('DistributeScripController@index', ['market_day' => $market_day]);
    }

    /**
     * Show the form for updating a Scrip Sale at a Market Day
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\ScripSale  $scrip_sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, MarketDay $market_day, ScripSale $scrip_sale)
    {
        $distributed_scrip_types = $this->distributed_scrip_types;

        return view('tools.distribute_scrip.create_or_edit', compact('market_day', 'scrip_sale', 'distributed_scrip_types'));
    }

	/**
     * Remove the specified resource from storage.
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function removeScripSaleFromMarketDay(Request $request, MarketDay $market_day, ScripSale $scrip_sale)
    {
	    // delete the scrip_sales_scrip
		$scrip_sale->scrips()->detach();

	    // delete the scrip_sale
		$scrip_sale->delete();

	    // redirect to index
		return redirect()->action('DistributeScripController@index', $market_day);

    }

}
