<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Market;
use Illuminate\Support\Facades\Storage;

class MarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markets=Market::orderBy("name")->get();

        return view('markets.index', ['markets'=> $markets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('markets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:markets,name',
            'day' => 'required|integer',
            'image' => 'required|image'
        ]);

		$market = new Market;
        $market->name = $request->input('name');
        $market->day = $request->input('day');
        $market->save();

        $request->image->storeAs('public/img/markets', $market->id . '.' . $request->image->clientExtension());
        $market->image = $market->id . '.' . $request->image->clientExtension();
        $market->save();

        return redirect()->action('MarketController@show', ['market' => $market]);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Market  $market
     * @return \Illuminate\Http\Response
     */
    public function show(Market $market)
    {
        return view('markets.show', ['market'=> $market]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Market  $market
     * @return \Illuminate\Http\Response
     */
    public function edit(Market $market)
    {
        return view('markets.edit', ['market'=> $market]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Market  $market
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Market $market)
    {
        $this->validate($request, [
            'name' => 'required|unique:markets,name,' . $market->id,
            'day' => 'required|integer',
            'image' => 'image'
        ]);

        $market->name = $request->input('name');
        $market->day = $request->input('day');
        if ($request->image) {
            $request->image->storeAs('public/img/markets', $market->id . '.' . $request->image->clientExtension());
            $market->image = $market->id . '.' . $request->image->clientExtension();
        }
        $market->save();

        return redirect()->action('MarketController@show', ['market' => $market]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Market $market)
    {
        $market->delete();
        return redirect()->action('MarketController@index');

    }
}
