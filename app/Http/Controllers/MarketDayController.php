<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MarketDay;
use App\Market;
use App\Vendor;
use App\Scrip;
use App\ScripSale;

use Validator;


class MarketDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $market_days=MarketDay::all()->sortByDesc("date");
        return view('market_days.index', ['market_days'=> $market_days]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selected_vendors = [];

        foreach (Vendor::orderBy("name")->withTrashed()->get() as $vendor) {
            $vendors[] = [
                'value' => $vendor->id,
                'text' => $vendor->name,
                'deleted_at' => $vendor->deleted_at,
            ];
        }
        $vendors = collect($vendors);

        $unselected_vendors = Vendor::orderBy("name")->get()->pluck('id')->all();

		$scrips = Scrip::all()->where('distributed_at_market','=',1)->sortBy("sort_order");

        $markets = Market::orderBy("name")->get();
        return view('market_days.create', compact(['vendors', 'markets', 'selected_vendors', 'scrips', 'unselected_vendors']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|date',
            'market_id' => 'required|exists:markets,id',
			'market_comments' => 'max:1000',
			'scrip_comments' => 'max:1000'
		],
        [
			'date_format' => 'Invalid date format: YYYY-M-D '
        ]
		);

        $market_day = new MarketDay;

		// make sure date format is Y-m--d
        $time = strtotime($request->input('date'));
  	    $market_day->date = date('Y-m-d',$time);

        $market_day->scrip_comments = $request->input('scrip_comments');
        $market_day->market_comments = $request->input('market_comments');
        $market_day->market()->associate( $request->input('market_id') ); // used in array_map below
        $market_day->save();

		// if scrips is not empty, process
		if ($request->input('scrip_amount') != '') {
	        $scrip_amounts = array_filter($request->input('scrip_amount'));
	        foreach ($scrip_amounts as $scrip_id => $pieces) {
	            $scrip_amounts[$scrip_id] = ['pieces' => (int)$pieces];
	        }
	        $market_day->brought_scrips()->sync( $scrip_amounts );
		}

		// if vendors is not empty, process
		if ($request->input('vendors') != '') {
	        $count = 0;
	        $vendors = array_flip($request->input('vendors', []));
	        $vendors = array_map(function($vendor) use ($market_day, &$count) {
	            $count += 1;
	            return array(
	                'market_id' => $market_day->market->id,
	                'location_order' => $count
	            );
	        }, $vendors);
	        $market_day->vendors()->sync( $vendors );
		}

        return redirect()->action('MarketDayController@show', ['market_day' => $market_day]); // after saving the $market_day model to market_day table, redirect to show method (which will display the show template)
    }

    /**
     * Display the specified resource.
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function show(MarketDay $market_day)
    {
        return view('market_days.show', ['market_day'=> $market_day]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketDay $market_day)
    {
        $selected_vendors = [];
        $vendors = [];

        // Create an array of all the currently selected MarketDay vendors
        foreach ($market_day->vendors()->orderBy('location_order')->get() as $md_vendor) {
            $selected_vendors[] = $md_vendor->id;
        }

        foreach (Vendor::orderBy("name")->withTrashed()->get() as $vendor) {
            $vendors[] = [
                'value' => $vendor->id,
                'text' => $vendor->name,
                'deleted_at' => $vendor->deleted_at,
            ];
        }
        $vendors = collect($vendors);

        $unselected_vendors = Vendor::orderBy("name")->get()->pluck('id')->diff( collect($selected_vendors) )->all();

        $scrips = Scrip::all()->where('distributed_at_market','=',1)->sortBy("sort_order");
        $markets = Market::orderBy("name")->get();
        return view('market_days.edit', compact(['market_day', 'vendors', 'markets', 'selected_vendors', 'scrips', 'unselected_vendors']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketDay $market_day)
    {

        $this->validate($request, [
            'date' => 'required|date',
            'market_id' => 'required|exists:markets,id',
			'market_comments' => 'max:1000',
			'scrip_comments' => 'max:1000'
		]
        ,
        [
			'date_format' => 'Invalid date format: YYYY-M-D'
        ]
		);
		// make sure date format is Y-m--d
        $time = strtotime($request->input('date'));
  	    $market_day->date = date('Y-m-d',$time);

        $market_day->market_comments = $request->input('market_comments');
        $market_day->scrip_comments = $request->input('scrip_comments');
        $market_day->market()->associate( $request->input('market_id') );
        $market_day->save();

        $scrip_amounts = array_filter($request->input('scrip_amount'));
        // if array is empty, don't update anything: either no scrips entered or Market user and did not get inputs to enter them
		if (! empty($scrip_amounts) ) {
	        foreach ($scrip_amounts as $scrip_id => $pieces) {
	            $scrip_amounts[$scrip_id] = ['pieces' => (int)$pieces];
	        }
	        $market_day->brought_scrips()->sync( $scrip_amounts );
		}

        $count = 0;
		if (! empty($request->input('vendors')) ) {
        $vendors = array_flip($request->input('vendors'));


	        $vendors = array_map(function($vendor) use ($market_day, &$count) {
	            $count += 1;
	            return array(
	                'market_id' => $market_day->market->id,
	                'location_order' => $count
	            );
	        }, $vendors);
	        $market_day->vendors()->sync( $vendors ); // remove existing vendors for this market day in the table, then add the new ones
		}

        return redirect()->action('MarketDayController@show', ['market_day' => $market_day]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarketDay $market_day)
    {
		// detach each scrip attached to the scrip sale
		$scrip_sales = ScripSale::all()->where('market_day_id',$market_day->id);
		foreach ($scrip_sales as $scrip_sale ) {
			$scrip_sale->scrips()->detach();
		}
		// delete scrip sales attached to market day
		$market_day->scrip_sales()->delete();
		// delete the atual amrket day
		$market_day->delete();

        return redirect()->action('MarketDayController@index');

    }

    /**
     * Show the form for batch uploading vendors to the specified resource.
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function showBatchUpload(MarketDay $market_day)
    {
        $submitted_vendors = [];
        $vendors = Vendor::orderBy("name")->get();
        return view('market_days.batch_upload', compact(['market_day', 'submitted_vendors', 'vendors']));
    }

    /**
     * Start the process of batch uploading vendors to the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function doBatchUpload(Request $request, MarketDay $market_day)
    {
        if ( $request->input('vendor_upload') ) {
            $submitted_vendors = nl2br($request->input('vendor_upload'));
            $submitted_vendors = array_map('trim', explode('<br />', $submitted_vendors));
            $vendors = Vendor::orderBy("name")->get();
            return view('market_days.batch_upload', compact(['market_day', 'vendors', 'submitted_vendors']));
        }

        if ( $request->input('vendors') ) {
            $validator = Validator::make($request->all(), [
                'vendors' => 'no_nulls',
            ],
            [
                'no_nulls' => 'You must match every provided vendor to an existing vendor name. If you cannot find a matching vendor, you may need to <a href="' . route('vendors.create') . '">create the vendor</a> within the system.'
            ]);
            if ($validator->fails()) {
                return redirect()
                    ->route('market_days.batch_upload', ['market_day' => $market_day->id])
                    ->withErrors($validator)
                    ->withInput();
            }
            $count = 0;
            $vendors = array_flip($request->input('vendors', []));
            $vendors = array_map(function($vendor) use ($market_day, &$count) {
                $count += 1;
                return array(
                    'market_id' => $market_day->market->id,
                    'location_order' => $count
                );
            }, $vendors);
            $market_day->vendors()->sync( $vendors );
            return redirect()->action('MarketDayController@show', ['market_day' => $market_day]);
        }
    }
}
