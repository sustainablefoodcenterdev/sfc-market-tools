<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scrip;

class ScripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scrips=Scrip::orderBy('deleted_at')->orderBy('dashboard_sort_order')->withTrashed()->get();

        return view('scrips.index', ['scrips'=> $scrips]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return view('scrips.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //
        $this->validate($request, [
            'name' => 'required|unique:scrips',
            'denomination' => 'required|integer',
            'color' => 'required',
            'dashboard_sort_order' => 'required',
            'report_sort_order' => 'required',
        ]); // On error, automatically exits method, does not continue to code below


        $scrip = new Scrip;
        $scrip->name =$request->input('name');
        $scrip->denomination =$request->input('denomination');
        $scrip->color =$request->input('color');
        $scrip->dashboard_sort_order =$request->input('dashboard_sort_order');
        $scrip->report_sort_order =$request->input('report_sort_order');
        $scrip->distributed_at_market =$request->input('distributed_at_market');
        $scrip->save();
        return redirect()->action('ScripController@show', ['scrip' => $scrip]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Scrip $scrip)
    {
        //
        return view('scrips.show', ['scrip'=> $scrip]);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Scrip $scrip)
    {
        //
        return view('scrips.edit', ['scrip'=> $scrip]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scrip $scrip)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'denomination' => 'required',
            'color' => 'required',
            'dashboard_sort_order' => 'required',
            'report_sort_order' => 'required'
        ]);

        $scrip->name =$request->input('name');
        $scrip->denomination =$request->input('denomination');
        $scrip->color =$request->input('color');
        $scrip->dashboard_sort_order =$request->input('dashboard_sort_order');
		$scrip->report_sort_order =$request->input('report_sort_order');

        $scrip->save();
        return redirect()->action('ScripController@show', ['scrip' => $scrip]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Scrip $scrip)
    {
        //
        $scrip->delete();
        return redirect()->action('ScripController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {

        Scrip::withTrashed()->find($id)->restore();
        return redirect()->action('ScripController@index');
    }
}
