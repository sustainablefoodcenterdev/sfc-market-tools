<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\VendorType;
use App\Scrip;
use Carbon\carbon;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        //
        $vendors=Vendor::orderBy('deleted_at')->orderBy('name')->withTrashed()->get();
		$vendor_types = VendorType::orderBy("name")->get();

        return view('vendors.index', ['vendors'=> $vendors,'vendor_types'=>$vendor_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        //
		$vendor_types = VendorType::orderBy("name")->get();

		$scrips = Scrip::all();

        return view('vendors.create', ['vendor_types'=>$vendor_types, "scrips"=>$scrips]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|unique:vendors',
            'email' => 'required|email',
            'phone' => 'required|regex:/\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/',
            'vendor_type_id' => 'required|integer'
        ]); // On error, automatically exits method, does not continue to code below


        $vendor = new Vendor;
        $vendor->name = $request->input('name');
        $vendor->email = $request->input('email');
        $vendor->phone = $request->input('phone');
        $vendor->vendor_type_id = $request->input('vendor_type_id');
        $vendor->email_receipt = 1;
//        $vendor->print_receipt = $request->input('print_receipt');
        $vendor->save();

        // Goal
        // [ $scrip_id => ['accepted' => $accepted] ]
        $scrip_ids = array_flip(Scrip::all()->pluck('id')->toArray());
        foreach ($scrip_ids as $key => $value) {
            $accepted = in_array((string)$key, $request->input('accept_scrip_id_list', []), true);
            $scrip_ids[$key] = ['accepted' => $accepted];
        }
        // Replace any existing items in the pivot table
        $vendor->scrips()->sync($scrip_ids);

        return redirect()->action('VendorController@show', ['vendor' => $vendor]);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function show(Vendor $vendor)
    {
        //
		$vendor_types = VendorType::orderBy("name")->get();

        return view('vendors.show', compact(['vendor', 'vendor_types', 'vendor_scrips']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit(Vendor $vendor)
    {
        //
		$vendor_types = VendorType::orderBy("name")->get();

		$scrips = Scrip::all();

        return view('vendors.edit', compact(['vendor', 'vendor_types', 'scrips']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Vendor $vendor)
    {
        //
        $this->validate($request, [
            'name' => 'required|unique:vendors,name,' . $vendor->id,
            'email' => 'required|email',
            'phone' => 'required|regex:/\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/',
            'vendor_type_id' => 'required|integer'
        ]);

        $vendor->name = $request->input('name');
        $vendor->email = $request->input('email');
        $vendor->phone = $request->input('phone');
        $vendor->vendor_type_id = $request->input('vendor_type_id');
        $vendor->email_receipt = 1;
//        $vendor->print_receipt = $request->input('print_receipt');

        $vendor->save();

        // Goal
        // [ $scrip_id => ['accepted' => $accepted] ]
        $scrip_ids = array_flip(Scrip::all()->pluck('id')->toArray());
        foreach ($scrip_ids as $key => $value) {
            $accepted = in_array((string)$key, $request->input('accept_scrip_id_list', []), true);
            $scrip_ids[$key] = ['accepted' => $accepted];
        }
        // Replace any existing items in the pivot table
        $vendor->scrips()->sync($scrip_ids);

        return redirect()->action('VendorController@show', ['vendor' => $vendor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Vendor $vendor)
    {
        //
        $vendor->deleted_at = Carbon::now();;
		$vendor->update();
        return redirect()->action('VendorController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {

        Vendor::withTrashed()->find($id)->restore();
        return redirect()->action('VendorController@index');
    }
}
