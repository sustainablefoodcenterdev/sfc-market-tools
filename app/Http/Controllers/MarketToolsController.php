<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MarketDay;
use App\Vendor;
use App\VendorType;
use App\Scrip;
use App\ScripType;
use App\Mail\BoothFeeReceipt;
use App\Mail\CollectScripReceipt;
use App\Mail\EstimatedSalesReceipt;
use Illuminate\Support\Facades\DB;


class MarketToolsController extends Controller
{

    /**
     * Show the home page of the 'Market Tools' section
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
	    // if market_day_id is set in SESSION scope forward to home page with id
	    if ($request->session()->has('market_day_id')) {
			// get item from Market Day model that matches the session id
		    $market_day = MarketDay::where('id', $request->session()->get('market_day_id'))->first();

			// if market_day->id is set and not null, send to the specific market_day
			if (isset($market_day->id)) {
	            return redirect()->route('tools.dashboard', ['market_day' => $market_day->id]);
			}
			else {
				// no record for this market day so act as if there is no SESSION.market_day_id at all (ie: the initial ELSE statement)
				$request->session()->forget('market_day_id');

				// only get the markets which have not occurred yet.
		        $market_days = MarketDay::where('date','>=',date('Y-m-d'))->orderBy("date",'desc')->get();
		        $previous_market_days = MarketDay::where('date','<',date('Y-m-d'))->orderBy("date",'desc')->get();

		        return view('tools.home')->with(['market_days' => $market_days, 'previous_market_days'=>$previous_market_days]);
			}

            return redirect()->route('tools.dashboard', ['market_day' => $market_day->id]);
		}
		else { // SESSION var not set, send to list to pick
			// only get the markets which have not occurred yet.
	        $market_days = MarketDay::where('date','>=',date('Y-m-d'))->orderBy("date",'desc')->get();
	        $previous_market_days = MarketDay::where('date','<',date('Y-m-d'))->orderBy("date",'desc')->get();

	        return view('tools.home')->with(['market_days' => $market_days, 'previous_market_days'=>$previous_market_days]);
		}
    }

    /**
     * Display the Market Tools page for a specific Market Day
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function dashboard(MarketDay $market_day, Request $request)
    {

		// set SESSION variable for market_day_id
		$request->session()->put('market_day_id', $market_day->id);
        $scrip_model = new Scrip;
        $scrips = $scrip_model->getDistrubutedScrips();

        $vendor_attended_total = $market_day->vendors()->wherePivot('attended_market',true)->count();
        $vendor_expected_up_total = $market_day->vendors()->count();

        return view('tools.dashboard', ['market_day'=> $market_day, 'scrips' => $scrips,  'vendor_attended_total' => $vendor_attended_total,  'vendor_expected_up_total' => $vendor_expected_up_total]);
    }

    /**
     * Reset the Session Market Day
     *
     * @param  App\MarketDay  Request $request
     * @return \Illuminate\Http\Response
     */
    public function resetMarketDay (Request $request)
    {
        $request->session()->forget('market_day_id');

        return redirect()->action('MarketToolsController@home');
    }


    /**
     * Display the Booth Fees module for a specific Market Day
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function showBoothFees(MarketDay $market_day, Request $request)
    {
		// get first vendor in list
		$first_vendor=$market_day->vendors->sortBy('pivot.location_order')->first();
		$vendors = $market_day->vendors()->orderBy("name")->get();

		$booth_fees=[];
		foreach ($vendors as $vendor) {
            $booth_fees[] = [
                'name' => $vendor->name,
                'id' => $vendor->id,
                'fee' => $vendor->pivot->booth_fee,
                'booths' => $vendor->pivot->booths_paid_for,
                'note' => $vendor->pivot->booth_fee_note
            ];
        }
		return view('tools.show_booth_fees', ['market_day' => $market_day,  'vendor' =>$first_vendor, 'booth_fees' => $booth_fees]);

    }

    /**
     * Display the Booth Fees module for a specific Market Day and Vendor
     *
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function showBoothFeesForVendor(MarketDay $market_day, Vendor $vendor)
    {
        $vendors = $market_day->vendors->sortBy('pivot.id')->values();
        $vendor = $vendors->where('id', $vendor->id)->first();
        $prev_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() - 1 );
        $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );
        $market = $vendor->type->markets->where('id', $market_day->market->id)->first();
        $default_booth_fee = ($market) ? $market->pivot->booth_fee : 0;
        return view('tools.show_booth_fees_for_vendor', compact('market_day', 'vendor', 'prev_vendor', 'next_vendor', 'default_booth_fee'));
    }

    /**
     * Update the Booth Fees for a specific Market Day and Vendor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function updateBoothFeesForVendor(Request $request, MarketDay $market_day, Vendor $vendor)
    {
	    $this->validate($request, [
            'booth_fee' => 'required|integer',
            'booths_paid_for' => 'required|integer',
        ]);

        $booth_fee = $request->input('booth_fee');
        $attended_market = $request->input('attended_market');
        $booth_fee_note = $request->input('booth_fee_note');
        $booths_paid_for = $request->input('booths_paid_for');
        $market_day->vendors()->updateExistingPivot($vendor->id, ['booth_fee' => $booth_fee, 'booth_fee_note' => $booth_fee_note, 'booths_paid_for' => $booths_paid_for, 'collected_booth_fee' => true, 'attended_market' => $attended_market]);
		// get the vendor_type info
		$vendor_type_name = VendorType::where('id',$vendor->vendor_type_id)->pluck('name')->first();
        $market = $vendor->type->markets->where('id', $market_day->market->id)->first();
        $default_booth_fee = ($market) ? $market->pivot->booth_fee : 0;
        if ( config('app.env')!= 'local' && $attended_market && $vendor->email_receipt && $vendor->email) {
            \Mail::to($vendor->email)->cc('farmersmarket@sustainablefoodcenter.org')->send(new BoothFeeReceipt($market_day, $vendor, $default_booth_fee, $vendor_type_name));
        }

/*        if ($vendor->print_receipt) {
            $vendor = $market_day->vendors->where('id', $vendor->id)->first();
            $pdf = \PDF::loadView('pdf.booth_fees_receipt', compact('market_day', 'vendor','vendor_types'));
            return $pdf->download('booth_fees_receipt.pdf');
        }
*/
		// determine next vendor to pass as the vendor to display
        $vendors = $market_day->vendors->sortBy('pivot.id')->values();
        $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );

        return redirect()->action('MarketToolsController@showBoothFees', ['market_day' => $market_day, 'vendor' => $next_vendor]);
    }

    /**
     * Display the Scrips module for a specific Market Day
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function showCollectScrips(MarketDay $market_day)
    {

//		$first_vendor=$market_day->vendors->sortBy('pivot.location_order')->first();

//		$vendors = $market_day->vendors()->orderBy("name")->get();

        return view('tools.show_collected_scrips', ['market_day' => $market_day]);

//        return view('tools.show_collected_scrips', ['market_day' => $market_day,  'vendor' => $first_vendor, 'vendors'=>$vendors]);
    }

    /**
     * Display the Scrips module for a specific Market Day and Vendor
     *
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function showCollectScripsForVendor(MarketDay $market_day, Vendor $vendor)
    {
        $vendors = $market_day
            ->vendors // keys will in order, but Collection is sorted by Vendor ID
            ->sortBy('pivot.id') // [ 1 => 'bar', 0 => 'foo', 2 => 'boo' ] // could be how keys look without `->values()`
            ->values(); // [ 0 => 'bar', 1  => 'foo', 2 => 'boo' ] // ->values() resets keys
        // Get the current vendor (with access to `pivot`)
        $vendor = $vendors->where('id', $vendor->id)->first();
        // Get previous and next from the Collection by key (returns null if key doesn't exist in Collection)
        $prev_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() - 1 );
        $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );

        $scrip_model = new Scrip;
        $scrips = $scrip_model->get();

        $vendor_scrips = $vendor->pivot->scrips()->get();

        return view('tools.show_collect_scrips_for_vendor', compact('market_day', 'vendor', 'prev_vendor', 'next_vendor', 'scrips', 'vendor_scrips'));
    }

    /**
     * Update the Scrips for a specific Market Day and Vendor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function updateCollectScripsForVendor(Request $request, MarketDay $market_day, Vendor $vendor)
    {

        $this->validate(
            $request, [
                'add-scrip' => 'required|no_duplicates',
            ],
            [
                'no_duplicates' => 'Please include only one row per scrip',
                'required' => 'At least one scrip is required'
            ]
        );


		// add note to vendor_to_market_days table
/*** Is this the best way, to update the note first and then the scrip info??? ***/
		$collected_scrip_note = $request->input('collected_scrip_note');
        $market_day->vendors()->updateExistingPivot($vendor->id, ['collected_scrip_note' => $collected_scrip_note]);

		// add scrip info to scrip_to_vendors
        $vendor = $market_day->vendors->where('id', $vendor->id)->first();
		$vendor_type_name = VendorType::where('id',$vendor->vendor_type_id)->pluck('name')->first();
        $scrip_ids = $request->input('add-scrip');
        $pieces_collected = $request->input('add-scrip-quantity');
        $scrips = [];

        // mark vendor as having been touched
		$market_day->vendors()->updateExistingPivot($vendor->id, ['collected_scrip' => true]);


		// if no values sent (didn't colelct or none to collect), don't process
		if (array_sum($pieces_collected) > 0) {
	        foreach ($scrip_ids as $key => $val) {
	            $scrips[(int)$val] = ['pieces_collected' => (int)$pieces_collected[$key]];
	            // $scrips[$key] = ['pieces_collected' => $val];
	        }

	        $vendor->pivot->scrips()->sync($scrips);

			$attended_market=$market_day->vendors()->where('vendor_id', $vendor->id)->wherePivot('attended_market',true)->count();
	        if (config('app.env')!= 'local' && $attended_market && $vendor->email_receipt && $vendor->email) {
				\Mail::to($vendor->email)->cc('farmersmarket@sustainablefoodcenter.org')->send(new CollectScripReceipt($market_day, $vendor, $vendor_type_name ));
	        }

	/*        if ($vendor->print_receipt) {
	            $collected_scrips = [];
	            $vendor->pivot
	                ->scrips
	                ->each(function ($scrip) use (&$collected_scrips) {
	                    $collected_scrips[$scrip->name] = $scrip->pivot->pieces_collected;
	                });
	            $pdf = \PDF::loadView('pdf.collect_scrip_receipt', compact('market_day', 'vendor', 'collected_scrips'));
	//            return $pdf->download('collect_scrip_receipt.pdf');
	        }
	*/
		} // only process if sme crip was sent

		// determine next vendor to pass as the vendor to display
        $vendors = $market_day->vendors->sortBy('pivot.id')->values();
        $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );

        return redirect()->action('MarketToolsController@showCollectScrips', ['market_day' => $market_day, 'vendor' => $next_vendor]);
    }

    /**
     * Add A Scrip for a specific Market Day and Vendor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function addScripToVendor(Request $request, MarketDay $market_day, Vendor $vendor)
    {
        $vendor = $market_day->vendors->where('id', $vendor->id)->first();
        $scrip_id = $request->input('add-scrip');
        $pieces_collected = $request->input('add-scrip-quantity');

        $vendor->pivot->scrips()->attach($scrip_id, ['pieces_collected' => $pieces_collected]);

        return redirect()->action('MarketToolsController@showCollectScripsForVendor', ['market_day' => $market_day, 'vendor' => $vendor]);
    }

    /**
     * Add A Scrip for a specific Market Day and Vendor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @param  App\Scrip  $scrip
     * @return \Illuminate\Http\Response
     */
    public function removeScripFromVendor(Request $request, MarketDay $market_day, Vendor $vendor, Scrip $scrip)
    {
        $vendor = $market_day->vendors->where('id', $vendor->id)->first();
        $vendor->pivot->scrips()->detach($scrip->id);

        return redirect()->action('MarketToolsController@showCollectScripsForVendor', ['market_day' => $market_day, 'vendor' => $vendor]);
    }

    /**
     * Display the Estimated Sales module for a specific Market Day
     *
     * @param  App\MarketDay  $market_day
     * @return \Illuminate\Http\Response
     */
    public function showEstimatedSales(MarketDay $market_day, Request $request)
    {
		$first_vendor=$market_day->vendors->sortBy('pivot.location_order')->first();

//		$vendors = $market_day->vendors->sortBy('pivot.id');
		$vendors = $market_day->vendors()->orderBy("name")->get();

		$estimated_sales = [];
		foreach ($vendors as $vendor) {
            $estimated_sales[] = [
                'name' => $vendor->name,
                'id' => $vendor->id,
                'sales' => $vendor->pivot->estimated_sales,
                'note' => $vendor->pivot->estimated_sales_note,
                'attended_market'=> $vendor->pivot->attended_market
            ];
        }

		return view('tools.show_estimated_sales', ['market_day' => $market_day,  'vendor' =>$first_vendor, 'estimated_sales' => $estimated_sales]);
//	  	return redirect()->action('ReportController@estimatedSales');

    }

    /**
     * Display the Estimated Sales module for a specific Market Day and Vendor
     *
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function showEstimatedSalesForVendor(MarketDay $market_day, Vendor $vendor)
    {
        foreach ($market_day->vendors as $key => $market_day_vendor) {
            if ( $market_day_vendor->id === $vendor->id ) {
                $vendor = $market_day_vendor;
				$vendors = $market_day->vendors->sortBy('pivot.id')->values();
                $prev_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() - 1 );
                $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );

            }
        }

        return view('tools.show_estimated_sales_for_vendor', compact('market_day', 'vendor', 'prev_vendor', 'next_vendor'));
    }

    /**
     * Update the Estimated Sales for a specific Market Day and Vendor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\MarketDay  $market_day
     * @param  App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function updateEstimatedSalesForVendor(Request $request, MarketDay $market_day, Vendor $vendor)
    {
	    $this->validate($request, [
            'estimated_sales' => 'required|numeric'
        ]);

        $estimated_sales = $request->input('estimated_sales');
        $estimated_sales_note = $request->input('estimated_sales_note');
        $market_day->vendors()->updateExistingPivot($vendor->id, ['estimated_sales' => $estimated_sales,'estimated_sales_note' => $estimated_sales_note,'collected_estimated_sales' => true]);

		// determine next vendor to pass as the vendor to display
        $vendors = $market_day->vendors->sortBy('pivot.id')->values();

        $next_vendor = $vendors->get( $vendors->where('id', $vendor->id)->keys()->first() + 1 );
		$vendor_type_name = VendorType::where('id',$vendor->vendor_type_id)->pluck('name')->first();

		$attended_market=$market_day->vendors()->where('vendor_id', $vendor->id)->wherePivot('attended_market',true)->count();

/* Ben says no need for reciepts anymore
		// if this is not local env, send email
        if ( config('app.env')!= 'local') {
			\Mail::to(config('app.admin_email'))->send(new EstimatedSalesReceipt($market_day, $vendor, $vendor_type_name));
        }
*/
        return redirect()->action('MarketToolsController@showEstimatedSales', ['market_day' => $market_day, 'vendor' => $next_vendor]);
    }
}
