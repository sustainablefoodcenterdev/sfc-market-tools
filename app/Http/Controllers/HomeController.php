<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MarketDay;

use Illuminate\Foundation\Auth\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// Get current market day, if any
		$today = date('Y/m/d 0:0:0');
		$tomorrow = date('Y-m-d', strtotime($today.' + 1 days'));
		$previous_month = date('Y-m-d', strtotime($today.' - 4 weeks'));
	    $today_market_days = MarketDay::where('date', '>=', $today )->where('date', '<', $tomorrow )->orderBy("date")->get();

	    $past_market_days= MarketDay::where('date', '<', $today )->where('date', '>', $previous_month )->orderBy("date",'desc')->get();
        return view('home',['today_market_days'=> $today_market_days, 'past_market_days'=>$past_market_days]);
    }
}
