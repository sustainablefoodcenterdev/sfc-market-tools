<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reports\BoothFeeReport;
use App\Reports\EstimatedSalesReport;
use App\Reports\CollectScrips;
use App\MarketVendorType;
use App\Vendor;
use App\MarketDay;
use App\Market;
use App\MarketDayVendor;
use App\Scrip;
use App\VendorType;
use \Auth;

class ReportController extends Controller
{

    public function boothFeesForm(Request $request) {

        $market_days=MarketDay::all()->sortByDesc("date");
		return view('reports.booth_fees_form',['market_days'=> $market_days]);
	}

    public function boothFees(Request $request, MarketDay $market_day) {

		$vendors = $market_day->vendors()->orderBy("name")->get();
		$booth_fees = [];
		$total_vendors=0;
		$total_booths=0;
		$total_fees=0;
		foreach ($vendors as $vendor) {

			// if note is NCNS or Called, show blanks
			if (! $vendor->pivot->attended_market) {
				$booth_count='';
				$booth_cost='';
				$total_fee='';
				$vendor_type='';
			}
			else {
				// Not NCNS or Called so add to totals

				$default_booth_fee = MarketVendorType::where('vendortype_id',$vendor->vendor_type_id)->where('market_id',$market_day->market_id)->pluck('booth_fee')->first();
				$booth_cost=$default_booth_fee;
				$total_vendors+=1;
				$total_booths+=$vendor->pivot->booths_paid_for;
				$total_fees+=$vendor->pivot->booth_fee;
				$booth_count=$vendor->pivot->booths_paid_for;

				$vendor_type=$vendor->type->name;
				$total_fee=$vendor->pivot->booth_fee;

			}

            $booth_fees[] = [
                'Vendor' => $vendor->name,
                'Type' => $vendor_type,
                'Booths' => $booth_count,
                'CostBooth' => $booth_cost,
                'Total' => $total_fee,
                'Market Notes (NSNC, etc.)' => $vendor->pivot->booth_fee_note
            ];
        }
        // totals
		$booth_fees[] = [
            'Vendor' => $total_vendors,
            'Type' => '',
            'Booths' => $total_booths,
            'CostBooth' => '',
            'Total' => $total_fees,
            'Market Notes (NSNC, etc.)' => ''
        ];

		// create and download Excel
        \Excel::create('BoothFeeReport', function($excel) use ($booth_fees, $vendors) {
            $excel->sheet('BoothFeeReport', function($sheet) use ($booth_fees, $vendors) {
                $sheet->fromArray($booth_fees);

                // set the vendor cell color
				$vendor_counter=2;
				foreach ($vendors  as $vendor) {

					$sheet->cell('A'.$vendor_counter, function($cell)  use ($vendor) {
					    // call cell manipulation methods
					    $cell->setBackground(VendorType::where('id',$vendor->vendor_type_id)->pluck('color')->first());
					});
					$vendor_counter+=1;

					// letter of last coumn
					$A_chr=65;
					$max_row_length=0;
					for ($i=1;$i < count($booth_fees); $i++) {
					if (count($booth_fees[$i]) > $max_row_length) {
						$max_row_length=count($booth_fees[$i]);
					}
					}
					$last_column_letter=chr($A_chr+$max_row_length);

					// total number of rows
					$sheet_row_count=count($booth_fees);

					// Set border for range
					$sheet->setBorder('A1:'.$last_column_letter.$sheet_row_count, 'thin');



				} // foreach vendor
            });
        })->download('xlsx');

        return '';
    }

    public function estimatedSalesForm(Request $request) {

        $market_days=MarketDay::all()->sortByDesc("date");
		return view('reports.estimated_sales_form',['market_days'=> $market_days]);
	}

    public function distributeScripForm(Request $request) {

        $market_days=MarketDay::all()->sortByDesc("date");
		return view('reports.distribute_scrip_form',['market_days'=> $market_days]);
	}


    public function estimatedSales(Request $request, MarketDay $market_day) {
		// get item from Market Day model that matches the session id
//		$market_day = MarketDay::where('id', $request->session()->get('market_day_id'))->first();

		$vendors = $market_day->vendors()->orderBy("name")->get();
		$estimated_sales[] = ['Name' =>$market_day->market->name.' '. $market_day->date->format('D, M jS, Y')];

		$estimated_sales[] = ['Name' =>'Name', 'Estimated' => 'Estimated','Note' =>'Notes'];

		// set divider
		array_push($estimated_sales,[' ']);

		$total_vendors=0;
		$total_sales=0;
		$max_sale=0;
		$first_time_through=true;
		foreach ($vendors as $vendor) {

			// if attended market, add
			if ($vendor->pivot->attended_market === 1){
				// if sales were collected
				if ($vendor->pivot->collected_estimated_sales===1) {
					// if data was entered for sales, use 0.01 if 0 was actually entered
					if ($vendor->pivot->estimated_sales == 0) {
						$estimated_sales_amount = $vendor->pivot->estimated_sales_note == 'Made $0' ? 0.01 : 'X';
					}
					else {
						$estimated_sales_amount = $vendor->pivot->estimated_sales;
					}
				}
				else {
					// not data was entered, show X on spreadsheet
					$estimated_sales_amount = 'X';
				}

	            $estimated_sales[] = [
	                'Name' => $vendor->name,
	                'Estimated' => $estimated_sales_amount,
	                'Note' => $vendor->pivot->estimated_sales_note
	            ];
	            $total_vendors+=1;
	            if ( is_numeric($estimated_sales_amount) ) {
		            $total_sales+=$estimated_sales_amount;
				}
	            // if sales is a number and more/less that min/max, update min/max
	            if ( is_numeric($estimated_sales_amount) &&$estimated_sales_amount > $max_sale) {
		            $max_sale = $estimated_sales_amount;
	            }
	            // if first time through, set min to sales and then lower flag
	            if (is_numeric($estimated_sales_amount) && $first_time_through==true) {
		            $min_sale = $estimated_sales_amount;
		            $first_time_through=false;
	            }
	            else if ( is_numeric($estimated_sales_amount) && $estimated_sales_amount < $min_sale) {
		            $min_sale = $estimated_sales_amount;
	            }
			} // if attended market
			else {
				// did not attend market
				 $estimated_sales[] = [
	                'Name' => $vendor->name,
	                'Estimated' => '',
	                'Note' => $vendor->pivot->booth_fee_note
	            ];

			}
        }

        // incase no sales, set $min_sale
        if (! isset($min_sale)) {
	        $min_sale=0;
        }


        // add totals
        $estimated_sales[] = [
                'name' => $total_vendors,
                'estimated' => $total_sales,
        ];
        // add average
        if ( count($vendors) <> 0) {
	        $this_estimated_sales = round($total_sales/count($vendors), 2);
        }
        else {
	        $this_estimated_sales=0;
        }
        $estimated_sales[] = [
                'name' => 'Ave',
                'estimated' => $this_estimated_sales,
        ];

        // add max & min
        $estimated_sales[] = [
                'name' => 'Max',
                'estimated' => $max_sale,
        ];
        // add max & min
        $estimated_sales[] = [
                'name' => 'Min',
                'estimated' => $min_sale,
        ];

//dd();
		\Excel::create('EstimatedSalesReport', function($excel) use ($estimated_sales, $vendors) {
            $excel->sheet('EstimatedSalesReport', function($sheet) use ($estimated_sales, $vendors) {
				$sheet->fromArray($estimated_sales,null,null,null,false);

				// Market name and date is brown
				$sheet->cell('A1', function($cell)  {
					    // call cell manipulation methods
					    $cell->setBackground('#C7BB99');
					});

				$sheet->mergeCells('A3:C3');
				$sheet->cell('A3:C3', function($cell)   {
				    // call cell manipulation methods
				    $cell->setBackground('#cccccc');
				});


                // set the vendor cell color
				$vendor_counter=4;
				foreach ($vendors  as $vendor) {

					$sheet->cell('A'.$vendor_counter, function($cell)  use ($vendor) {
					    // call cell manipulation methods
					    $cell->setBackground(VendorType::where('id',$vendor->vendor_type_id)->pluck('color')->first());
					});
					$vendor_counter+=1;

				} // foreach vendor

				// letter of last coumn
				$A_chr=65;
				$max_row_length=0;
				for ($i=1;$i < count($estimated_sales); $i++) {
				if (count($estimated_sales[$i]) > $max_row_length) {
					$max_row_length=count($estimated_sales[$i]);
				}
				}
				$last_column_letter=chr($A_chr+$max_row_length);

				// total number of rows
				$sheet_row_count=count($estimated_sales);

				// Set border for range
				$sheet->setBorder('A1:C'.$sheet_row_count, 'thin');

				// make row currency format
				$sheet->setColumnFormat(array(
				    'B' => '$#,##'
				));

            });
        })->download('xlsx');

		return '';
		// return view('reports.estimated_sales',  ['estimated_sales'=> $estimated_sales] );

    }

    public function collectScripForm(Request $request) {

        $market_days=MarketDay::all()->sortByDesc("date");
		return view('reports.collect_scrip_form',['market_days'=> $market_days]);
	}


	public function collectScrip(Request $request,  MarketDay $market_day ) {

		// get all vendors in market
		$vendors = $market_day->vendors()->orderBy("name")->get();

		// get all scrips in system
		$scrip_model = new Scrip;
		$scrips = $scrip_model->getReportScrips();

		// init the array to hold colected scrip info
		$collected_scrips[] = [];

		/** start with header info **/
		// denominations
		$scrip_info=[$market_day->market->name.' '. $market_day->date->format('D, M jS, Y')];
		foreach($scrips as $scrip) {
			array_push($scrip_info,'$'.$scrip->denomination);
		}
		array_push($collected_scrips,$scrip_info);

		// Vendor and scrips
		$scrip_info=['Vendor'];
		foreach($scrips as $scrip) {
			array_push($scrip_info,$scrip->name);
		}
		array_push($scrip_info,'Total');

		//MARKET gets Confirmed and Notes, DDIP gets market and Date
		if ( Auth::user()->hasRole(['superadmin','market_coordinator']) ) {
			array_push($scrip_info,'Confirmed','Notes');
		}
		if ( Auth::user()->hasRole(['superadmin','double_dollar_coordinator']) ) {
			array_push($scrip_info,'Market','Date');
		}
		array_push($collected_scrips,$scrip_info);

		// set divider
		array_push($collected_scrips,[' ']);


		// init column totals; first column is Total
		$column_total=[''=>'Column Totals $'];
		foreach($scrips as $scrip) {
			array_push($column_total,0);
		}

		// init column counts; first column is blank
		$column_count=[''=>'Column Count'];
		foreach($scrips as $scrip) {
			array_push($column_count,0);
		}


		foreach ($vendors  as $vendor) {
			// init the counter for row's total
			$row_total=0;

			$scrips_collected=$market_day->vendors_scrip_collected_info[$vendor->name];
			$pieces_array = [];
			// init array with name
			$row = [
                'name' => $vendor->name
			];

			// if there were any scrips colelcted, put in array
			if (array_key_exists('scrip_id',$scrips_collected) ) {

				// loop over all scrip ids and add toa rray if present
				$column_counter=0;
				foreach($scrips as $scrip) {
					// if scrip was colelcted, add value to array
					if ( array_key_exists($scrip->name,$scrips_collected['scrip_collected']) ) {
						// add value to array
						$this_value=$scrips_collected['scrip_collected'][$scrip->name]['pieces'];
						// update $row_total
						$row_total+=$this_value*$scrips_collected['scrip_collected'][$scrip->name]['denomination'];

						// add the value to the column's total
						$column_count[$column_counter]+=$this_value;
						$column_total[$column_counter]+=$this_value*$scrips_collected['scrip_collected'][$scrip->name]['denomination'];

					}// if scrip was collected, add value to array
					else {
						$this_value='';
					}// if scrip was colelcted, add value to array
					// push the value  onto the row's array
					array_push($row,$this_value);

					// inc the col count
					$column_counter+=1;

				} // foreach scrips

				// add the row total, market name and date to the array
				array_push($row,$row_total);

				// MARKET gets blanks for Confirmed and notes; DDIP gets market name and date
				if ( Auth::user()->hasRole(['superadmin','market_coordinator']) ) {
					array_push($row,' ',$scrips_collected['collected_scrip_note']);
				}
				if ( Auth::user()->hasRole(['superadmin','double_dollar_coordinator']) ) {
					array_push($row,$market_day->market->name,$market_day->date->format('D, M jS, Y'));
				} // if ddip
			} // // if there were any scrips colelcted, put in array

			// add the row array to the main array
			array_push($collected_scrips,$row);

        } // foreach vendors

		// add for each column
		array_push($collected_scrips,$column_count);

		// add Confirmed row
		array_push($collected_scrips,['Confirmed']);

		// add totals line at end
        // sum all totals
		$column_total[]=array_sum($column_total);
		array_push($collected_scrips,$column_total);

		/// remove first row which is empty
		unset($collected_scrips[0]);

		\Excel::create('CollectScripReport', function($excel) use ($collected_scrips, $vendors, $scrips) {
			$excel->sheet('CollectScripReport', function($sheet) use ($collected_scrips, $vendors, $scrips) {
				$sheet->fromArray($collected_scrips,null,null,null,false);

				// set the vendor cell color
				$vendor_counter=4;
				foreach ($vendors  as $vendor) {
					$sheet->cell('A'.$vendor_counter, function($cell)  use ($vendor) {
					    // call cell manipulation methods
					    $cell->setBackground(VendorType::where('id',$vendor->vendor_type_id)->pluck('color')->first());
					});
					$vendor_counter+=1;

				} // foreach vendor

				// set the column colors
				$scrip_counter=1;
				$A_chr=65;
				// letter of last coumn
				$max_row_length=0;
				for ($i=1;$i < count($collected_scrips); $i++) {
					if (count($collected_scrips[$i]) > $max_row_length) {
						$max_row_length=count($collected_scrips[$i]);
					}
				}
				$last_column_letter=chr($A_chr+$max_row_length);

				// total number of rows
				$sheet_row_count=count($collected_scrips);

				$start_row=3;
				$end_row=$start_row+$vendors->count();
				foreach ($scrips->sortBy("report_sort_order")  as $scrip) {

					// determine cell range
					$this_column=chr($A_chr+$scrip_counter);
					// set color
					$sheet->cell($this_column.$start_row.':'.$this_column.$end_row, function($cell)  use ($scrip) {
					    // call cell manipulation methods
					    $cell->setBackground($scrip->color);
					});
					// cell wrap
					$sheet->getStyle($this_column.'2')->getAlignment()->setWrapText(true);


					// column width
					$sheet->setWidth($this_column, 7);

					$scrip_counter+=1;

				} // foreach vendor

				// Column A is wider and A1 gets wrapping
				$sheet->setWidth('A', 25);
				$sheet->getStyle('A1')->getAlignment()->setWrapText(true);

				// merge the second row and make it grey bg
				$sheet->mergeCells('A'.$start_row.':'.$last_column_letter.$start_row);
				$sheet->cell('A'.$start_row.':'.$last_column_letter.$start_row, function($cell)   {
				    // call cell manipulation methods
				    $cell->setBackground('#cccccc');
				});

				// Set border for range
				$sheet->setBorder('A1:'.$last_column_letter.$sheet_row_count, 'thin');
            });

        })->download('xlsx');

		return '';

    }

	public function home() {
		// get item from Market Day model that matches the session id
		$market_days = MarketDay::all();

//		$market_day_vendor = MarketDayVendor::where('id', $request->session()->get('market_day_id'))->first();

		return view('reports.home',  ['market_days'=> $market_days] );

    }

	public function distributeScrip(Request $request,  MarketDay $market_day ) {

		// get all scrips
		$scrips = Scrip::all()->sortBy("report_sort_order");

		// init the array to hold colected scrip info
		$distributed_scrips[] = [];

		// set header
		$scrip_info=['Shoppers First Name','First time at this market? (Mark 1 if yes)','Shoppers Zip Code','Last 4 #s of SNAP Card','Last group of  4 #s of WIC card','# of FMNP booklets','Date of Market','SNAP EBT','SNAP DBL','WIC EBT','WIC DBL','FMNP Voucher Booklet','FMNP DBL','TOT DBL','TOT  Benefit use','TOTAL TRANS'];
		array_push($distributed_scrips,$scrip_info);
		// set divider
		array_push($distributed_scrips,[' ']);


		// for each marketday customer, loop
		foreach ($market_day->scrip_sales as $scrip_sale) {

			// init row
			$row=[];
			$total_amount=0;
			$total_ddip=0;
//dd($scrip_sale);
			// NAME
			array_push($row,$scrip_sale->name);
			// add 1 if FIRST TIME
			$this_value = ($scrip_sale->first_market_visit) ? 1 : ' ';
			array_push($row,$this_value);
			//ZIP
			array_push($row,$scrip_sale->zip_code);

			//SNAP card number
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 5)->first()->pivot->card_number) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 5)->first()->pivot->card_number : ' ';
			array_push($row,$this_value);

			//SNAP card number: used for both WIC or FMNP
			if ( isset($scrip_sale->scrips()->wherePivot('scrip_id', 8)->first()->pivot->card_number) ) {
				$this_value = $scrip_sale->scrips()->wherePivot('scrip_id', 8)->first()->pivot->card_number;
			}
			elseif (isset($scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->card_number) ) {
				$this_value = $scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->card_number;
			}
			else {
				$this_value = ' ';
			}
			array_push($row,$this_value);



			//FMNP # of booklets: there are 30 scrip in a booklet
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->card_number) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->amount / 30 : ' ';
			array_push($row,$this_value);

			//DATE
			array_push($row,$market_day->date->format('m/j/y'));

			// RED SNAP
			$this_value = 0;
			$this_value += (isset($scrip_sale->scrips()->wherePivot('scrip_id', 5)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 5)->first()->pivot->amount * Scrip::all()->where('id', 5)->first()->denomination : 0;
			$this_value += (isset($scrip_sale->scrips()->wherePivot('scrip_id', 6)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 6)->first()->pivot->amount * Scrip::all()->where('id', 6)->first()->denomination : 0;
			array_push($row,$this_value);
			$total_amount+=$this_value;
			// ddip
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 7)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 7)->first()->pivot->amount * Scrip::all()->where('id', 7)->first()->denomination : 0;
			array_push($row,$this_value);
			$total_ddip+=$this_value;

			//WIC
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 8)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 8)->first()->pivot->amount * Scrip::all()->where('id', 8)->first()->denomination : 0;
			array_push($row,$this_value);
			$total_amount+=$this_value;
			// ddip
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 9)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 9)->first()->pivot->amount * Scrip::all()->where('id', 9)->first()->denomination : 0;
			array_push($row,$this_value);
			$total_ddip+=$this_value;

			//FMNP
			$this_value = (isset($scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->amount) ) ?  $scrip_sale->scrips()->wherePivot('scrip_id', 2)->first()->pivot->amount  : 0;
			array_push($row,$this_value);
			$total_amount+=$this_value;
			// ddip
			// FMNP uses same value for ddip
			array_push($row,$this_value);
			$total_ddip+=$this_value;

			// TOTALS
			array_push($row,$total_ddip,$total_amount,$total_ddip+$total_amount);

			// add to report array
			array_push($distributed_scrips,$row);

		} // foreach $market_day

		// colors for cells
		$cell_colors=['#ffffff','#fecb9c','#fecb9c','#ffff66','#ceffff','#fecb9c','#ffffff','#ffff66','#ffff66',
					  '#ceffff','#ceffff','#fecb9c','#fecb9c','#CB9CFC','#CB9CFC','#CB9CFC'];

		\Excel::create('DistributeScripReport', function($excel) use ($distributed_scrips,$cell_colors) {
			$excel->sheet('DistributeScripReport', function($sheet) use ($distributed_scrips,$cell_colors) {
				$sheet->fromArray($distributed_scrips,null,null,null,false);

				// set header cell backgrounds,column width, and wrap
				$A_chr=65;
				// letter of last coumn
				$max_row_length=0;
				for ($i=1;$i < count($distributed_scrips); $i++) {
					if (count($distributed_scrips[$i]) > $max_row_length) {
						$max_row_length=count($distributed_scrips[$i]);
					}
				}
				$last_column_letter=chr($A_chr+$max_row_length);
				// total number of rows
				$sheet_row_count=count($distributed_scrips);

				for ($i = 0; $i < count($cell_colors); $i++) {
					$this_column=chr($A_chr+$i);
					// cell color and wrap
					$sheet->cell($this_column.'2', function($cell) use ($cell_colors, $i) {
						    // call cell manipulation methods
						    $cell->setBackground($cell_colors[$i]);
						});
					$sheet->getStyle($this_column.'2')->getAlignment()->setWrapText(true);

					// column width
					$sheet->setWidth($this_column, 9);

				} // for

				// merge the second row and make it gret bg
				$sheet->mergeCells('A3:P3');
				$sheet->cell('A3:P3', function($cell)   {
				    // call cell manipulation methods
				    $cell->setBackground('#cccccc');
				});

				// column width
				$sheet->setWidth('A', 12);

				// Set border for range
				$sheet->setBorder('A1:'.$last_column_letter.$sheet_row_count, 'thin');


            });

        })->download('xlsx');

		return '';

    }


	public function tableauExportForm(Request $request) {

        $market_days=MarketDay::all()->sortByDesc("date");
		return view('reports.tableau_export_form',['market_days'=> $market_days]);
	}

    public function tableauExport(Request $request, MarketDay $market_day) {

		// get the vendors for the passed marketday
		$vendors = $market_day->vendors()->orderBy("name")->get();

		// START the export
		$tableau_export[] = ['Name' =>'Name', 'Type' => 'Type', 'Category' =>'Category', 'Comments' =>'Mkt Comments', 'Preciptiation' =>'Preciptiation', 'Rain' =>'Rain Y/N', 'Wind' =>'Wind', 'Booths' =>'# Booths', 'Estimated' =>'Estimated', 'Notes' =>'Est Notes', 'Donation' =>'Taste Donation'];

		// set divider
		array_push($tableau_export,[' ']);
/*
		$total_vendors=0;
		$total_sales=0;
		$max_sale=0;
		$first_time_through=true;
*/
		foreach ($vendors as $vendor) {
//dd($vendor);
			$vendor_type_name = VendorType::where('id',$vendor->vendor_type_id)->pluck('name')->first();
			// if attended market, add
			if ($vendor->pivot->attended_market === 1){
	            $tableau_export[] = [
	                'Name' => $vendor->name,
	                'Type' => $vendor_type_name,
'Category' => "TBD",
	                'Comments' => $market_day->market_comments,
'Temp' => "TBD",
'Preciptiation' => "TBD",
'Rain' => "TBD",
'Wind' => "TBD",
					'Booths' => $vendor->pivot->booths_paid_for,
	                'Estimated' => $vendor->pivot->estimated_sales,
	                'Note' => $vendor->pivot->estimated_sales_note,
'Donation' => "TBD",
	            ];

			} // if attended market
			else {
				// did not attend market
				 $tableau_export[] = [
	                'Name' => $vendor->name,
					'Type' => $vendor_type_name,
	            ];

			}
        }
		// return view('reports.tableau_export',  ['estimated_sales'=> $estimated_sales] );


		\Excel::create('TableauExportReport', function($excel) use ($tableau_export, $vendors) {
            $excel->sheet('TableauExportReport', function($sheet) use ($tableau_export, $vendors) {
				$sheet->fromArray($tableau_export,null,null,null,false);
/*
				// Market name and date is brown
				$sheet->cell('A1', function($cell)  {
					    // call cell manipulation methods
					    $cell->setBackground('#C7BB99');
					});

				$sheet->mergeCells('A3:C3');
				$sheet->cell('A3:C3', function($cell)   {
				    // call cell manipulation methods
				    $cell->setBackground('#cccccc');
				});


                // set the vendor cell color
				$vendor_counter=4;
				foreach ($vendors  as $vendor) {

					$sheet->cell('A'.$vendor_counter, function($cell)  use ($vendor) {
					    // call cell manipulation methods
					    $cell->setBackground(VendorType::where('id',$vendor->vendor_type_id)->pluck('color')->first());
					});
					$vendor_counter+=1;

				} // foreach vendor

				// letter of last coumn
				$A_chr=65;
				$max_row_length=0;
				for ($i=1;$i < count($tableau_export); $i++) {
				if (count($tableau_export[$i]) > $max_row_length) {
					$max_row_length=count($tableau_export[$i]);
				}
				}
				$last_column_letter=chr($A_chr+$max_row_length);

				// total number of rows
				$sheet_row_count=count($tableau_export);

				// Set border for range
				$sheet->setBorder('A1:C'.$sheet_row_count, 'thin');

				// make row currency format
				$sheet->setColumnFormat(array(
				    'B' => '$#,##'
				));
*/
            });
        })->download('xlsx');

		return '';

    }


}