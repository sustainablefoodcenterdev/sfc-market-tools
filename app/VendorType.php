<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorType extends Model
{
    use SoftDeletes;

    public function markets() {
        return $this->belongsToMany('App\Market', 'market_vendor_types', 'vendortype_id', 'market_id')
            ->withPivot('id', 'booth_fee')
            ->withTimestamps();
    }
}
