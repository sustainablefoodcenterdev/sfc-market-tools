<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CollectScripReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $market_day;

    public $vendor;

    public $collected_scrips = [];

    public $total_pieces = [];
    public $total_amount = [];


    public $vendor_type_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($market_day, $vendor, $vendor_type_name)
    {
        $this->market_day = $market_day;

        $this->vendor = $market_day->vendors->where('id', $vendor->id)->first();

		// init the array
        $collected_scrips = [];

        $vendor->pivot
            ->scrips // relationship of MarketDayVendor
            ->each(function ($scrip) use (&$collected_scrips) {
                $collected_scrips[$scrip->name] = ['pieces' => $scrip->pivot->pieces_collected, 'amount'=> $scrip->pivot->pieces_collected * $scrip->denomination];

            });

        $this->collected_scrips = $collected_scrips;
        $this->total_pieces=0;
		$this->total_amount=0;

		$this->vendor_type_name = $vendor_type_name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.collect_scrip_receipt')->subject('Collected Scrip Receipt');
    }
}
