<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoothFeeReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $market_day;

    public $vendor;

    public $default_booth_fee;

    public $vendor_type_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($market_day, $vendor, $default_booth_fee, $vendor_type_name)
    {
        $this->market_day = $market_day;

        $this->vendor = $market_day->vendors->where('id', $vendor->id)->first();

        $this->default_booth_fee = $default_booth_fee;

        $this->vendor_type_name = $vendor_type_name;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.booth_fee_receipt');
    }
}
