<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EstimatedSalesReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $market_day;

    public $vendor;

    public $vendor_type_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($market_day, $vendor, $vendor_type_name)
    {
        $this->market_day = $market_day;

        $this->vendor = $market_day->vendors->where('id', $vendor->id)->first();

        $this->vendor_type_name = $vendor_type_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.estimated_sales_receipt')->subject('Estimated Sales Receipt');

    }
}
