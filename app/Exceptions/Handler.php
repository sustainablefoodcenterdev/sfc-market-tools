<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\Debug\Exception\FlattenException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
// report 404s        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {

		if ($this->shouldReport($exception)) {
            // bind the event ID for Feedback
            $this->sentryID = app('sentry')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
	public function render($request, Exception $exception)
    {
	    return parent::render($request, $exception);
/*
	    // attach methods like getStatusCode
	    if (!$exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }
        // define current error code.
		$error_code=$exception->getStatusCode();
		// borrowed withr espect from http://stackoverflow.com/questions/35488801/laravel-request-validation-throwing-httpresponseexception-in-version-5-2
		//  handle the HttpResponseException by simply returning the response the same way the ExceptionHandler class does.
		//
		if ($exception instanceof TokenMismatchException) {
		    return redirect()->back()->withInput()->with('error', 'Your Session has Expired');
		}
		if ($exception instanceof HttpResponseException) {
		    return $exception->getResponse();
		}

		// return appropriate error page
        return response()->view('errors.'.$error_code, [
            'sentryID' => $this->sentryID,
        ], $error_code);
*/
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
