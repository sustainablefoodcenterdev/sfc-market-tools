<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('market_id')->unsigned();
			$table->datetime('date')->nullable();
			$table->string('market_comments',1000)->nullable();
			$table->string('scrip_comments',1000)->nullable();
            $table->timestamps();
            $table->softDeletes(); // like active column

            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_days', function (Blueprint $table) {
            $table->dropForeign('market_days_market_id_foreign');
        });

        Schema::dropIfExists('market_days');
    }
}
