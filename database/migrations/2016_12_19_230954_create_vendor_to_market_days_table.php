<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorToMarketDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_to_market_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('market_id')->unsigned();
            $table->integer('market_day_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->float('booth_fee')->default(0);
			$table->float('booths_paid_for')->default(0);
            $table->string('booth_fee_note')->default('')->nullable();
			$table->float('estimated_sales')->default(0);
            $table->string('estimated_sales_note')->default('')->nullable();
            $table->string('collected_scrip_note')->default('')->nullable();
			$table->integer('location_order')->default(0);
			$table->boolean('attended_market')->default(false);
            $table->boolean('collected_booth_fee')->default(false);
            $table->boolean('collected_estimated_sales')->default(false);
            $table->boolean('collected_scrip')->default(false);
            $table->timestamps();
            $table->softDeletes(); // like active column
            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade');
            $table->foreign('market_day_id')->references('id')->on('market_days')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_to_market_days', function (Blueprint $table) {
            $table->dropForeign('vendor_to_market_days_market_id_foreign');
            $table->dropForeign('vendor_to_market_days_market_day_id_foreign');
            $table->dropForeign('vendor_to_market_days_vendor_id_foreign');
        });

        Schema::dropIfExists('vendor_to_market_days');
    }
}
