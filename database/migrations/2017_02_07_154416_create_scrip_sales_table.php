<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScripSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrip_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('zip_code');
            $table->boolean('first_market_visit');
            $table->string('distribute_scrip_note')->nullable();
            $table->integer('market_day_id')->unsigned();
            $table->timestamps();

            $table->foreign('market_day_id')->references('id')->on('market_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrip_sales');
    }
}
