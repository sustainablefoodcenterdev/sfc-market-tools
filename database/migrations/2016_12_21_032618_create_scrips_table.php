<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('denomination')->default(''); // added :EL
            $table->string('color')->default(''); // added :EL
            $table->boolean('distributed_at_market')->nullable();
			$table->integer('dashboard_sort_order');
			$table->integer('report_sort_order');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrips');
    }
}
