<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScripSalesScripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrip_sales_scrips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scrip_sale_id')->unsigned();
            $table->integer('scrip_id')->unsigned();
            $table->integer('amount')->default(0);
            $table->integer('card_number')->default(0);
            $table->timestamps();

            $table->foreign('scrip_id')->references('id')->on('scrips');
            $table->foreign('scrip_sale_id')->references('id')->on('scrip_sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrip_sales_scrips');
    }
}
