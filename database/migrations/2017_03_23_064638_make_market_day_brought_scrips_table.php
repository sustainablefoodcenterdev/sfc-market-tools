<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMarketDayBroughtScripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_day_brought_scrips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('market_day_id')->unsigned();
            $table->integer('scrip_id')->unsigned();
            $table->integer('pieces');
            $table->timestamps();

            $table->foreign('market_day_id')->references('id')->on('market_days')->onDelete('cascade');
            $table->foreign('scrip_id')->references('id')->on('scrips')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_day_brought_scrips');
    }
}
