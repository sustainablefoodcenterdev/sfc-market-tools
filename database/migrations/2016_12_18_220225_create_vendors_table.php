<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('email')->default('');
            $table->string('phone')->default('');
            $table->integer('vendor_type_id')->unsigned()->nullable();
            $table->boolean('email_receipt')->default(false)->nullable(); // need to be nullable
            $table->boolean('print_receipt')->default(false)->nullable(); // need to be nullable
            $table->timestamps();
            $table->softDeletes(); // like active column

			$table->foreign('vendor_type_id')->references('id')->on('vendor_types')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
