<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScripToVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrip_to_vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_to_market_day_id')->unsigned();
            $table->integer('scrip_id');
            $table->integer('pieces_collected')->default(0);
            $table->timestamps();

			$table->foreign('vendor_to_market_day_id')->references('id')->on('vendor_to_market_days')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrip_to_vendors');
    }
}
