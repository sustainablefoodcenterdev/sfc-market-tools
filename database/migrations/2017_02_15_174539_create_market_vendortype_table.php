<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketVendortypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_types', function (Blueprint $table) {
            $table->dropColumn('booth_fee');
        });

        Schema::create('market_vendor_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('market_id')->unsigned();
            $table->integer('vendortype_id')->unsigned();
            $table->float('booth_fee')->default(0);
            $table->timestamps();

            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade');
			$table->foreign('vendortype_id')->references('id')->on('vendor_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_vendor_types');

        Schema::table('vendor_types', function (Blueprint $table) {
            $table->float('booth_fee')->after('name');
        });
    }
}
