<?php

use Illuminate\Database\Seeder;

class MarketVendorTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendor_info = [
	        ['market_id' => 1,'vendortype_id'=> 1, 'booth_fee'=>50],
	        ['market_id' => 2,'vendortype_id'=> 1, 'booth_fee'=>35],
	        ['market_id' => 3,'vendortype_id'=> 1, 'booth_fee'=>35],

	        ['market_id' => 1,'vendortype_id'=> 7, 'booth_fee'=>50],
	        ['market_id' => 2,'vendortype_id'=> 7, 'booth_fee'=>35],
	        ['market_id' => 3,'vendortype_id'=> 7, 'booth_fee'=>45],

	        ['market_id' => 1,'vendortype_id'=> 2, 'booth_fee'=>45],
	        ['market_id' => 2,'vendortype_id'=> 2, 'booth_fee'=>30],
	        ['market_id' => 3,'vendortype_id'=> 2, 'booth_fee'=>40],

	        ['market_id' => 1,'vendortype_id'=> 5, 'booth_fee'=>45],
	        ['market_id' => 2,'vendortype_id'=> 5, 'booth_fee'=>30],
	        ['market_id' => 3,'vendortype_id'=> 5, 'booth_fee'=>40],

	        ['market_id' => 1,'vendortype_id'=> 6, 'booth_fee'=>50],
	        ['market_id' => 2,'vendortype_id'=> 6, 'booth_fee'=>35],
	        ['market_id' => 3,'vendortype_id'=> 6, 'booth_fee'=>45],

	        ['market_id' => 1,'vendortype_id'=> 3, 'booth_fee'=>45],
	        ['market_id' => 2,'vendortype_id'=> 3, 'booth_fee'=>30],
	        ['market_id' => 3,'vendortype_id'=> 3, 'booth_fee'=>40],

	        ['market_id' => 1,'vendortype_id'=> 4, 'booth_fee'=>50],
	        ['market_id' => 2,'vendortype_id'=> 4, 'booth_fee'=>35],
	        ['market_id' => 3,'vendortype_id'=> 4, 'booth_fee'=>45],

	        ['market_id' => 1,'vendortype_id'=> 8, 'booth_fee'=>50],
	        ['market_id' => 2,'vendortype_id'=> 8, 'booth_fee'=>35],
	        ['market_id' => 3,'vendortype_id'=> 8, 'booth_fee'=>35],
	  	    ];

        foreach ($vendor_info as $vendor_info_raw) {
            $vendor_info = new App\MarketVendorType;
            $vendor_info->fill($vendor_info_raw);
            $vendor_info->save();
        }
    }
}
