<?php

use Illuminate\Database\Seeder;

class ScripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scrips = [
			['name' => 'BCBS', 'denomination' => '2', 'color' => '#fecb9c', 'distributed_at_market'=>0,'dashboard_sort_order'=>10,'report_sort_order'=>90],
			['name' => 'FMNP DDIP', 'denomination' => '1', 'color' => '#ffff66', 'distributed_at_market'=>1,'dashboard_sort_order'=>90,'report_sort_order'=>60],
            ['name' => 'Green $1', 'denomination' => '1', 'color' => '#cdfece', 'distributed_at_market'=>0,'dashboard_sort_order'=>20,'report_sort_order'=>10],
			['name' => 'Green $5', 'denomination' => '5', 'color' => '#cdfece', 'distributed_at_market'=>0,'dashboard_sort_order'=>30,'report_sort_order'=>20],
            ['name' => 'SNAP EBT $1', 'denomination' => '1', 'color' => '#fd9bcb', 'distributed_at_market'=>1,'dashboard_sort_order'=>40,'report_sort_order'=>30],
			['name' => 'SNAP EBT $5', 'denomination' => '5', 'color' => '#fd9bcb', 'distributed_at_market'=>1,'dashboard_sort_order'=>50,'report_sort_order'=>40],
			['name' => 'SNAP DDIP', 'denomination' => '1', 'color' => '#ffff66', 'distributed_at_market'=>1,'dashboard_sort_order'=>60,'report_sort_order'=>50],
			['name' => 'WIC Stamped', 'denomination' => '1', 'color' => '#ceffff', 'distributed_at_market'=>1,'dashboard_sort_order'=>70,'report_sort_order'=>80],
			['name' => 'WIC DDIP', 'denomination' => '1', 'color' => '#ffff66', 'distributed_at_market'=>1,'dashboard_sort_order'=>80,'report_sort_order'=>70],

        ];

        foreach ($scrips as $scrip_raw) {
            $scrip = new App\Scrip;
            $scrip->fill($scrip_raw);
            $scrip->save();
        }

        $scrip_ids = App\Scrip::all()->pluck('id')->all();

        App\Vendor::all()->each(function ($vendor) use ($scrip_ids) {
            $to_sync = [];
            foreach ($scrip_ids as $scrip_id) {
                $to_sync[$scrip_id] = ['accepted' => true];
            }
            $vendor->scrips()->sync($to_sync);
        });
    }
}
