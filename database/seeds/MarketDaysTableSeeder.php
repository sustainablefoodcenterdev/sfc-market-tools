<?php

use Illuminate\Database\Seeder;

class MarketDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $vendor_ids = App\Vendor::all()->pluck('id')->shuffle();
        $market_ids = App\Market::all()->pluck('id');

        factory(App\MarketDay::class, 15)->create()->each(function ($md) use ($faker, $market_ids, $vendor_ids) {
            $trim_amount = $faker->numberBetween(10,30);
            $vendor_ids = $vendor_ids->slice( 0, $trim_amount );
            $vendors = [];

            $vendor_ids->each(function ($vendor_id, $key) use ($market_ids, &$vendors) {
                $vendors[ $vendor_id ] = ['market_id' => $market_ids->random()];
                return $vendor_id;
            });

            $md->vendors()->attach( $vendors );
        });
    }
}
