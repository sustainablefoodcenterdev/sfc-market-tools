<?php

use Illuminate\Database\Seeder;

class VendorTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
		 $vendortypes = [
            ['name' => 'Artisian', 'color' => '#D89595'],
            ['name' => 'Farmer', 'color' => '#C2D59E'],
            ['name' => 'Rancher', 'color' => '#FFFE9F'],
            ['name' => 'Ready to Eat', 'color' => '#96B4D6'],
            ['name' => 'Other', 'color' => '#B0AFA7'],
            ['name' => 'Prepared Food', 'color' => '#94CDDB'],
            ['name' => 'Cottage Food', 'color' => '#D2DBDB'],
            ['name' => 'Service', 'color' => '#F2DCDC'],
        ];

        foreach ($vendortypes as $vendortype_raw) {
            $vendortype = new App\VendorType;
            $vendortype->fill($vendortype_raw);
            $vendortype->save();
        }


    }
}