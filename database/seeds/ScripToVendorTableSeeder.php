<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScripToVendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scrips = App\Scrip::all();
        $vendors = App\Vendor::all();
        $market_days = App\MarketDay::all();
        $faker = Faker\Factory::create();

        foreach ($market_days as $market_day) {
            foreach ($market_day->vendors as $vendor) {
                $add_scrips = $faker->boolean(75);
                if ( $add_scrips ) {

                    DB::table('scrip_to_vendors')->insert([
                        'vendor_to_market_day_id' => $vendor->pivot->id,
                        'scrip_id' => $scrips->pluck('id')->random(),
                        'pieces_collected' => rand(0,1000)
                    ]);

                }
            }
        }
    }
}
