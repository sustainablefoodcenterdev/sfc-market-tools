<?php

use Illuminate\Database\Seeder;

class ScripTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		 $scriptypes = [
            ['name' => 'Snap'],
            ['name' => 'Wic'],
            ['name' => 'FMNP'],
        ];

        foreach ($scriptypes as $scriptype_raw) {
            $scriptype = new App\ScripType;
            $scriptype->fill($scriptype_raw);
            $scriptype->save();
        }


    }
}