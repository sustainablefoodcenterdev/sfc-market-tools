<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class MarketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	     $markets = [
            ['name' => 'Downtown', 'day' => '6', 'image' => 'downtown.jpg'],
            ['name' => 'Triangle', 'day' => '3', 'image' => 'triangle.jpg'],
            ['name' => 'Sunset Valley', 'day' => '6', 'image' => 'sunsetvalley.jpg'],
        ];

        foreach ($markets as $market_raw) {
            if (Storage::disk('approot')->has('storage/app/public/img/markets/' . $market_raw['image'])) {
                unlink(base_path('storage/app/public/img/markets/' . $market_raw['image']));
            }
            Storage::disk('approot')->copy(
                'public/img/markets/' . $market_raw['image'],
                'storage/app/public/img/markets/' . $market_raw['image']
            );

            $market = new App\Market;
            $market->fill($market_raw);
            $market->save();
        }
    }
}
