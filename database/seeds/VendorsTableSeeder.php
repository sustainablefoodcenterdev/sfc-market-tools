<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendors = [
            ['name' => 'Taco Deli', 'email' => 'eric@webchronic.com', 'phone' => '512-123-4567', 'vendor_type_id' => '3', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'B5 Farm', 'email' => 'eric@webchronic.com', 'phone' => '512-987-6543', 'vendor_type_id' => '1', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Austin Honey Company', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '4', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Hemp 360', 'email' => 'eric@webchronic.com', 'phone' => '317-123-4567', 'vendor_type_id' => '7', 'email_receipt' => '1', 'print_receipt' => '0'],

            ['name' => 'Esperanza', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '2', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Johnsons Backyard Garden', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '2', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Margarets Farm', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '2', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'ATX Homemade Jerky', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '6', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Ranger Cattle', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '3', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Goodwin Homestead', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '1', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'Happy Vegan', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '4', 'email_receipt' => '1', 'print_receipt' => '0'],
            ['name' => 'El Cruz Ranch', 'email' => 'eric@webchronic.com', 'phone' => '605-230-9015', 'vendor_type_id' => '4', 'email_receipt' => '1', 'print_receipt' => '0']
        ];

        foreach ($vendors as $vendor_raw) {
            $vendor = new App\Vendor;
            $vendor->fill($vendor_raw);
            $vendor->save();
        }

//		factory(App\Vendor::class, 30)->create(); // creates fake data; above array and foreach inserts real data

    }
}
