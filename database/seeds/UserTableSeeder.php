<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'superadmin']);
        Role::create(['name' => 'double_dollar_coordinator']);
        Role::create(['name' => 'market_coordinator']);
        Role::create(['name' => 'double_dollar_associate']);
        Role::create(['name' => 'market_crew']);

        $admin_users = [
            ['name' => 'Admin',  'username' => 'admin', 'password' => Hash::make( 'N2r5gRvT' ), 'remember_token' => '1'],
        ];

        foreach ($admin_users as $user_raw) {
            $user = new App\User;
            $user->fill($user_raw);
            $user->save();
            $user->assignRole('superadmin');
        }

		$dd_coordinator_users = [
            ['name' => 'DDIP Coordinator',  'username' => 'dd_coordinator', 'password' => Hash::make( '49Y7g3QF' ), 'remember_token' => '1'],
        ];

        foreach ($dd_coordinator_users as $user_raw) {
            $user = new App\User;
            $user->fill($user_raw);
            $user->save();
            $user->assignRole('double_dollar_coordinator');
        }


		$dd_users = [
            ['name' => 'DDIP Associate',  'username' => 'dd_associate', 'password' => Hash::make( 'A9pss2wE' ), 'remember_token' => '1'],
        ];

        foreach ($dd_users as $user_raw) {
            $user = new App\User;
            $user->fill($user_raw);
            $user->save();
            $user->assignRole('double_dollar_associate');
        }

		$crew_coordinator_users = [
            ['name' => 'Market Coordinator',  'username' => 'market_coordinator', 'password' => Hash::make( 'wJUr2RVV' ), 'remember_token' => '1'],
        ];
        foreach ($crew_coordinator_users as $user_raw) {
            $user = new App\User;
            $user->fill($user_raw);
            $user->save();
            $user->assignRole('market_coordinator');
        }

        $crew_users = [
            ['name' => 'Market Crew',  'username' => 'market_crew', 'password' => Hash::make( 'F3FzZ62R' ), 'remember_token' => '1'],
        ];
        foreach ($crew_users as $user_raw) {
            $user = new App\User;
            $user->fill($user_raw);
            $user->save();
            $user->assignRole('market_crew');
        }


    }
}
