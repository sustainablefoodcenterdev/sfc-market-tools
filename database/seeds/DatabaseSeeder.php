<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(MarketTableSeeder::class);
        $this->call(VendorTypesTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
//        $this->call(MarketDaysTableSeeder::class);
        $this->call(MarketVendorTypeTableSeeder::class);
        // $this->call(VendortoMarketDaysTableSeeder::class);
        $this->call(ScripsTableSeeder::class);
//        $this->call(ScripToVendorTableSeeder::class);
        $this->call(ScripTypesTableSeeder::class);
        $this->call(VendorCategoriesTableSeeder::class);
    }
}
