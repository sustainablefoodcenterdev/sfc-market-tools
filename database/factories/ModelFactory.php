<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/* Table is now seeded with real data
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
*/

/* Table is now seeded with real data
$factory->define(App\Market::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,   // company is faker type that will use text that seems like a Co. name
        'day' => $faker->numberBetween($min = 1, $max = 7) ,
		'image' => $faker->imageUrl($width = 640, $height = 480, "cats"	),
    ];
});
*/

/* Table is now seeded with real data
$factory->define(App\Vendor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,   // company is faker type that will use text that seems like a Co. name
		'email' => $faker->email(),
		'phone' => $faker->phoneNumber(),
		'email_receipt' => $faker->boolean(),
		'print_receipt' => $faker->boolean()
    ];
});
*/
/*
$factory->define(App\MarketDay::class, function (Faker\Generator $faker) {
    return [
	    'scrip_comments'=>$faker->sentence($nbWords = 6, $variableNbWords = true),
        'date'=> $faker->dateTimeThisMonth($max = '+ 5 days'),   // need some in future, some in past.
        'market_id' => $faker->numberBetween($min = 1, $max = 3)
    ];
});

$factory->define(App\VendorType::class, function (Faker\Generator $faker) {
    return [
        'name'=> $faker->unique()->randomElement($array = array ('Value Added','Farmer','Rancher/Other','Ready to Eat'))
    ];
});

$factory->define(App\Scrip::class, function (Faker\Generator $faker) {
    return [
        'name'=> $faker->unique()->randomElement($array = array ('Red 1','Red 5','SNAP'))
    ];
});
*/
