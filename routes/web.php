<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

	/*  TOOLS */

	// Tools home
    Route::get('/tools', 'MarketToolsController@home')->name('tools.home');
	// Reset market day - must be above /tools/{market_day} since that matches /tools/reset
	Route::get('/tools/reset', 'MarketToolsController@resetMarketDay')->name('tools.reset');
	// maanges a particular market day
    Route::get('/tools/dashboard/{market_day}', 'MarketToolsController@dashboard')->name('tools.dashboard');



    // Distribute Scrips
    Route::get('/tools/{market_day}/distribute_scrip', 'DistributeScripController@index')->name('tools.distribute_scrip.index');
    Route::get('/tools/{market_day}/distribute_scrip/create', 'DistributeScripController@create')->name('tools.distribute_scrip.create');
    Route::post('/tools/{market_day}/distribute_scrip', 'DistributeScripController@storeOrUpdate')->name('tools.distribute_scrip.store');
    Route::get('/tools/{market_day}/distribute_scrip/{scrip_sale}/', 'DistributeScripController@edit')->name('tools.distribute_scrip.edit');
    Route::patch('/tools/{market_day}/distribute_scrip/{scrip_sale}/', 'DistributeScripController@storeOrUpdate')->name('tools.distribute_scrip.update');
	Route::delete('/tools/{market_day}/distribute_scrip/{scrip_sale}', 'DistributeScripController@removeScripSaleFromMarketDay')->name('tools.distribute_scrip.destroy');



	// BOOTH FEES
    Route::get('/tools/{market_day}/booth_fees', 'MarketToolsController@showBoothFees')->name('tools.show_booth_fees');
    Route::get('/tools/{market_day}/booth_fees/{vendor}', 'MarketToolsController@showBoothFeesForVendor')->name('tools.show_booth_fees_for_vendor');
    Route::patch('/tools/{market_day}/booth_fees/{vendor}', 'MarketToolsController@updateBoothFeesForVendor')->name('tools.update_booth_fees_for_vendor');



    // Collect Scrips
    Route::get('/tools/{market_day}/collect_scrips', 'MarketToolsController@showCollectScrips')->name('tools.show_collect_scrips');
    Route::get('/tools/{market_day}/collect_scrips/{vendor}', 'MarketToolsController@showCollectScripsForVendor')->name('tools.show_collect_scrips_for_vendor');
    Route::patch('/tools/{market_day}/collect_scrips/{vendor}', 'MarketToolsController@updateCollectScripsForVendor')->name('tools.update_collect_scrips_for_vendor');
    Route::post('/tools/{market_day}/collect_scrips/{vendor}', 'MarketToolsController@addScripToVendor')->name('tools.add_scrip_to_vendor');
    Route::delete('/tools/{market_day}/collect_scrips/{vendor}/{scrip}', 'MarketToolsController@removeScripFromVendor')->name('tools.remove_scrip_from_vendor');
//	Route::get('/reports/collect_scrips', 'ReportController@collectScrips')->name('reports.collect_scrips');


	// ESTIMATED SALES
	Route::get('/tools/{market_day}/estimated_sales', 'MarketToolsController@showEstimatedSales')->name('tools.show_estimated_sales');
    Route::get('/tools/{market_day}/estimated_sales/{vendor}', 'MarketToolsController@showEstimatedSalesForVendor')->name('tools.show_estimated_sales_for_vendor');
    Route::patch('/tools/{market_day}/estimated_sales/{vendor}', 'MarketToolsController@updateEstimatedSalesForVendor')->name('tools.update_estimated_sales_for_vendor');

//	Route::get('/reports/estimated_sales', 'ReportController@estimatedSales')->name('reports.estimated_sales');

	// REPORTS
    Route::get('/reports', 'ReportController@home')->name('reports.index');
//    Route::get('/reports/booth_fees/{market_day}', 'ReportController@boothFees')->name('reports.booth_fees');
    Route::get('/reports/booth_fees_form', 'ReportController@boothFeesForm')->name('reports.booth_fees_form');
    Route::get('/reports/booth_fees/{market_day}', 'ReportController@boothFees')->name('reports.booth_fees');
    Route::get('/reports/estimated_sales_form', 'ReportController@estimatedSalesForm')->name('reports.estimated_sales_form');
    Route::get('/reports/estimated_sales/{market_day}', 'ReportController@estimatedSales')->name('reports.estimated_sales');
    Route::get('/reports/distribute_scrip_form', 'ReportController@distributeScripForm')->name('reports.distribute_scrip_form');
    Route::get('/reports/distribute_scrip/{market_day}', 'ReportController@distributeScrip')->name('reports.distribute_scrip');
    Route::get('/reports/collect_scrip_form', 'ReportController@collectScripForm')->name('reports.collect_scrip_form');
    Route::get('/reports/collect_scrip/{market_day}', 'ReportController@collectScrip')->name('reports.collect_scrip');

    Route::get('/reports/tableau_export_form', 'ReportController@tableauExportForm')->name('reports.tableau_export_form');
    Route::get('/reports/tableau_export/{market_day}', 'ReportController@tableauExport')->name('reports.tableau_export');



	// ADMIN tools

	Route::get('/admin', function () {
        return view('admin.index');
    })->name('admin');


	Route::group(['prefix' => 'admin'], function () {


	    // Routes below are long form of:
	    // Route::resource('markets', 'MarketController');
	    Route::get('/markets', 'MarketController@index')->name('markets.index');
	    Route::get('/markets/create', 'MarketController@create')->name('markets.create');
	    Route::post('/markets', 'MarketController@store')->name('markets.store');
	    Route::get('/markets/{market}', 'MarketController@show')->name('markets.show');
	    Route::get('/markets/{market}/edit', 'MarketController@edit')->name('markets.edit');
	    Route::put('/markets/{market}', 'MarketController@update')->name('markets.update');
	    Route::patch('/markets/{market}', 'MarketController@update')->name('markets.update');
	    Route::delete('/markets/{market}', 'MarketController@destroy')->name('markets.destroy');

	    // Routes below are long form of:
	    // Route::resource('vendors', 'VendorController');
	    Route::get('/vendors', 'VendorController@index')->name('vendors.index');
	    Route::get('/vendors/create', 'VendorController@create')->name('vendors.create');
	    Route::post('/vendors', 'VendorController@store')->name('vendors.store');
	    Route::get('/vendors/{vendor}', 'VendorController@show')->name('vendors.show');
	    Route::get('/vendors/{vendor}/edit', 'VendorController@edit')->name('vendors.edit');
	    Route::put('/vendors/{vendor}', 'VendorController@update')->name('vendors.update');
	    Route::patch('/vendors/{vendor}', 'VendorController@update')->name('vendors.update');
	    Route::delete('/vendors/{vendor}', 'VendorController@destroy')->name('vendors.destroy');
        Route::get('/vendors/{id}/restore', 'VendorController@restore')->name('vendors.restore');

	   // Routes below are long form of:
	    // Route::resource('vendortypes', 'VendorTypeController');
	    Route::get('/vendortypes', 'VendorTypeController@index')->name('vendortypes.index');
	    Route::get('/vendortypes/create', 'VendorTypeController@create')->name('vendortypes.create');
	    Route::post('/vendortypes', 'VendorTypeController@store')->name('vendortypes.store');
	    Route::get('/vendortypes/{vendortype}', 'VendorTypeController@show')->name('vendortypes.show');
	    Route::get('/vendortypes/{vendortype}/edit', 'VendorTypeController@edit')->name('vendortypes.edit');
	    Route::put('/vendortypes/{vendortype}', 'VendorTypeController@update')->name('vendortypes.update');
	    Route::patch('/vendortypes/{vendortype}', 'VendorTypeController@update')->name('vendortypes.update');
	    Route::delete('/vendortypes/{vendortype}', 'VendorTypeController@destroy')->name('vendortypes.destroy');

	    // Routes below are long form of:
	    // Route::resource('scrips', 'ScripController');
	    Route::get('/scrips', 'ScripController@index')->name('scrips.index');
	    Route::get('/scrips/create', 'ScripController@create')->name('scrips.create');
	    Route::post('/scrips', 'ScripController@store')->name('scrips.store');
	    Route::get('/scrips/{scrip}', 'ScripController@show')->name('scrips.show');
	    Route::get('/scrips/{scrip}/edit', 'ScripController@edit')->name('scrips.edit');
	    Route::put('/scrips/{scrip}', 'ScripController@update')->name('scrips.update');
	    Route::patch('/scrips/{scrip}', 'ScripController@update')->name('scrips.update');
	    Route::delete('/scrips/{scrip}', 'ScripController@destroy')->name('scrips.destroy');
        Route::get('/scrips/{id}/restore', 'ScripController@restore')->name('scrips.restore');


        // Routes below are long form of:
	    // Route::resource('market-days', 'MarketDayController');
	    Route::get('/market_days', 'MarketDayController@index')->name('market_days.index');
	    Route::get('/market_days/create', 'MarketDayController@create')->name('market_days.create');
	    Route::post('/market_days', 'MarketDayController@store')->name('market_days.store');
	    Route::get('/market_days/{market_day}', 'MarketDayController@show')->name('market_days.show');
	    Route::get('/market_days/{market_day}/edit', 'MarketDayController@edit')->name('market_days.edit');
	    Route::put('/market_days/{market_day}', 'MarketDayController@update')->name('market_days.update');
	    Route::patch('/market_days/{market_day}', 'MarketDayController@update')->name('market_days.update');
	    Route::delete('/market_days/{market_day}', 'MarketDayController@destroy')->name('market_days.destroy');
	    Route::get('/market_days/{market_day}/batch_upload', 'MarketDayController@showBatchUpload')->name('market_days.batch_upload');
	    Route::post('/market_days/{market_day}/batch_upload', 'MarketDayController@doBatchUpload')->name('market_days.do_batch_upload');
	}); // prefix
});
